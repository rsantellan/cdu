<?php
/**
 *
 */

$s_categories = get_option( 'resume_manager_enable_categories' );
wp_enqueue_script( 'wp-resume-manager-ajax-filters' );
?>
<form class="resume_filters">

	<div class="search_resumes">
		<div class="row">
			<?php do_action( 'resume_manager_resume_filters_search_resumes_start', $atts ); ?>

			<div class="<?php echo $s_categories ? 'col-md-4 col-sm-6' : 'col-md-5 col-sm-6'; ?> col-xs-12 search_keywords">
				<label for="search_keywords"><?php _e( 'Keywords', 'jobify' ); ?></label>
				<input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'All Resumes', 'jobify' ); ?>" />
			</div>

			<div class="<?php echo $s_categories ? 'col-md-3 col-sm-6' : 'col-md-5 col-sm-6'; ?> col-xs-12 search_location">
				<label for="search_location"><?php _e( 'Location', 'jobify' ); ?></label>
				<input type="text" name="search_location" id="search_location" placeholder="<?php esc_attr_e( 'Any Location', 'jobify' ); ?>" />
			</div>

			<?php if ( $categories ) : ?>
				<?php foreach ( $categories as $category ) : ?>
					<input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( $category ); ?>" />
				<?php endforeach; ?>
			<?php elseif ( $show_categories && $s_categories && ! is_tax( 'resume_category' ) ) : ?>
				<div class="col-md-3 col-sm-12">
					<div class="search_categories">
						<label for="search_categories"><?php _e( 'Category', 'jobify' ); ?></label>
						<?php wp_dropdown_categories( array( 'taxonomy' => 'resume_category', 'hierarchical' => 1, 'show_option_all' => __( 'All Resume Categories', 'jobify' ), 'name' => 'search_categories' ) ); ?>
					</div>
				</div>
			<?php endif; ?>

			<?php do_action( 'resume_manager_resume_filters_search_resumes_end', $atts ); ?>
		</div>
	</div>

	<div class="showing_resumes"></div>
</form>