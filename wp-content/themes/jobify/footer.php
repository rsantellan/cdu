<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package Jobify
 * @since Jobify 1.0
 */
?>

		</div><!-- #main -->
    <?php //if (function_exists("wpptopdfenh_display_icon")) echo wpptopdfenh_display_icon();?>
		<?php if ( jobify_theme_mod( 'jobify_cta', 'jobify_cta_display' ) ) : ?>
		<div class="footer-cta">
			<div class="container">
				<?php echo wpautop( jobify_theme_mod( 'jobify_cta', 'jobify_cta_text' ) ); ?>
			</div>
		</div>
		<?php endif; ?>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php if ( is_active_sidebar( 'widget-area-footer' ) ) : ?>
			<div class="footer-widgets">
				<div class="container">
					<?php dynamic_sidebar( 'widget-area-footer' ); ?>
				</div>
			</div>
			<?php endif; ?>

			<div class="copyright">
				<div class="container">
					<div class="site-info">
						<?php echo apply_filters( 'jobify_footer_copyright', sprintf( __( '&copy; %1$s %2$s &mdash; All Rights Reserved', 'jobify' ), date( 'Y' ), get_bloginfo( 'name' ) ) ); ?>
					</div><!-- .site-info -->

					<a href="#top" class="btt"><i class="icon-up-circled"></i></a>

					<?php
						if ( has_nav_menu( 'footer-social' ) ) :
							$social = wp_nav_menu( array(
								'theme_location'  => 'footer-social',
								'container_class' => 'footer-social',
								'items_wrap'      => '%3$s',
								'depth'           => 1,
								'echo'            => false,
								'link_before'     => '<span class="screen-reader-text">',
								'link_after'      => '</span>',
							) );

							echo strip_tags( $social, '<a><div><span>' );
						endif;
					?>
				</div>
			</div>
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
    

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.atooltip.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.counter.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <script src="<?php echo get_template_directory_uri(); ?>/jquery.bxslider/jquery.bxslider.min.js"></script>
    <!-- bxSlider CSS file -->
    <link href="<?php echo get_template_directory_uri(); ?>/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" />

    <script>
    <?php if(!isset($_GET['search_categories'])){ ?>
        var job_manager_ajax_filters = {};
        
        function checkCheckBoxes(){
          console.log(!jQuery("#grafico-etiqueta").is(":visible"));
          if(!jQuery("#grafico-etiqueta").is(":visible"))
          {
              jQuery("#grafico-etiqueta").find('input[type=checkbox]:checked').removeAttr('checked');
          }
          if(!jQuery("#grafico").is(":visible"))
          {
              jQuery("#grafico").find('input[type=checkbox]:checked').removeAttr('checked');
          }
          if(!jQuery("#producto-tab").is(":visible"))
          {
              jQuery("#producto-tab").find('input[type=checkbox]:checked').removeAttr('checked');
          }
          if(!jQuery("#producto-etiqueta").is(":visible"))
          {
              jQuery("#producto-etiqueta").find('input[type=checkbox]:checked').removeAttr('checked');
          }
          if(!jQuery("#interiorismo").is(":visible"))
          {
              jQuery("#interiorismo").find('input[type=checkbox]:checked').removeAttr('checked');
          }
          if(!jQuery("#interiorismo-etiqueta").is(":visible"))
          {
              jQuery("#interiorismo-etiqueta").find('input[type=checkbox]:checked').removeAttr('checked');
          }
          if(!jQuery("#textil").is(":visible"))
          {
              jQuery("#textil").find('input[type=checkbox]:checked').removeAttr('checked');
          }
          if(!jQuery("#textil-etiqueta").is(":visible"))
          {
              jQuery("#textil-etiqueta").find('input[type=checkbox]:checked').removeAttr('checked');
          }
          if(!jQuery("#web").is(":visible"))
          {
              jQuery("#web").find('input[type=checkbox]:checked').removeAttr('checked');
          }
          if(!jQuery("#web-etiqueta").is(":visible"))
          {
              jQuery("#web-etiqueta").find('input[type=checkbox]:checked').removeAttr('checked');
          }
        }
        job_manager_ajax_filters.ajax_url = '<?php echo admin_url(); ?>admin-ajax.php?lang=<?php echo $lang;?>'; 
        jQuery( document ).ready(function($) {
            
            <?php if ( is_user_logged_in() ) { ?>
            $('#login-modal').hide();
            <?php } ?>
        
            $('a.fixedTip').aToolTip({
				fixed: true,
				xOffset: -200,
        		yOffset: 10
			});

			job_manager_ajax_filters.ajax_url = '<?php echo admin_url(); ?>admin-ajax.php?lang=<?php echo $lang;?>';
			jobifySettings.ajaxurl = '<?php echo admin_url(); ?>admin-ajax.php?lang=<?php echo $lang;?>';
            
            $(".soliloquy_category_<?php echo $pm->slug; ?>").show();
            
            function showPortfolio(category){
				$(".soliloquy-container").hide();
				$('html, body').animate({scrollTop:$('#job-portfolio').offset().top - 20}, 'slow');
				$(".soliloquy_category_" + category).fadeIn( "slow" );
			}
            
            //Oculta el link a portfolio en Insitituos de formacion
            if($( "body" ).hasClass( "term-institutos-de-formacion" )){
                $(".contact a").text(function () {
                    return $(this).text().replace("Ficha/Portfolio", "Ficha"); 
                });
            }            
            
            //Oculta el portfolio dentro de la ficha de Institutos de formacion
            var job_type = $('.job-type').length;        
            var institutos = $('.instituto-de-formacion').length;
            if(job_type == 1 && institutos == 1){
                $('.job-overview-title').hide();
                $('#SliderPrincipal').hide();
                $('#ContenedorInfoTab').hide();
                $('.bx-controls').hide();
            }else{
                $('.job-overview-title').show();
                $('#SliderPrincipal').show();
                $('#ContenedorInfoTab').show();
                $('.bx-controls').show();
            }
            
            //Oculta el selector de categorias del portfolio si la empresa solo tiene una categoria
            var job_category = $('.job-category').length;    
            if(job_category == 1){
                $('#SliderPrincipal').hide();            
            }
            
            //Cambia todos los campos del preview despues de modificar una empresa a un Boton de Seguir Modificando
            if($( "body" ).hasClass( "page-id-1915" ) && $( ".job_listing_preview" ).length > 0){
                $("#submit-job-form").html('<input id="seguir_modificando" type="submit" value="Seguir Modificando" class="button" name="">');
                $( ".job_listing_preview" ).show();
            }else{
                $( ".job_listing_preview" ).show();
            }
            
            //El boton de seguir modificando lleva al formulario || CAMBIAR SI SE CAMBIA EL SITIO DE LUGAR
            $("#seguir_modificando").click(function(){
                //window.location.href = "http://sintropiadesign.com/cduagustin/modificar-datos/?action=upsert";
            })
            
            //Si es la pagina de SEGUIR MODIFICANDO que no tiene nada, redirigir al home
            if($( "body" ).hasClass( "page-id-4533" )){
                //window.location.href = "http://directorio.cdu.org.uy";
                
                $("#post-4533 .entry-content div").attr('style','text-align: center;font-size: 28px;color: #00ade5;');
            }

            //Para agregar tabs en el Modificar Datos
            $( '<div class="tabs-container"><ul class="nav nav-tabs" role="tablist" id="tabs-subcat">  <li role="subcategorias" class="active"><a href="#grafico" aria-controls="home" role="tab" data-toggle="tab">Gráfico / Packaging</a></li>  <li role="subcategorias"><a href="#interiorismo" aria-controls="profile" role="tab" data-toggle="tab">Interiorismo / Paisajismo</a></li>  <li role="subcategorias"><a href="#producto-tab" aria-controls="messages" role="tab" data-toggle="tab">Producto</a></li>  <li role="subcategorias"><a href="#textil" aria-controls="settings" role="tab" data-toggle="tab">Textil / Indumentaria</a></li>  <li role="subcategorias"><a href="#web" aria-controls="settings" role="tab" data-toggle="tab">Web / Multimedia</a></li></ul><div class="tab-content tab-content-subcat">  <div role="tabpanel" class="tab-pane active tab-sub" id="grafico">...</div>  <div role="tabpanel" class="tab-pane tab-sub" id="interiorismo">...</div>  <div role="tabpanel" class="tab-pane tab-sub" id="producto-tab">...</div>  <div role="tabpanel" class="tab-pane tab-sub" id="textil">...</div>  <div role="tabpanel" class="tab-pane tab-sub" id="web">...</div></div></div>' ).insertAfter( "#idEtiquetaModificar" );
            
            var my_html = "";
            var contador = 0;
            $('.subcat-class').each(function(){
                my_html = $(this).html();
                $('.tab-sub').eq(contador).html('<ul class="subcat-class">'+my_html+'</ul>');
                contador++;
                $(this).remove();
            });
            
            contador = 0;
            
            $( '<div class="tabs-container"><ul class="nav nav-tabs" role="tablist" id="etiquetas">  <li role="etiquetas" class="active"><a href="#grafico-etiqueta" aria-controls="home" role="tab" data-toggle="tab">Gráfico / Packaging</a></li>  <li role="etiquetas"><a href="#interiorismo-etiqueta" aria-controls="profile" role="tab" data-toggle="tab">Interiorismo / Paisajismo</a></li>  <li role="etiquetas"><a href="#producto-etiqueta" aria-controls="messages" role="tab" data-toggle="tab">Producto</a></li>  <li role="etiquetas"><a href="#textil-etiqueta" aria-controls="settings" role="tab" data-toggle="tab">Textil / Indumentaria</a></li>  <li role="etiquetas"><a href="#web-etiqueta" aria-controls="settings" role="tab" data-toggle="tab">Web / Multimedia</a></li></ul><div class="tab-content">  <div role="tabpanel" class="tab-pane active tab-sub-etiqueta" id="grafico-etiqueta">...</div>  <div role="tabpanel" class="tab-pane tab-sub-etiqueta" id="interiorismo-etiqueta">...</div>  <div role="tabpanel" class="tab-pane tab-sub-etiqueta" id="producto-etiqueta">...</div>  <div role="tabpanel" class="tab-pane tab-sub-etiqueta" id="textil-etiqueta">...</div>  <div role="tabpanel" class="tab-pane tab-sub-etiqueta" id="web-etiqueta">...</div></div></div>' ).insertAfter( "#IdUl" );
            
            $('#IdUl ul').each(function(){
                my_html = $(this).html();
                $('.tab-sub-etiqueta').eq(contador).html('<ul class="etiqueta-ul">'+my_html+'</ul>');
                contador++;
                $(this).remove();
            });
            
            $('.tituloTag').remove();
            
            $( "input[name*='job_category']" ).change(function() {
                if($(this).is(":checked")){
                    switch ($(this).attr('id'))
                    {
                       case "graficopackaging":
                            $("a[href*=#grafico]").show();
                            $("a[href*=#grafico-etiqueta]").show();
                            $("#grafico").show();
                            $("#grafico-etiqueta").show();
                            break;
                       case "producto":
                            $("a[href*=#producto-tab]").show();
                            $("a[href*=#producto-etiqueta]").show();
                            $("#producto-tab").show();
                            $("#producto-etiqueta").show();
                            break;
                       case "interiorismo-paisajismo":
                            $("a[href*=#interiorismo]").show();
                            $("a[href*=#interiorismo-etiqueta]").show();
                            $("#interiorismo").show();
                            $("#interiorismo-etiqueta").show();
                            break;    
                       case "textil-indumentaria": 
                            $("a[href*=#textil]").show();
                            $("a[href*=#textil-etiqueta]").show();
                            $("#textil").show();
                            $("#textil-etiqueta").show();
                            break;
                        /*case "tienda-de-diseno": 
                            $("a[href*=#producto-tab]").show();
                            $("a[href*=#producto-etiqueta]").show();
                            $("#producto-tab").show();
                            $("#producto-etiqueta").show();
                            break;*/
                        case "web-multimedia": 
                            $("a[href*=#web]").show();
                            $("a[href*=#web-etiqueta]").show();
                            $("#web").show();
                            $("#web-etiqueta").show();
                            break;
                    }//fin de switch    
                }else{//no esta chequeado
                    switch ($(this).attr('id'))
                    {
                        case "graficopackaging":
                            $("a[href*=#grafico]").hide();
                            $("a[href*=#grafico-etiqueta]").hide();
                            $("#grafico").hide();
                            $("#grafico-etiqueta").hide();
                            $("#grafico input[type='checkbox']").prop("checked", false);
                            $("#grafico-etiqueta input[type='checkbox']").prop("checked", false);
                            break;
                        case "producto":
                            $("a[href*=#producto-tab]").hide();
                            $("a[href*=#producto-etiqueta]").hide();
                            $("#producto-tab").hide();
                            $("#producto-etiqueta").hide();
                            
                            $("#producto input[type='checkbox']").prop("checked", false);
                            $("#producto-etiqueta input[type='checkbox']").prop("checked", false);
                            break;
                        case "interiorismo-paisajismo":
                            $("a[href*=#interiorismo]").hide();
                            $("a[href*=#interiorismo-etiqueta]").hide();
                            $("#interiorismo").hide();
                            $("#interiorismo-etiqueta").hide();
                            
                            $("#interiorismo input[type='checkbox']").prop("checked", false);
                            $("#interiorismo-etiqueta input[type='checkbox']").prop("checked", false);
                            break;    
                       case "textil-indumentaria": 
                            $("a[href*=#textil]").hide();
                            $("a[href*=#textil-etiqueta]").hide();
                            $("#textil").hide();
                            $("#textil-etiqueta").hide();
                            
                            $("#textil input[type='checkbox']").prop("checked", false);
                            $("#textil-etiqueta input[type='checkbox']").prop("checked", false);
                            break;
                        /*case "tienda-de-diseno": 
                            $("a[href*=#producto-tab]").hide();
                            $("a[href*=#producto-etiqueta]").hide();
                            $("#producto-tab").hide();
                            $("#producto-etiqueta").hide();
                            
                            $("#producto-tab input[type='checkbox']").prop("checked", false);
                            $("#producto-etiqueta input[type='checkbox']").prop("checked", false);
                            break;*/
                         case "web-multimedia": 
                            $("a[href*=#web]").hide();
                            $("a[href*=#web-etiqueta]").hide();
                            $("#web").hide();
                            $("#web-etiqueta").hide();
                            
                            $("#web input[type='checkbox']").prop("checked", false);
                            $("#web-etiqueta input[type='checkbox']").prop("checked", false);
                            break;
                    }   
                }//Fin de IF si esta chequeado    
            }); //Fin de checkbox control
            
            $( "input[name*='job_category']" ).each(function(){
                if($(this).is(":checked")){
                    switch ($(this).attr('id'))
                    {
                        case "producto":
                            $("a[href*=#producto-tab]").show();
                            $("a[href*=#producto-etiqueta]").show();
                            $("#producto-tab").show();
                            $("#producto-etiqueta").show();
                            break;  
                       case "graficopackaging":
                            $("a[href*=#grafico]").show();
                            $("a[href*=#grafico-etiqueta]").show();
                            $("#grafico").show();
                            $("#grafico-etiqueta").show();
                            break;     
                       case "graficopackaging":
                            $("a[href*=#grafico]").show();
                            $("a[href*=#grafico-etiqueta]").show();
                            $("#grafico").show();
                            $("#grafico-etiqueta").show();
                            break;
                       case "interiorismo-paisajismo":
                            $("a[href*=#interiorismo]").show();
                            $("a[href*=#interiorismo-etiqueta]").show();
                            $("#interiorismo").show();
                            $("#interiorismo-etiqueta").show();
                            break;    
                       case "textil-indumentaria": 
                            $("a[href*=#textil]").show();
                            $("a[href*=#textil-etiqueta]").show();
                            $("#textil").show();
                            $("#textil-etiqueta").show();
                            break;
                        case "tienda-de-diseno": 
                            $("a[href*=#producto-tab]").show();
                            $("a[href*=#producto-etiqueta]").show();
                            $("#producto-tab").show();
                            $("#producto-etiqueta").show();
                            break;
                        case "web-multimedia": 
                            $("a[href*=#web]").show();
                            $("a[href*=#web-etiqueta]").show();
                            $("#web").show();
                            $("#web-etiqueta").show();
                            break;
                    }//fin de switch    
                }    
            });
            
            $("#grafico-etiqueta").parent().css('height','200px');
            
            //Imagenes duplicadas
            var seen = {};
            $('.imagenesBorrar .ImgYBoton').each(function() {
                var txt = $(this).attr('id');
                if (seen[txt])
                    $(this).parent().remove();
                else
                    seen[txt] = true;
            });
            
            //Reordenar datos
            
            var html_datos = "";
            html_datos = '<fieldset class="fieldset-_job_address test">'+$('.fieldset-_job_address').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-_job_city test">'+$('.fieldset-_job_city').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-_job_information test">'+$('.fieldset-_job_information').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-application test">'+$('.fieldset-application').html()+'</fieldset>';
            
            $('.fieldset-_job_address').remove();
            $('.fieldset-_job_city').remove();
            $('.fieldset-_job_information').remove();
            $('.fieldset-application').remove();
            
            $('.fieldset-_job_mobile').after(html_datos);
            
            //Campo direccion no requerido y sacar texto "Ubicación de la empresa (opcional)"
            $('#_job_address').parent().attr('class','field');
            $('.fieldset-job_location label').html('Geolocalización de la empresa (únicamente para tiendas de diseño)');
            
            var general = 'Datos Generales', categories = 'Categorias , Subcategorias Y Etiquetas',social = 'Redes Sociales',images = 'Imágenes',descripcion = 'Descripción',descripcion_breve = 'Descripción Breve',descripcion_extendida = 'Descripción Extendida';
            if($( ".no_translate select option:selected" ).text() == "English"){
                general = 'General Information';
                categories = 'Categories, Subcategories and Tags';
                social = 'Social Media';
                descripcion = 'Description'
                images = 'Images';
                descripcion_breve = 'Short Description';
                descripcion_extendida = 'Extended Description';
            }
            if($( ".no_translate select option:selected" ).text() == "Français"){
                general = 'Informations Générales';
                categories = 'Catégories, Sous y étiquettes';
                social = 'Médias Sociaux';
                descripcion = 'Description'
                images = 'Images';
                descripcion_breve = 'Description Courte';
                descripcion_extendida = 'Description étendue';
            }
            if($( ".no_translate select option:selected" ).text() == "Italiano"){
                general = 'Informazioni Generali';
                categories = 'Categorie, Sottocategorie y Etichette';
                social = 'Social Media';
                descripcion = 'Descrizione'
                images = 'Immagini';
                descripcion_breve = 'Breve Descrizione';
                descripcion_extendida = 'Descrizione estesa';
            }
            if($( ".no_translate select option:selected" ).text() == "Português"){
                general = 'Informações Gerais';
                categories = 'Categorias , Subcategorias y Etiquetas';
                descripcion = 'Descrição'
                social = 'Social Media';
                images = 'Imagens';
                descripcion_breve = 'Descrição curta';
                descripcion_extendida = 'Descrição Estendida';
            }
            
            //Reorden de campos
            html_datos = '<fieldset class="fieldset-_job_employees test">'+$('.fieldset-_job_employees').html()+'</fieldset>';
            $('.fieldset-_job_employees').remove();
            $('.fieldset-_job_creation_year').after( html_datos );
            
            html_datos = '<fieldset class="fieldset-_job_bilingual test">'+$('.fieldset-_job_bilingual').html()+'</fieldset>';
            $('.fieldset-_job_bilingual').remove();
            $('.fieldset-_job_employees').after( html_datos );
            
            html_datos = '<fieldset class="fieldset-_job_phone test">'+$('.fieldset-_job_phone').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-_job_mobile test">'+$('.fieldset-_job_mobile').html()+'</fieldset>';
            $('.fieldset-_job_phone').remove();
            $('.fieldset-_job_mobile').remove();
            $('.fieldset-_job_bilingual').after( html_datos );
            
            html_datos = '<fieldset class="fieldset-application test">'+$('.fieldset-application').html()+'</fieldset>';
            $('.fieldset-application').remove();
            $('.fieldset-_job_mobile').after( html_datos );
            
            html_datos = '<fieldset class="fieldset-_company_website">'+$('.fieldset-_company_website').html()+'</fieldset>';
            $('.fieldset-_company_website').remove();
            $('.fieldset-application').after( html_datos );
            
            html_datos = '<fieldset class="fieldset-_job_address">'+$('.fieldset-_job_address').html()+'</fieldset>';
            $('.fieldset-_job_address').remove();
            $('.fieldset-job_region').after( html_datos );
            
            html_datos = '<fieldset class="fieldset-_company_twitter">'+$('.fieldset-_company_twitter').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-company_facebook">'+$('.fieldset-company_facebook').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-company_facebook2">'+$('.fieldset-company_facebook2').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-company_google">'+$('.fieldset-company_google').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-company_linkedin">'+$('.fieldset-company_linkedin').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-company_pinterest">'+$('.fieldset-company_pinterest').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-company_instagram">'+$('.fieldset-company_instagram').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-company_behance">'+$('.fieldset-company_behance').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-company_foursquare">'+$('.fieldset-company_foursquare').html()+'</fieldset>';
            html_datos = html_datos + '<fieldset class="fieldset-company_youtube">'+$('.fieldset-company_youtube').html()+'</fieldset>';
            $('.fieldset-_company_twitter').remove();
            $('.fieldset-company_facebook').remove();
            $('.fieldset-company_facebook2').remove();
            $('.fieldset-company_google').remove();
            $('.fieldset-company_linkedin').remove();
            $('.fieldset-company_pinterest').remove();
            $('.fieldset-company_instagram').remove();
            $('.fieldset-company_behance').remove();
            $('.fieldset-company_foursquare').remove();
            $('.fieldset-company_youtube').remove();
            $('.fieldset-_job_city').after( html_datos );
            
            html_datos = '<fieldset class="ffieldset-company_description">'+$('.fieldset-company_description').html()+'</fieldset>';
            $('.fieldset-company_description').remove();
            $('.fieldset-_job_information').after( html_datos );
            
            $('.account-sign-in').after('<h2>'+general+'</h2>');
            $('.fieldset-_company_twitter').before('<h2>'+social+'</h2>');
            $('.fieldset-company_youtube').after('<h2>'+descripcion+'</h2>');
            $('.ffieldset-company_description').after('<h2>'+categories+'</h2>');
            
            //Textos en reordenar categorias
            $('.fieldset-job_primarycategory label').after( "<p class='text-aclaracion'>Se podrá seleccionar más de una en caso que corresponda, pero se deberá indicar cuál es la categoría principal de la empresa y cuál/es la/s secundaria/s.  Si la empresa participa en más de una categoría deberá subir de forma obligatoria imágenes asociadas a las mismas para ilustrar su porfolio.</p>" );
            
            $('.fieldset-job_category label').after( "<p class='text-aclaracion'>Seleccione las Sub categorías preestablecidas de la siguiente lista por las cuales desea que el usuario pueda filtrar su empresa a través del buscador del directorio, según la/s categoría/s en la/s que se encuentra. Solo deberá seleccionar subcategorías relacionadas a las categorías en las que participa.</p>" );
            
            $('#idEtiquetaModificar').next('.tabs-container').prepend( "<label style='text-align: center;width: 100%;font-size: 18px;margin-top:40px !important;'>Sub categorías pre establecidas<br>(visibles en el buscador para el usuario)</label><p class='text-aclaracion'>Seleccione las palabras claves preestablecidas de la siguiente lista por las cuales desea que el usuario pueda encontrar su empresa a través del buscador del directorio, según la/s categoría/s en la/s que se encuentra. Solo deberá seleccionar las palabras claves-tags relacionadas a las categorías en las que participa.</p>" );
            
            $('#ingrese-etiquetar-per').after( "<p class='text-aclaracion'>Ingrese sus palabras clave - tags personalizadas (que no se encuentren en los listados antes mencionados) por los cuales desea que el usuario encuentre a su empresa a través del buscador del directorio. Las mismas deben estar separadas por comas.</p>" );
            
            var text = 'Las imágenes deben ser de 1100x450 px y no pueden superar los 500 kb. Podrá agregar la descripción de la imagen en el campo de texto.';
            $("p:contains('" + text + "')").remove();
            
            $('.fieldset-_company_logo label').after( "<p class='text-aclaracion'>JPG de 300x300 px. de no más de 150 Kb.</p>" );
            
            $('#divImagenes').prepend("<p class='text-aclaracion' id='portfolio-aclaracion'>Deberá incluir imágenes de 1100 x 450 px y no pueden superar los 500 kb cada una. Podrá agregar la descripción de cada una de las imágenes en el campo de texto. Si la empresa participa en más de una categoría deberá subir de forma obligatoria hasta 5 imágenes asociadas a cada una de las categorías correspondientes.</p>");
            
            $('#portfolio-aclaracion').after('<h2>'+images+'</h2>');
            
            $('#portfolio-aclaracion').before( "<label style='text-align: center;width: 100%;font-size: 18px;'>Portfolio</label>" );
            
            $('.fieldset-_job_information label').after( "<p class='text-aclaracion'>Ingrese una breve descripción de su empresa de forma genérica que figurará en su ficha específica al lado de los datos generales.</p>" );
            
            $('.ffieldset-company_description label').after( "<p class='text-aclaracion'>Ingrese una descripción extendida complementaria a la anterior, donde podrá incluir visión, misión, clientes destacados, información de interés sobre la empresa para el usuario, etc.</p>" );
            $('.ffieldset-company_description .description').remove();
            
            $('#IdUl').after( "<p class='text-aclaracion' style='margin-bottom:0;'><label style='color:black;text-align: center;width: 100%;font-size: 18px;margin-top:40px !important;'>Palabras clave - tags preestablecidas (Ocultas)</label><br>Seleccione las palabras claves preestablecidas de la siguiente lista por las cuales desea que el usuario pueda encontrar su empresa a través del buscador del directorio, según la/s categoría/s en la/s que se encuentra. Solo deberá seleccionar las palabras claves-tags relacionadas a las categorías en las que participa.</p>" );
            
            if($('#submit-job-form').length && !$( ".job_listing_preview" ).length){
                $('input[type=submit]').attr('value','Actualizar Empresa');
            }
            
            $('.fieldset-_job_information label').html(descripcion_breve);
            $('.ffieldset-company_description label').html(descripcion_extendida);
            
            $('#company_description').attr('maxlength',1500);
            
            $("#company_description").counter({
                type: 'char',
                goal: 1500,
                msg: 'caracteres disponibles con espacios.'
            });
            
            $("#_job_information").counter({
                type: 'char',
                goal: 500,
                msg: 'caracteres disponibles con espacios.'
            });
            
        });
    <?php } ?>
    </script>
    
    <script src="<?php echo get_template_directory_uri(); ?>/js/publicedit.js"></script>
</body>
</html>
