<?php
/**
 * Job Category
 *
 * @package Jobify
 * @since Jobify 1.0
 */

$taxonomy = get_taxonomy( get_queried_object()->taxonomy );

get_header(); ?>

	<header class="page-header <?php echo 'page-header-' . get_queried_object()->slug; ?>">
		<h1 class="page-title"><?php single_term_title(); ?></h1>

		<?php if( $taxonomy ) : ?>
			<h2 class="page-subtitle"><?php echo esc_attr( $taxonomy->labels->singular_name ); ?></h2>
		<?php endif; ?>
	</header>

	<div class="application">
		<div class="dir_application_details animated modal">
			<h2 class="modal-title"><?php _e( 'Apply', 'jobify' ); ?></h2>

			<div class="application-content">
			<?php
				/**
				 * job_manager_application_details_email or job_manager_application_details_url hook
				 */
				do_action( 'job_manager_application_details_email', false );
			?>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<div id="content" class="container" role="main">
			<div class="entry-content">

				<?php
				add_filter( 'posts_clauses', 'order_featured_job_listing' );

				global $wp_query;
				query_posts(
					array_merge(
						$wp_query->query,
						array(
							'orderby' => 'meta_key',
							'meta_key' => '_featured',
							'order' => 'DESC',
							'posts_per_page' => -1
						)
					)
				);

				remove_filter( 'posts_clauses', 'order_featured_job_listing' );
				?>

				<?php if ( have_posts() ) : ?>
				<div class="job_listings">
					<ul class="job_listings">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_job_manager_template_part( 'content', 'job_listing' ); ?>
						<?php endwhile; ?>
					</ul>
				</div>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>

			</div>
		</div><!-- #content -->

		<?php do_action( 'jobify_loop_after' ); ?>
	</div><!-- #primary -->

<?php get_footer(); ?>
