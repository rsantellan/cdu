<?php

function jobify_apply_via_form() {
	$apply = false;

	if ( function_exists( 'gravity_form' ) && class_exists( 'Astoundify_Job_Manager_Apply_GF' ) )
		$apply = 'gravityforms';

	if ( defined( 'NINJA_FORMS_VERSION' ) && class_exists( 'Astoundify_Job_Manager_Apply_Ninja' ) )
		$apply = 'ninjaforms';

	/*
	if ( defined( 'WPCF7_VERSION' ) )
		$apply = 'cf7';
	*/

	return apply_filters( 'jobify_apply_via_form', $apply );
}

/**
 * If we are applying via a form we need to ajust a few CSS values.
 *
 * @since Jobify 1.7.1
 *
 * @param array $classes Existing class values.
 * @return array Filtered class values.
 */
function jobify_job_manager_body_class( $classes ) {
	if ( jobify_apply_via_form() ) {
		$classes[] = 'wp-job-manager-apply-form';
	}

	return $classes;
}
add_filter( 'body_class', 'jobify_job_manager_body_class' );

/**
 * Determine what to show for a job listing applying via email.
 *
 * If an application form is set up then remove the default output and add the
 * form. The apply via resume will still appear after if ncessary.
 *
 * @since Jobify 1.7.1
 *
 * @return void
 */
function jobify_job_manager_application_details_email() {
	if ( jobify_apply_via_form() ) {
		remove_all_actions( 'job_manager_application_details_email', 10 );
		add_action( 'job_manager_application_details_email', 'jobify_job_manager_apply_via_form', 15 );
	}
}
add_action( 'init', 'jobify_job_manager_application_details_email' );

/**
 * Determine what to show for resume listing.
 *
 * If an application form is set up then show that, otherwise show email links.
 *
 * @since Jobify 1.7.1
 *
 * @return void
 */
function jobify_resume_manager_application_details_email() {
	if ( jobify_apply_via_form() ) {
		add_action( 'jobify_resume_manager_application_details_email', 'jobify_job_manager_apply_via_form', 15 );
	} else {
		add_action( 'jobify_resume_manager_application_details_email', 'jobify_resume_manager_application_details_email_std' );
	}
}
add_action( 'init', 'jobify_resume_manager_application_details_email' );

/**
 * Load the correct form shortcode.
 *
 * @since Jobify 1.7.0
 *
 * @return void
 */
function jobify_job_manager_apply_via_form( $apply ) {
	$plugin = jobify_apply_via_form();
	$post   = get_post();

	if ( 'resume' == $post->post_type ) {
		$form = get_option( 'job_manager_resume_apply' );
	} else {
		$form = get_option( 'job_manager_job_apply' );
	}

	switch ( $plugin ) {
		case 'gravityforms' :
			echo do_shortcode( sprintf( '[gravityform id="%s" %s]', $form, apply_filters( 'gravityforms_apply_form_args', 'title="false" description="false" ajax="true"' ) ) );
		break;
		case 'ninjaforms' :
			echo do_shortcode( sprintf( '[ninja_forms_display_form id="%s" %s]', $form, apply_filters( 'ninjaforms_apply_form_args', '' ) ) );
		break;
		case 'cf7' :
			echo do_shortcode( sprintf( '[contact-form-7 id="%s" %s]', $form, apply_filters( 'cf7_apply_form_args', '' ) ) );
		break;
		default :
			do_action( 'jobify_job_apply_form_default', $form );
		break;
	}
}