<?php
/**
 * Google Maps for things.
 */

/**
 * Map Points
 *
 * @since Jobify 1.0
 */
class Jobify_Widget_Map_Interactive {

	/**
	 * @var $items
	 */
	private $items;

	/**
	 * @var $args
	 */
	private $args;

	/**
	 * Set things up
	 *
	 * @since Jobify 1.0
	 *
	 * @return void
	 */
	public function __construct( $args = array() ) {
		$defaults = array(
			'type'           => 'job_listing',
			'posts_per_page' => apply_filters( 'jobify_map_points_count', 20 ),
			'search_job_types' => array(117)
		);

		$this->args  = wp_parse_args( $args, $defaults );
		$this->items = $this->get_items();

		$this->setup_actions();
	}

	private function get_items() {
		if ( 'job_listing' == $this->args[ 'type' ] ) {

			return get_job_listings( $this->args );
		} else {
			if ( class_exists( 'WP_Resume_Manager' ) ) {
				return get_resumes( $this->args );
			}
		}
	}

	/**
	 * Create default map points, and update when needed.
	 *
	 * @since Jobify 1.0
	 *
	 * @return void
	 */
	private function setup_actions() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		add_action( 'wp_ajax_nopriv_jobify_update_map', array( $this, 'jobify_json_ajax_points' ) );
		add_action( 'wp_ajax_jobify_update_map', array( $this, 'jobify_json_ajax_points' ) );

		add_action( 'wp_ajax_jobify_cache_cords', array( $this, 'cache_cords' ) );
		add_action( 'wp_ajax_nopriv_jobify_cache_cords', array( $this, 'cache_cords' ) );

		remove_action( 'job_manager_job_filters_search_jobs_end', array( 'WP_Job_Manager_Job_Tags_Shortcodes', 'show_tag_filter' ) );
	}

	/**
	 * Enqueue Scripts
	 *
	 * @since Jobify 1.4.3
	 *
	 * @return void
	 */
	function enqueue_scripts() {
		wp_enqueue_script( 'google-maps', ( is_ssl() ? 'https' : 'http' ) . '://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array(), 20140306, true );
		wp_enqueue_script( 'jobify-map', get_template_directory_uri() . '/js/jobify-map.min.js', array( 'google-maps' ), 20140306, true );
	}

	/**
	 * Create an array of all of the points found.
	 *
	 * @since Jobify 1.0
	 *
	 * @return void
	 */
	public function json_points() {
		$points = array();

		while ( $this->items->have_posts() ) {
			$this->items->the_post();

			$item = get_post();
			$location = $item->_job_location ? $item->_job_location : $item->_candidate_location;

			if ( ! ( $item->geolocation_lat || $item->geolocation_long ) ) {
				if ( ! $location ) {
					continue;
				}

				if ( ! class_exists( 'WP_Job_Manager_Geocode' ) ) {
					continue;
				}

				$address_data = WP_Job_Manager_Geocode::get_location_data( $location );

				if ( ! is_wp_error( $address_data ) && $address_data ) {
					foreach ( $address_data as $key => $value ) {
						if ( $value ) {
							update_post_meta( $item->ID, 'geolocation_' . $key, $value );
						}
					}
				} else {
					continue;
				}
			}

			if ( 'job_listing' == get_post_type( $item->ID ) ) {
				$title = get_post_field( 'post_title', $item->ID );
			} else {
				$title = get_post_field( 'post_title', $item->ID );
			}

			$featured = get_post_meta( $item->ID, '_featured', true );
			if($featured){
				$points[] = array(
					'latLng'    => array( $item->geolocation_lat, $item->geolocation_long ),
					'data'      => array(
						'item'      => $item->ID,
						'permalink' => get_permalink( $item->ID ),
						'title'     => $title
					)
				);
			}else{
				$points[] = array(
					'latLng'    => array( $item->geolocation_lat, $item->geolocation_long ),
					'data'      => array(
						'item'      => $item->ID,
						'permalink' => '#',
						'title'     => $title
					)
				);
			}
		}

		return $points;
	}

	/**
	 * Set the filtered points
	 *
	 * @since Jobify 1.0
	 *
	 * @return void
	 */
	function jobify_json_ajax_points() {
		global $job_manager, $wpdb;

		$search_location   = isset ( $_POST[ 'search_location' ] ) ? sanitize_text_field( stripslashes( $_POST['search_location'] ) ) : '';
		$search_keywords   = isset ( $_POST[ 'search_keywords' ] ) ? sanitize_text_field( stripslashes( $_POST['search_keywords'] ) ) : '';
		$search_categories = isset( $_POST['search_categories'] ) ? $_POST['search_categories'] : '';

		if ( is_array( $search_categories ) ) {
			$search_categories = array_filter( array_map( 'sanitize_text_field', array_map( 'stripslashes', $search_categories ) ) );
		} else {
			$search_categories = array_filter( array( sanitize_text_field( stripslashes( $search_categories ) ) ) );
		}

		$search_type      = isset( $_POST[ 'search_type' ] ) ? esc_attr( $_POST[ 'search_type' ] ) : 'job_listing';
		$per_page         = isset( $_POST[ 'per_page' ] ) ? absint( $_POST[ 'per_page' ] ) : null;

		$map = new self( array(
			'search_location'   => $search_location,
			'search_keywords'   => $search_keywords,
			'search_categories' => $search_categories,
			'type'              => $search_type,
			'posts_per_page'    => $per_page
		) );

		$points = $map->json_points();

		echo json_encode( $points );

		die(0);
	}
}