<?php
/**
 * Home: Jobs Search
 *
 * @since Jobify 1.7.0
 */
class Jobify_Widget_Jobs_Search extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_jobs_search';
		$this->widget_description = __( 'Output search options to search jobs.', 'jobify' );
		$this->widget_id          = 'jobify_widget_jobs_search';
		$this->widget_name        = __( 'Jobify - Home: Jobs Search', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => __( 'Search Jobs', 'jobify' ),
				'label' => __( 'Title:', 'jobify' )
			)
		);

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );

		echo $before_widget;
		?>

				<?php if ( $title ) echo $before_title . $title . $after_title; ?>
				<?php
					$referer_uri = str_replace('http://'.$_SERVER['SERVER_NAME'], '', $_SERVER['HTTP_REFERER']);
					$url_parts = explode('/', $referer_uri);
					$lang = $url_parts[1];
					if(strlen($lang)!=2)
						$lang = 'es';

					$add2url = '';
					if($lang!='es')
						$add2url = '/'.$lang;
				?>
				<form method="get" action="<?php echo esc_url( home_url('/directorio' ) ); ?>">
					<div class="search_jobs standalone-search-jobs row">
						<div class="col-md-3 col-sm-12">
							<div class="search_categories">
								<label for="search_categories"><?php _e( 'Category', 'jobify' ); ?></label>
								<?php wp_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'depth' => 1, 'show_option_all' => __( 'Category', 'jobify' ), 'name' => 'search_categories', 'orderby' => 'name' ) ); ?>
							</div>
						</div>

						<div class="col-md-2 col-sm-12">
							<div class="search_categories">
								<label for="search_subcategories"><?php _e( 'Subcategory', 'jobify' ); ?></label>
							<div class="select" id="subcat_container"><select name="search_subcategories" id="search_subcategories"><option value="0" selected="selected"><?php echo __( 'Subcategory', 'jobify' ) ?></option></select></div>
							</div>
						</div>

						<div class="col-md-2 col-sm-12">
							<div class="search_categories">
								<label for="search_job_types"><?php _e( 'Type', 'jobify' ); ?></label>
								<div class="select"><?php wp_dropdown_categories( array( 'taxonomy' => 'job_listing_type', 'hierarchical' => 1, 'show_option_all' => __( 'Type', 'jobify' ), 'name' => 'search_job_types', 'orderby' => 'name' ) ); ?></div>
							</div>
						</div>

						<div class="col-md-3 col-sm-12 search_keywords">
							<label for="search_keywords"><?php _e( 'Keywords', 'jobify' ); ?></label>
							<input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'Keywords', 'jobify' ); ?>" value="<?php echo esc_attr( $keywords ); ?>" />
						</div>

						<?php do_action( 'job_manager_job_filters_search_jobs_end', $atts ); ?>

						<!-- <div class="search_keywords col-md-5">
							<label for="search_keywords"><?php _e( 'Keywords', 'jobify' ); ?></label>
							<input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'All Jobs', 'jobify' ); ?>" />
						</div>
						<div class="search_location col-md-5">
							<label for="search_location"><?php _e( 'Location', 'jobify' ); ?></label>
							<input type="text" name="search_location" id="search_location" placeholder="<?php esc_attr_e( 'Any Location', 'jobify' ); ?>" />
						</div>

						<div class="col-md-2">
							<input type="submit" name="submit" value="<?php echo esc_attr_e( 'Search', 'jobify' ); ?>" />
						</div> -->
					</div>
				</form>



		<?php
		echo $after_widget;

		$content = ob_get_clean();

		echo apply_filters( 'jobify_widget_jobs_search', $content );

		$this->cache_widget( $args, $content );
	}
}