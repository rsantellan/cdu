<?php
/**
 * Home: Recent Jobs
 *
 * @since Jobify 1.0
 */
class Jobify_Widget_Jobs extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_jobs';
		$this->widget_description = __( 'Output a list of recent jobs.', 'jobify' );
		$this->widget_id          = 'jobify_widget_jobs';
		$this->widget_name        = __( 'Jobify - Home: Recent Jobs', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => __( 'Recent Jobs', 'jobify' ),
				'label' => __( 'Title:', 'jobify' )
			),
			'number' => array(
				'type'  => 'number',
				'step'  => 1,
				'min'   => 1,
				'max'   => '',
				'std'   => 5,
				'label' => __( 'Number of jobs to show:', 'jobify' )
			),
			'spotlight' => array(
				'type'  => 'checkbox',
				'std'   => 1,
				'label' => __( 'Display the "Job Spotlight" section', 'jobify' )
			),
			'spotlight-title' => array(
				'type'  => 'text',
				'std'   => __( 'Job Spotlight', 'jobify' ),
				'label' => __( 'Spotlight Title:', 'jobify' )
			),
		);

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$title           = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$number          = $instance[ 'number' ];
		$has_spotlight   = $instance[ 'spotlight' ];
		$spotlight_title = isset( $instance[ 'spotlight-title' ] ) ? esc_attr( $instance[ 'spotlight-title' ] ) : __( 'Job Spotlight', 'jobify' );

		if ( $has_spotlight ) {
			$spotlight = new WP_Query( array(
				'post_type' => 'job_listing',
				'post_status' => 'publish',
				'meta_query' => array(
					array(
						'key'   => '_featured',
						'value' => 1
					)
				),
				'posts_per_page' => 36,
				'orderby' => 'rand',
				'no_found_rows' => true,
				'update_post_term_cache' => false,
				'update_post_meta_cache' => false,
			) );
		}

		echo $before_widget;
		?>

			<div class="container">
				<?php $count = 0; ?>

				<?php if ( $has_spotlight && $spotlight->have_posts() ): ?>
				<div class="job-spotlight">
					<h3 class="homepage-widget-title"><?php echo esc_attr( $spotlight_title ); ?></h3>

					<?php
						while ( $spotlight->have_posts() ) : $spotlight->the_post();
							add_filter( 'excerpt_length', array( $this, 'excerpt_length' ) );
					?>	
						<?php if ($count == 0 || $count == 6 || $count == 12 || $count == 18 || $count == 24 || $count == 30 ) : ?>
							<div class="recent-box">
						<?php endif; ?>
						<?php get_template_part( 'content', 'single-job-featured' ); ?>
						<?php remove_filter( 'excerpt_length', array( $this, 'excerpt_length' ) ); ?>

						<?php if ($count == 5 || $count == 11 || $count == 17 || $count == 23 || $count == 29 || $count == 35 ||  $count == $spotlight->post_count ) : ?>
							</div>
						<?php endif; ?>
						<?php $count++ ?>
						<?php endwhile;
					?>
				</div>
				
				<?php endif; ?>
			</div>

		<?php
		echo $after_widget;

		$content = apply_filters( 'jobify_widget_jobs', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}

	public function excerpt_length() {
		return 20;
	}
}
