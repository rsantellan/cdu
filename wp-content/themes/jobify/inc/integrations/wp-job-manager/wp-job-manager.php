<?php
/**
 * WP Job Manager
 */

/**
 * Sets up theme support.
 *
 * @since Jobify 1.7.0
 *
 * @return void
 */
function jobify_setup_job_manager() {
	add_theme_support( 'job-manager-templates' );
}
add_action( 'after_setup_theme', 'jobify_setup_job_manager' );

/**
 * Registers widgets, and widget areas.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_wp_job_manager_widgets_init() {
	unregister_widget( 'WP_Job_Manager_Widget_Recent_Jobs' );

	register_widget( 'Jobify_Widget_Job_Company_Logo' );
	register_widget( 'Jobify_Widget_Job_Type' );
	register_widget( 'Jobify_Widget_Job_Apply' );
	register_widget( 'Jobify_Widget_Job_Company_Social' );
	register_widget( 'Jobify_Widget_Job_Categories' );
	register_widget( 'Jobify_Widget_Job_More_Jobs' );
	register_widget( 'Jobify_Widget_Job_Share' );

	register_widget( 'Jobify_Widget_Jobs' );
	register_widget( 'Jobify_Widget_Jobs_Search' );
	register_widget( 'Jobify_Widget_Stats' );
	register_widget( 'Jobify_Widget_Map' );

	if ( class_exists( 'WP_Job_Manager_Job_Tags' ) ) {
		register_widget( 'Jobify_Widget_Job_Tags' );
	}

	if ( class_exists( 'WP_Job_Manager_Application_Deadline' ) ) {
		register_widget( 'Jobify_Widget_Job_Deadline' );
	}

	if ( 'side' == jobify_theme_mod( 'jobify_listings', 'jobify_listings_display_area' ) ) {
		register_sidebar( array(
			'name'          => __( 'Job Page Sidebar', 'jobify' ),
			'id'            => 'sidebar-single-job_listing',
			'description'   => __( 'Choose what should display on single job listings.', 'jobify' ),
			'before_widget' => '<aside id="%1$s" class="job_listing-widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="job_listing-widget-title">',
			'after_title'   => '</h3>',
		) );
	} else {
		$columns = jobify_theme_mod( 'jobify_listings', 'jobify_listings_topbar_columns' );

		for ( $i = 1; $i <= $columns; $i++ ) {
			register_sidebar( array(
				'name'          => sprintf( __( 'Job Info Column %s', 'jobify' ), $i ),
				'id'            => sprintf( 'single-job_listing-top-%s', $i ),
				'description'   => sprintf( __( 'Choose what should display on single job listings column #%s.', 'jobify' ), $i ),
				'before_widget' => '<aside id="%1$s" class="job_listing-widget-top %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="job_listing-widget-title-top">',
				'after_title'   => '</h3>',
			) );
		}
	}
}
add_action( 'widgets_init', 'jobify_wp_job_manager_widgets_init' );

/**
 * Preview view override
 *
 * @since Jobify 1.6.0
 *
 * @param array $steps
 * @return array $steps
 */
function jobify_submit_job_steps( $steps ) {
	$steps[ 'preview' ][ 'view' ] = 'jobify_preview_handler';

	return $steps;
}
add_filter( 'submit_job_steps', 'jobify_submit_job_steps' );

/**
 * Preview View
 *
 * The default "Preview" view in WP Job Manager adds a little extra and doesn't
 * use the exact template file that displays what we need. Override it here.
 *
 * @since Jobify 1.6.0
 *
 * @return void
 */
function jobify_preview_handler() {
	global $job_manager, $post;

	$job_id = WP_Job_Manager_Form_Submit_Job::get_job_id();

	if ( $job_id ) {

		$post = get_post( $job_id );
		setup_postdata( $post );

		?>
		<form method="post" id="job_preview">
			<div class="job_listing_preview_title">
				<input type="submit" name="continue" id="job_preview_submit_button" class="button" value="<?php echo apply_filters( 'submit_job_step_preview_submit_text', __( 'Submit Listing &rarr;', 'jobify' ) ); ?>" />
				<input type="submit" name="edit_job" class="button" value="<?php esc_attr_e( '&larr; Edit listing', 'jobify' ); ?>" />
				<input type="hidden" name="job_id" value="<?php echo esc_attr( $job_id ); ?>" />
				<input type="hidden" name="step" value="<?php echo esc_attr( WP_Job_Manager_Form_Submit_Job::get_step() ); ?>" />
				<input type="hidden" name="job_manager_form" value="<?php echo WP_Job_Manager_Form_Submit_Job::$form_name; ?>" />
			</div>
			<?php get_job_manager_template_part( 'content-single', 'job' ); ?>
		</form>
		<?php

		wp_reset_postdata();
	}
}

/**
 * Job Listing post type arguments.
 *
 * @since Jobify 1.0.0
 *
 * @param array $args
 * @return array $args
 */
function jobify_register_post_type_job_listing( $args ) {
	$args[ 'supports' ] = array( 'title', 'editor', 'custom-fields', 'thumbnail' );

	return $args;
}
add_filter( 'register_post_type_job_listing', 'jobify_register_post_type_job_listing' );

/**
 * When viewing a taxonomy archive, use the same template for all.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_job_archives() {
	global $wp_query;

	$taxonomies = array(
		'job_listing_category',
		'job_listing_region',
		'job_listing_type',
		'job_listing_tag'
	);

	if ( ! is_tax( $taxonomies ) )
		return;

	$taxonomy = $wp_query->query_vars['taxonomy'];

	if($taxonomy == 'job_listing_type') {
		locate_template( array( 'taxonomy-job_listing_type.php' ), true ); 
	}
	else {
		locate_template( array( 'taxonomy-job_listing_category.php' ), true );
	}

	exit();
}
add_action( 'template_redirect', 'jobify_job_archives' );

/**
 * When viewing a taxonomy archive, make sure the job manager settings are respected.
 *
 * @since Jobify 1.0
 *
 * @param $query
 * @return $query
 */
function jobify_job_archives_query( $query ) {
	if ( is_admin() || ! $query->is_main_query() )
			return;

	$taxonomies = array(
		'job_listing_category',
		'job_listing_region',
		'job_listing_type',
		'job_listing_tag'
	);

	if ( is_tax( $taxonomies ) ) {
		$query->set( 'posts_per_page', get_option( 'job_manager_per_page' ) );
		$query->set( 'post_type', array( 'job_listing' ) );
		$query->set( 'post_status', array( 'publish' ) );

		if ( get_option( 'job_manager_hide_filled_positions' ) == 1 ) {
			$query->set( 'meta_query', array(
				array(
					'key'     => '_filled',
					'value'   => '1',
					'compare' => '!='
				)
			) );
		}
	}

	return $query;
}
add_filter( 'pre_get_posts', 'jobify_job_archives_query' );

/**
 * When updating a post remove the cached meta information.
 *
 * @since Jobify 1.4.2
 */
function jobify_clear_location_cache() {
	global $post;

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	if ( ! is_object( $post ) )
		return;

	if ( 'job_listing' != $post->post_type )
		return;

	if ( ! current_user_can( 'edit_post', $post->ID ) )
		return;

	if ( $post->job_cords ) {
		delete_post_meta( $post->ID, 'job_cords' );
	}
}
add_action( 'save_post', 'jobify_clear_location_cache' );

/**
 * Add extra fields to the submission form.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_submit_job_form_fields( $fields ) {
	$fields[ 'company' ][ 'company_website' ][ 'priority' ] = 4.2;

	$fields[ 'company' ][ 'company_description' ] = array(
		'label'       => _x( 'Description', 'company description on submission form', 'jobify' ),
		'type'        => 'wp-editor',
		'required'    => false,
		'placeholder' => '',
		'priority'    => 3.5
	);

	$fields[ 'company' ][ 'company_facebook' ] = array(
		'label'       => __( 'Facebook username', 'jobify' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => __( 'yourcompany', 'jobify' ),
		'priority'    => 4.5
	);
	
	
	$fields[ 'company' ][ 'company_facebook2' ] = array(
		'label'       => __( 'Facebook2 username', 'jobify' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => __( 'yourcompany', 'jobify' ),
		'priority'    => 4.5
	);

	$fields[ 'company' ][ 'company_google' ] = array(
		'label'       => __( 'Google+ username', 'jobify' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => __( 'yourcompany', 'jobify' ),
		'priority'    => 4.5
	);

	$fields[ 'company' ][ 'company_linkedin' ] = array(
		'label'       => __( 'LinkedIn username', 'jobify' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => __( 'yourcompany', 'jobify' ),
		'priority'    => 4.6
	);

	$fields[ 'company' ][ 'company_pinterest' ] = array(
		'label'       => __( 'Pinterest username', 'jobify' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => __( 'yourcompany', 'jobify' ),
		'priority'    => 4.6
	);

	$fields[ 'company' ][ 'company_instagram' ] = array(
		'label'       => __( 'Instagram username', 'jobify' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => __( 'yourcompany', 'jobify' ),
		'priority'    => 4.6
	);
	
	
	
	$fields[ 'company' ][ 'company_beahnce' ] = array(
		'label'       => __( 'Behance username', 'jobify' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => __( 'yourcompany', 'jobify' ),
		'priority'    => 4.6
	);
	
	
	
	$fields[ 'company' ][ 'company_foursquare' ] = array(
		'label'       => __( 'foursquare username', 'jobify' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => __( 'yourcompany', 'jobify' ),
		'priority'    => 4.6
	);
	
	
	$fields[ 'company' ][ 'company_youtube' ] = array(
		'label'       => __( 'Youtube username', 'jobify' ),
		'type'        => 'text',
		'required'    => false,
		'placeholder' => __( 'yourcompany', 'jobify' ),
		'priority'    => 4.6
	);
	
	
	

	return $fields;
}
add_filter( 'submit_job_form_fields', 'jobify_submit_job_form_fields' );

/**
 * Save the extra frontend fields
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_job_manager_update_job_data( $job_id, $values ) {

	update_post_meta( $job_id, '_company_description', $values[ 'company' ][ 'company_description' ] );
	update_post_meta( $job_id, '_company_facebook', $values[ 'company' ][ 'company_facebook' ] );
		update_post_meta( $job_id, '_company_facebook2', $values[ 'company' ][ 'company_facebook2' ] );
	update_post_meta( $job_id, '_company_google', $values[ 'company' ][ 'company_google' ] );
	update_post_meta( $job_id, '_company_linkedin', $values[ 'company' ][ 'company_linkedin' ] );
	update_post_meta( $job_id, '_company_pinterest', $values[ 'company' ][ 'company_pinterest' ] );
	update_post_meta( $job_id, '_company_instagram', $values[ 'company' ][ 'company_instagram' ] );
	update_post_meta( $job_id, '_company_behance', $values[ 'company' ][ 'company_behance' ] );
	update_post_meta( $job_id, '_company_foursquare', $values[ 'company' ][ 'company_foursquare' ] );
	update_post_meta( $job_id, '_company_youtube', $values[ 'company' ][ 'company_youtube' ] );

	if ( is_user_logged_in() ) {
		update_user_meta( get_current_user_id(), '_company_description', $values[ 'company' ][ 'company_description' ] );
		update_user_meta( get_current_user_id(), '_company_facebook', $values[ 'company' ][ 'company_facebook2' ] );
		update_user_meta( get_current_user_id(), '_company_facebook2', $values[ 'company' ][ 'company_facebook2' ] );
		update_user_meta( get_current_user_id(), '_company_google', $values[ 'company' ][ 'company_google' ] );
		update_user_meta( get_current_user_id(), '_company_linkedin', $values[ 'company' ][ 'company_linkedin' ] );
		update_post_meta( get_current_user_id(), '_company_pinterest', $values[ 'company' ][ 'company_pinterest' ] );
		update_post_meta( get_current_user_id(), '_company_instagram', $values[ 'company' ][ 'company_instagram' ] );
		update_post_meta( get_current_user_id(), '_company_behance', $values[ 'company' ][ 'company_behance' ] );	
		update_post_meta( get_current_user_id(), '_company_foursquare', $values[ 'company' ][ 'company_foursquare' ] );	
		update_post_meta( get_current_user_id(), '_company_youtube', $values[ 'company' ][ 'company_youtube' ] );		
	}
}
add_action( 'job_manager_update_job_data', 'jobify_job_manager_update_job_data', 10, 2 );

/**
 * Add extra fields to the WordPress admin.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_wp_job_manager_job_listing_data_fields( $fields ) {
	$fields[ '_company_description' ] = array(
		'label' => __( 'Company Description', 'jobify' ),
		'placeholder' => '',
		'type'        => 'textarea'
	);

	$fields[ '_company_facebook' ] = array(
		'label' => __( 'Facebook', 'jobify' ),
		'placeholder' => ''
	);

$fields[ '_company_facebook2' ] = array(
		'label' => __( 'Facebook2', 'jobify' ),
		'placeholder' => ''
	);


	$fields[ '_company_google' ] = array(
		'label' => __( 'Google+', 'jobify' ),
		'placeholder' => ''
	);

	$fields[ '_company_linkedin' ] = array(
		'label' => __( 'LinkedIn', 'jobify' ),
		'placeholder' => ''
	);

	$fields[ '_company_pinterest' ] = array(
		'label' => __( 'Pinterest', 'jobify' ),
		'placeholder' => ''
	);

	$fields[ '_company_instagram' ] = array(
		'label' => __( 'Instagram', 'jobify' ),
		'placeholder' => ''
	);
	
	$fields[ '_company_behance' ] = array(
		'label' => __( 'Behance', 'jobify' ),
		'placeholder' => ''
	);
	
	
	$fields[ '_company_foursquare' ] = array(
		'label' => __( 'Foursquare', 'jobify' ),
		'placeholder' => ''
	);
	
	$fields[ '_company_youtube' ] = array(
		'label' => __( 'Youtube', 'jobify' ),
		'placeholder' => ''
	);
	

	return $fields;
}
add_filter( 'job_manager_job_listing_data_fields', 'jobify_wp_job_manager_job_listing_data_fields' );

/**
 * Save the extra admin fields.
 *
 * WP Job Manager strips our tags out. Resave it after with the tags.
 *
 * @since Jobify 1.4.4
 *
 * @return void
 */
function jobify_job_manager_save_job_listing( $job_id, $post ) {
	update_post_meta( $job_id, '_company_description', wp_kses_post( $_POST[ '_company_description' ] ) );
}
add_action( 'job_manager_save_job_listing', 'jobify_job_manager_save_job_listing', 10, 2 );

/**
 * The Company Description template tag.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_the_company_description( $before = '', $after = '', $echo = true ) {
	$company_description = jobify_get_the_company_description();

	if ( strlen( $company_description ) == 0 )
		return;

	$company_description = wp_kses_post( $company_description );
	$company_description = $before . wpautop( $company_description ) . $after;

	if ( $echo )
		echo $company_description;
	else
		return $company_description;
}

/**
 * Get the company description.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_get_the_company_description( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	return apply_filters( 'the_company_description', $post->_company_description, $post );
}

/**
 * Trim the job location output on all pages except the actual listing.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_the_job_location( $location ) {
	if ( is_singular( 'job_listing' ) )
		return $location;

	$location = wp_trim_words( $location, 3, '' );

	return $location;
}
add_filter( 'the_job_location', 'jobify_the_job_location' );

/**
 * Get the Company Facebook
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_get_the_company_facebook( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	$company_facebook = $post->_company_facebook;

	if ( strlen( $company_facebook ) == 0 )
		return;

	return apply_filters( 'the_company_facebook', $company_facebook, $post );
}



function jobify_get_the_company_facebook2( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	$company_facebook2 = $post->_company_facebook2;

	if ( strlen( $company_facebook2 ) == 0 )
		return;

	return apply_filters( 'the_company_facebook2', $company_facebook2, $post );
}

/**
 * Get the Company Google Plus
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_get_the_company_gplus( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	$company_google = $post->_company_google;

	if ( strlen( $company_google ) == 0 )
		return;

	return apply_filters( 'the_company_google', $company_google, $post );
}

/**
 * Get the Company LinkedIn
 *
 * @since Jobify 1.6.0
 *
 * @return void
 */
function jobify_get_the_company_linkedin( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	$company_linkedin = $post->_company_linkedin;

	if ( strlen( $company_linkedin ) == 0 )
		return;

	return apply_filters( 'the_company_linkedin', $company_linkedin, $post );
}

function jobify_get_the_company_pinterest( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	$company_pinterest = $post->_company_pinterest;

	if ( strlen( $company_pinterest ) == 0 )
		return;

	return apply_filters( 'the_company_pinterest', $company_pinterest, $post );
}

function jobify_get_the_company_instagram( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	$company_instagram = $post->_company_instagram;

	if ( strlen( $company_instagram ) == 0 )
		return;

	return apply_filters( 'the_company_instagram', $company_instagram, $post );
}


function jobify_get_the_company_behance( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	$company_behance = $post->_company_behance;

	if ( strlen( $company_behance ) == 0 )
		return;

	return apply_filters( 'the_company_behance', $company_behance, $post );
}




function jobify_get_the_company_foursquare( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	$company_foursquare = $post->_company_foursquare;

	if ( strlen( $company_foursquare ) == 0 )
		return;

	return apply_filters( 'the_company_foursquare', $company_foursquare, $post );
}




function jobify_get_the_company_youtube( $post = 0 ) {
	$post = get_post( $post );

	if ( $post->post_type !== 'job_listing' )
		return;

	$company_youtube = $post->_company_youtube;

	if ( strlen( $company_youtube ) == 0 )
		return;

	return apply_filters( 'the_company_youtube', $company_youtube, $post );
}



/**
 * Login Form Shortcode
 *
 * @since Jobify 1.0
 *
 * @return $form HTML form.
 */
function jobify_shortcode_login_form() {
	ob_start();

	wp_login_form( apply_filters( 'jobify_shortcode_login_form', array(
		'label_log_in' => _x( 'Sign In', 'login for submit label', 'jobify' ),
		'value_remember' => true,
		'redirect' => home_url()
	) ) );

	$form = ob_get_clean();

	return $form;
}
add_shortcode( 'jobify_login_form', 'jobify_shortcode_login_form' );

/**
 * Add a "Forgot Password" link to the login form
 *
 * @since Jobify 1.0
 *
 * @return $output HTML output
 */
function jobify_login_form_middle( $output ) {
	$output .= sprintf( '<p class="has-account"><i class="icon-help-circled"></i> <a href="%s">%s</a></p>', wp_lostpassword_url(), __( 'Forgot Password?', 'jobify' ) );

	return $output;
}
add_filter( 'login_form_middle', 'jobify_login_form_middle' );

function jobify_login_form_top( $output ) {
	if ( isset ( $_GET[ 'login' ] ) && 'failed' == $_GET[ 'login' ] ) {
		$output .= '<div class="job-manager-error">' . __( 'Please try again.', 'jobify' ) . '</div>';
	}

	return $output;
}
add_filter( 'login_form_top', 'jobify_login_form_top' );

/**
 * Register Form Shortcode
 *
 * @since Jobify 1.0
 *
 * @return $form HTML form.
 */
function jobify_shortcode_register_form() {
	ob_start();

	if ( ! class_exists( 'WP_Job_Manager_Form' ) )
		include_once( JOB_MANAGER_PLUGIN_DIR . '/includes/abstracts/abstract-wp-job-manager-form.php' );

	include_once( get_template_directory() . '/inc/integrations/wp-job-manager/wp-job-manager-form-register.php' );

	WP_Job_Manager_Form_Register::output();

	$form = ob_get_clean();

	return $form;
}
add_shortcode( 'jobify_register_form', 'jobify_shortcode_register_form' );

/**
 * Posted Register Form
 *
 * @since Jobify 1.0
 *
 * @return $form HTML form.
 */
function jobify_load_posted_form() {
	if ( ! empty( $_POST['job_manager_form'] ) ) {
		$form        = esc_attr( $_POST['job_manager_form'] );

		$form_class  = 'WP_Job_Manager_Form_' . str_replace( '-', '_', $form );
		$form_file   = get_template_directory() . '/inc/integrations/wp-job-manager/wp-job-manager-form-' . $form . '.php';

		if ( class_exists( $form_class ) )
			return $form_class;

		if ( ! file_exists( $form_file ) )
			return false;

		if ( ! class_exists( $form_class ) )
			include $form_file;

		call_user_func( array( $form_class, "init" ) );
	}
}
add_action( 'init', 'jobify_load_posted_form' );

/**
 * Clear shortcode options when a post is saved.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_clear_page_shortcode() {
	$shortcodes = array(
		'login_form',
		'register_form',
		'jobify_login_form',
		'jobify_register_form'
	);

	foreach ( $shortcodes as $shortcode ) {
		delete_option( 'job_manager_page_' . $shortcode );
	}
}
add_action( 'save_post', 'jobify_clear_page_shortcode' );

/**
 * Add a submit button the filtering options.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_job_manager_job_filters_after() {
?>
	<div class="col-md-1 col-sm-12">
		<input type="submit" name="submit" value="<?php echo esc_attr_e( 'Search', 'jobify' ); ?>" />
	</div>
	<div class="col-md-1 col-sm-12">
		<a id="question-link" class="fixedTip" title="En el buscador se podrán filtrar diferentes datos para hacer una búsqueda específicia en el Directorio.
        Las opciones permiten filtrar por Categorías, Sub-Categorías (dentro de las anteriores) y por Tipo de organización. 
        Asimismo podrá ingresar en el último campo el Nombre o Código numérico de la empresa o cualquier palabra que quiera filtrar." href="javascript:void(0)">?</a>
	</div>
<?php
}
add_action( 'job_manager_job_filters_search_jobs_end', 'jobify_job_manager_job_filters_after', 9 );
add_action( 'resume_manager_resume_filters_search_resumes_end', 'jobify_job_manager_job_filters_after', 9 );

/**
 * Job/Resume login page.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_submit_job_form_login_url( $url ) {
	$page = jobify_find_page_with_shortcode( array( 'jobify_login_form', 'login_form' ) );

	if ( ! $page ) {
		return $url;
	}

	return get_permalink( $page );
}
add_filter( 'submit_job_form_login_url', 'jobify_submit_job_form_login_url' );
add_filter( 'submit_resume_form_login_url', 'jobify_submit_job_form_login_url' );

/** Apply */
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/wp-job-manager-apply.php' );

/** Map */
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/class-map-interactive.php' );

/** Widgets */
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-job-company-logo.php' );
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-job-type.php' );
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-job-apply.php' );
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-job-company-social.php' );
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-job-categories.php' );
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-job-more-jobs.php' );
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-job-share.php' );

if ( class_exists( 'WP_Job_Manager_Application_Deadline' ) ) {
	require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-job-deadline.php' );
}

if ( class_exists( 'WP_Job_Manager_Job_Tags' ) ) {
	require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-job-tags.php' );
}

require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-jobs-recent.php' );
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-jobs-search.php' );
require_once( get_template_directory() . '/inc/integrations/wp-job-manager/widgets/class-widget-jobs-map.php' );