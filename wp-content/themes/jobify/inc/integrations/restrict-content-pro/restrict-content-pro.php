<?php
/**
 * Restrict Content Pro
 */

/**
 * Registers widgets, and widget areas for RCP
 *
 * @since Jobify 1.7.0
 *
 * @return void
 */
function jobify_rcp_widgets_init() {
	register_widget( 'Jobify_Widget_Price_Table_RCP' );
}
add_action( 'widgets_init', 'jobify_rcp_widgets_init' );

/**
 * @unknown
 *
 * @since unknown
 *
 * @return string
 */
function jobify_rcp_subscription_teaser() {
	global $rcp_options;

	return apply_filters( 'jobify_rcp_paid_message_widget', $rcp_options[ 'paid_message' ] );
}

/**
 * Filter Jobify widget output depending on RCP subscription level.
 *
 * @since Jobify 1.6.0
 *
 * @return $widget
 */
function jobify_rcp_job_widget( $widget, $instance, $args ) {
	extract( $args );

	if ( ! isset( $instance[ 'subscription' ] ) ) {
		return $widget;
	}

	$sub_level = maybe_unserialize( $instance[ 'subscription' ] );

	if ( ! is_array( $sub_level ) )
		$sub_level = array();

	if ( ! in_array( rcp_get_subscription_id( get_current_user_id() ), $sub_level ) && ! empty( $sub_level ) ) {
		$widget = $before_widget . jobify_rcp_subscription_teaser() . $after_widget;
	}

	return $widget;
}
add_filter( 'jobify_job_widget', 'jobify_rcp_job_widget', 10, 3 );

/**
 * Widgets
 */
require_once( get_template_directory() . '/inc/integrations/restrict-content-pro/widgets/class-widget-price-table-rcp.php' );