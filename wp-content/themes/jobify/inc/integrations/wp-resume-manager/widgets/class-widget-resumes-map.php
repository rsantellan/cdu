<?php
/**
 * Home: Resumes Map
 *
 * @since Jobify 1.0
 */
class Jobify_Widget_Resumes_Map extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		new Jobify_Widget_Map_Interactive( array(
			'type' => 'resume'
		) );

		$this->widget_cssclass    = 'jobify_widget_map_resumes';
		$this->widget_description = __( 'Display a map with pins indicating areas with active resume listings.', 'jobify' );
		$this->widget_id          = 'jobify_widget_map_resumes';
		$this->widget_name        = __( 'Jobify - Home: Resumes Map', 'jobify' );
		$this->settings           = array(
			'search' => array(
				'type'  => 'checkbox',
				'std'   => 1,
				'label' => __( 'Display search/filtering options', 'jobify' )
			),
			'scrollWheel' => array(
				'label' => __( 'Zoom with Scroll Wheel', 'jobify' ),
				'type'  => 'checkbox',
				'std'   => 'off'
			),
			'pins' => array(
				'type'  => 'number',
				'std'   => 20,
				'min'   => 0,
				'max'   => 200,
				'step'  => 5,
				'label' => __( 'Number of pins to load', 'jobify' )
			),
			'clusterRadius' => array(
				'label' => __( 'Cluster Radius', 'jobify' ),
				'type'  => 'number',
				'min'   => 0,
				'max'   => 1000,
				'std'   => 200
			),
			'clusterColor' => array(
				'label' => __( 'Cluster Color', 'jobify' ),
				'type'  => 'colorpicker',
				'std'   => '#01da90'
			),
			'zoom' => array(
				'type'  => 'select',
				'std'   => 'auto',
				'label' => __( 'Zoom Level:', 'jobify' ),
				'options' => array(
					'auto' => __( 'Auto', 'jobify' ),
					'1'      => 1,
					'2'      => 2,
					'3'      => 3,
					'4'      => 4,
					'5'      => 5,
					'6'      => 6,
					'7'      => 7,
					'8'      => 8,
					'9'      => 9,
					'10'     => 10,
					'11'     => 11,
					'12'     => 12,
					'13'     => 13,
					'14'     => 14,
					'15'     => 15,
					'16'     => 16,
					'17'     => 17
				)
			),
			'center' => array(
				'type'    => 'text',
				'label'   => __( 'Center Coordinates (optional):', 'jobify' ),
				'std'     => ''
			),
			'desc'  => array(
				'type'    => 'description',
				'std'     => __( 'Coordinates must be set to respect zoom level. Otherwise the map will automatically be set to the bounds of the latest jobs.', 'jobify' )
			)
		);
		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$search = '' == $instance[ 'search' ] ? false : 'show';
		$pins   = isset( $instance[ 'pins' ] ) ? absint( $instance[ 'pins' ] ) : 20;
		$scroll_wheel = ( isset( $instance[ 'scrollWheel' ] ) && 1 == $instance[ 'scrollWheel' ] ) ? 'true' : 'false';
		$cluster_radius = isset( $instance[ 'clusterRadius' ] ) ? absint( $instance[ 'clusterRadius' ] ) : 200;
		$cluster_color = isset( $instance[ 'clusterColor' ] ) ? $instance[ 'clusterColor' ] : jobify_theme_mod( 'colors', 'primary' );
		$zoom = isset( $instance[ 'zoom' ] ) ? $instance[ 'zoom' ] : 'auto';
		$center = isset( $instance[ 'center' ] ) && '' != $instance[ 'center' ] ? array_map( 'trim', explode( ',', $instance[ 'center' ] ) ) : 'autofit';

		echo $before_widget;
		?>

			<div id="map-canvas-wrap">
				<div class="map-filter animated fadeInUp">
					<div class="live-map">
						<?php
							get_job_manager_template(
								'resume-filters.php',
								array(
									'show_categories' => get_option( 'resume_manager_enable_categories' ),
									'categories' => array(),
									'atts' => array()
								),
								'resume_manager',
								RESUME_MANAGER_PLUGIN_DIR . '/templates/'
							);
						?>
					</div>
				</div>

				<div id="jobify-map-canvas-resumes" class="map-canvas"></div>
			</div>
			<script>
				jQuery( document ).ready(function($) {
					var map = jobifyMap();

					map.init({
						widget        : '.<?php echo $this->widget_id; ?>',
						canvas        : '#jobify-map-canvas-resumes',
						type          : 'resume',
						inString      : '<?php _e( '%s Resumes Found', 'jobify' ); ?>',
						showSearch    : '<?php echo $search; ?>',
						scrollWheel   : <?php echo $scroll_wheel; ?>,
						clusterRadius : <?php echo $cluster_radius; ?>,
						clusterColor  : '<?php echo $cluster_color; ?>',
						zoom          : '<?php echo $zoom; ?>',
						center        : <?php echo is_array( $center ) ? json_encode( $center ) : "'" . $center . "'"; ?>
					});
				});
			</script>

		<?php
		echo $after_widget;

		$content = apply_filters( 'jobify_widget_map_resumes', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}
}