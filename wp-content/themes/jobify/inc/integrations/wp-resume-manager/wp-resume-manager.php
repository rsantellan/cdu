<?php
/**
 * Resume Manager
 */

/**
 * Sets up theme support.
 *
 * @since Jobify 1.7.0
 *
 * @return void
 */
function jobify_setup_resume_manager() {
	add_theme_support( 'resume-manager-templates' );
}
add_action( 'after_setup_theme', 'jobify_setup_resume_manager' );

/**
 * Registers widgets, and widget areas.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_wp_resume_manager_widgets_init() {
	register_widget( 'Jobify_Widget_Resumes' );
	register_widget( 'Jobify_Widget_Resumes_Map' );

	register_widget( 'Jobify_Widget_Resume_Links' );
	register_widget( 'Jobify_Widget_Resume_Categories' );

	if ( get_option( 'resume_manager_enable_skills' ) ) {
		register_widget( 'Jobify_Widget_Resume_Skills' );
	}

	if ( get_option( 'resume_manager_enable_resume_upload' ) ) {
		register_widget( 'Jobify_Widget_Resume_File' );
	}

	if ( 'side' == jobify_theme_mod( 'jobify_listings', 'jobify_listings_display_area' ) ) {
		register_sidebar( array(
			'name'          => __( 'Resume Page Sidebar', 'jobify' ),
			'id'            => 'sidebar-single-resume',
			'description'   => __( 'Choose what should display on single resume listings.', 'jobify' ),
			'before_widget' => '<aside id="%1$s" class="job_listing-widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="job_listing-widget-title">',
			'after_title'   => '</h3>',
		) );
	} else {
		$columns = jobify_theme_mod( 'jobify_listings', 'jobify_listings_topbar_columns' );

		for ( $i = 1; $i <= $columns; $i++ ) {
			register_sidebar( array(
				'name'          => sprintf( __( 'Resume Info Column %s', 'jobify' ), $i ),
				'id'            => sprintf( 'single-resume-top-%s', $i ),
				'description'   => sprintf( __( 'Choose what should display on resume listings column #%s.', 'jobify' ), $i ),
				'before_widget' => '<aside id="%1$s" class="job_listing-widget-top %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="resume-widget-title-top">',
				'after_title'   => '</h3>',
			) );
		}
	}
}
add_action( 'widgets_init', 'jobify_wp_resume_manager_widgets_init' );

/**
 * Resume post type arguments.
 *
 * @since Jobify 1.5.0
 *
 * @param array $args
 * @return array $args
 */
function jobify_register_post_type_resume( $args ) {
	$args[ 'exclude_from_search' ] = false;

	return $args;
}
add_filter( 'register_post_type_resume', 'jobify_register_post_type_resume' );

/**
 * When viewing a taxonomy archive, use the same template for all.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_resume_archives() {
	global $wp_query;

	$taxonomies = array(
		'resume_skill'
	);

	if ( ! is_tax( $taxonomies ) )
		return;

	locate_template( array( 'taxonomy-resume_category.php' ), true );

	exit();
}
add_action( 'template_redirect', 'jobify_resume_archives' );

/**
 * When viewing a taxonomy archive, make sure the job manager settings are respected.
 *
 * @since Jobify 1.0
 *
 * @param $query
 * @return $query
 */
function jobify_resume_archives_query( $query ) {
	if ( is_admin() || ! $query->is_main_query() )
			return;

	$taxonomies = array(
		'resume_category'
	);

	if ( is_tax( $taxonomies ) ) {
		$query->set( 'posts_per_page', get_option( 'job_manager_per_page' ) );
		$query->set( 'post_type', array( 'resume' ) );
		$query->set( 'post_status', array( 'publish' ) );
	}

	return $query;
}
add_filter( 'pre_get_posts', 'jobify_resume_archives_query' );

/**
 * Output resume email links (until this is moved into a separate template file)
 *
 * @since Jobify 1.7.0
 *
 * @return void
 */
function jobify_resume_manager_application_details_email_std() {
	global $post;

	$email   = get_post_meta( $post->ID, '_candidate_email', true );
	$subject = sprintf( __( 'Contact via the resume for "%s" on %s', 'jobify' ), $post->post_title, home_url() );

	echo '<p>' . sprintf( __( 'To contact this candidate <strong>email</strong> <a class="job_application_email" href="mailto:%1$s%2$s">%1$s</a>', 'jobify' ), $email, '?subject=' . rawurlencode( $subject ) ) . '</p>';

	echo '<p>' . __( 'Contact using webmail: ', 'jobify' );

	echo '<a href="' . 'https://mail.google.com/mail/?view=cm&fs=1&to=' . $email . '&su=' . urlencode( $subject ) .'" target="_blank" class="job_application_email">Gmail</a> / ';

	echo '<a href="' . 'http://webmail.aol.com/Mail/ComposeMessage.aspx?to=' . $email . '&subject=' . urlencode( $subject ) .'" target="_blank" class="job_application_email">AOL</a> / ';

	echo '<a href="' . 'http://compose.mail.yahoo.com/?to=' . $email . '&subject=' . urlencode( $subject ) .'" target="_blank" class="job_application_email">Yahoo</a> / ';

	echo '<a href="' . 'http://mail.live.com/mail/EditMessageLight.aspx?n=&to=' . $email . '&subject=' . urlencode( $subject ) .'" target="_blank" class="job_application_email">Outlook</a>';

	echo '</p>';
}

/** Widgets */
require_once( get_template_directory() . '/inc/integrations/wp-resume-manager/widgets/class-widget-resumes-recent.php' );
require_once( get_template_directory() . '/inc/integrations/wp-resume-manager/widgets/class-widget-resumes-map.php' );

require_once( get_template_directory() . '/inc/integrations/wp-resume-manager/widgets/class-widget-resume-links.php' );
require_once( get_template_directory() . '/inc/integrations/wp-resume-manager/widgets/class-widget-resume-categories.php' );
require_once( get_template_directory() . '/inc/integrations/wp-resume-manager/widgets/class-widget-resume-skills.php' );
require_once( get_template_directory() . '/inc/integrations/wp-resume-manager/widgets/class-widget-resume-file.php' );