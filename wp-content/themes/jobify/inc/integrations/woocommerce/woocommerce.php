<?php
/**
 * WooCommerce by WooThemes
 */

/**
 * Sets up theme support.
 *
 * @since Jobify 1.7.0
 *
 * @return void
 */
function jobify_setup_woocommerce() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'jobify_setup_woocommerce' );

/**
 * Registers widgets, and widget areas for WooCommerce
 *
 * @since Jobify 1.7.0
 *
 * @return void
 */
function jobify_woocommerce_widgets_init() {
	register_widget( 'Jobify_Widget_Price_Table_WC' );
}
add_action( 'widgets_init', 'jobify_woocommerce_widgets_init' );

/**
 * Widgets
 */
require_once( get_template_directory() . '/inc/integrations/woocommerce/widgets/class-widget-price-table-wc.php' );