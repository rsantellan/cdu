<?php
/**
 * Testimonials by WooThemes
 */

/**
 * Registers widgets, and widget areas for Testimonials
 *
 * @since Jobify 1.7.0
 *
 * @return void
 */
function jobify_widgets_testimonials_init() {
	register_widget( 'Jobify_Widget_Companies' );
	register_widget( 'Jobify_Widget_Testimonials' );
}
add_action( 'widgets_init', 'jobify_widgets_testimonials_init' );

/**
 * Testimonials by WooThemes
 *
 * @since Jobify 1.0
 *
 * @param string $tpl
 * @return string $tpl
 */
function jobify_woothemes_testimonials_item_template( $tpl, $args ) {
	if ( 'individual' == $args[ 'category' ] ) {
		$tpl  = '<blockquote id="quote-%%ID%%" class="individual-testimonial %%CLASS%%">';
		$tpl .= '%%TEXT%%';
		$tpl .= '<cite class="individual-testimonial-author">%%AVATAR%% %%AUTHOR%%</cite>';
		$tpl .= '</blockquote>';
	} else {
		$tpl  = '<div class="company-slider-item">';
		$tpl .= '%%AVATAR%%';
		$tpl .= '</div>';
	}

	return $tpl;
}
add_filter( 'woothemes_testimonials_item_template', 'jobify_woothemes_testimonials_item_template', 10, 2 );

/** Widgets */
require_once( get_template_directory() . '/inc/integrations/woo-testimonials/widgets/class-widget-companies.php' );
require_once( get_template_directory() . '/inc/integrations/woo-testimonials/widgets/class-widget-testimonials.php' );