<?php if ( $apply = get_the_job_application_method() ) :
	?>
	<div class="application">
		<input class="application_button" type="button" value="<?php esc_attr_e( 'Apply', 'jobify' ); ?>" />

		<div class="application_details animated modal">
			<h2 class="modal-title"><?php _e( 'Apply', 'jobify' ); ?></h2>

			<div class="application-content">
			<?php
				/**
				 * job_manager_application_details_email or job_manager_application_details_url hook
				 */
				do_action( 'job_manager_application_details_' . $apply->type, $apply );
			?>
			</div>
		</div>
	</div>
<?php endif; ?>