<?php
/**
 * Featured Job
 *
 * @package Jobify
 * @since Jobify 1.0
 */
?>

<div class="single-job-spotlight">
	<div class="single-job-spotlight-feature-image">
		<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'content-job-featured' ); ?></a>
	</div>

	<div class="single-job-spotlight-content">
		<p><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></p>

		<?php the_excerpt(); ?>
	</div>

	<div class="single-job-spotlight-actions">
		<? $pm = get_term( $post->_job_primarycategory, 'job_listing_category' ); ?>
		<div class="action"><span class="job-category <?php echo $pm->slug; ?>"><?php echo $pm->name; ?></span></div>
		<div class="action"><span class="job-location"><a href="<?php the_job_permalink(); ?>"><?php echo __( 'Job view portfolio', 'wp-job-manager' ); ?></a></span></div>
	</div>
</div>