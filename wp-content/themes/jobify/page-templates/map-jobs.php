<?php
/**
 * Template Name: Map + Jobs
 *
 * @package Jobify
 * @since Jobify 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="homepage-content" role="main">
			<?php
				the_widget(
					'Jobify_Widget_Map',
					apply_filters( 'jobify_widget_map_page_template', array(
						'search'        => '',
						'pins'          => 20,
						'scrollWheel'   => false,
						'clusterColor'  => jobify_theme_mod( 'colors', 'primary' ),
						'clusterRadius' => 200,
						'zoom'          => 'auto',
						'center'        => 'autofit',
					) ),
					array(
						'before_widget' => sprintf( '<section id="%1$s" class="homepage-widget %2$s">', 'jobify_widget_map', 'jobify_widget_map' ),
						'after_widget'  => '</section>',
						'before_title'  => '<h3 class="homepage-widget-title">',
						'after_title'   => '</h3>',
						'widget_id' => 'jobify_widget_map-999'
					)
				);
			?>

			<div class="container">
				<div class="entry-content">
					<?php while ( have_posts() ) : the_post(); ?>

						<?php if ( '' == get_post()->post_content ) : ?>

							<?php echo do_shortcode( '[jobs]' ); ?>

						<?php else : ?>

							<?php the_content(); ?>

						<?php endif; ?>

					<?php endwhile; ?>
				</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>