<?php
/**
 * Template Name: Pic + Jobs
 *
 * @package Jobify
 * @since Jobify 1.0
 */

get_header(); ?>
	<div class="application">
		<div class="dir_application_details animated modal">
			<h2 class="modal-title"><?php _e( 'Apply', 'jobify' ); ?></h2>

			<div class="application-content">
			<?php
				/**
				 * job_manager_application_details_email or job_manager_application_details_url hook
				 */
				do_action( 'job_manager_application_details_email', false );
			?>
			</div>
		</div>
	</div>
	<div id="primary" class="content-area">
		<div id="content" class="homepage-content" role="main">
			
			<?php
				the_post_thumbnail('full');
			?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php if ( '' == get_post()->post_content ) : ?>

					<?php echo do_shortcode( '[pic_jobs]' ); ?>

				<?php else : ?>

					<?php the_content(); ?>

				<?php endif; ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>