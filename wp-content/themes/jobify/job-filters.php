<?php
/**
 *
 */

$s_categories = get_option( 'job_manager_enable_categories' ) && get_terms( 'job_listing_category' );
wp_enqueue_script( 'wp-job-manager-ajax-filters' );
?>

<form class="job_filters">

	<div class="search_jobs">
		<div class="row">
			<?php do_action( 'job_manager_job_filters_search_jobs_start', $atts ); ?>

			<?php if ( $categories ) : ?>
				<?php foreach ( $categories as $category ) : ?>
					<input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( $category ); ?>" />
				<?php endforeach; ?>
			<?php elseif ( $show_categories && $s_categories && ! is_tax( 'job_listing_category' ) ) : ?>
				<div class="col-md-3 col-sm-12">
					<div class="search_categories">
						<label for="search_categories"><?php _e( 'Category', 'jobify' ); ?></label>
						<?php wp_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'depth' => 1, 'show_option_all' => __( 'Category', 'jobify' ), 'name' => 'search_categories', 'orderby' => 'name' ) ); ?>
					</div>
				</div>
			<?php endif; ?>

			<div class="col-md-2 col-sm-12">
				<div class="search_categories">
					<label for="search_subcategories"><?php _e( 'Subcategory', 'jobify' ); ?></label>
					<div class="select" id="subcat_container"><select name="search_subcategories" id="search_subcategories"><option value="0" selected="selected"><?php echo __( 'Subcategory', 'jobify' ) ?></option></select></div>
				</div>
			</div>

			<?php if ( ! is_tax( 'job_listing_type' ) && empty( $job_types ) ) : ?>
				<div class="col-md-2 col-sm-12">
					<div class="search_categories">
						<label for="search_job_types"><?php _e( 'Type', 'jobify' ); ?></label>
						<div class="select"><?php wp_dropdown_categories( array( 'taxonomy' => 'job_listing_type', 'hierarchical' => 1, 'show_option_all' => __( 'Type', 'jobify' ), 'name' => 'search_job_types', 'orderby' => 'name' ) ); ?></div>
					</div>
				</div>
			<?php elseif ( $job_types ) : ?>
				<?php foreach ( $job_types as $job_type ) : ?>
					<input type="hidden" name="search_job_types[]" value="<?php echo sanitize_title( $job_type ); ?>" />
				<?php endforeach; ?>
			<?php endif; ?>

			<div class="<?php echo $s_categories ? 'col-md-3 col-sm-12' : 'col-md-5 col-sm-6'; ?> col-xs-12 search_keywords">
				<label for="search_keywords"><?php _e( 'Keywords', 'jobify' ); ?></label>
				<input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'Keywords', 'jobify' ); ?>" value="<?php echo esc_attr( $keywords ); ?>" />
			</div>

			<!-- <div class="<?php echo $s_categories ? 'col-md-2 col-sm-12' : 'col-md-5 col-sm-6'; ?> col-xs-12 search_location">
				<label for="search_location"><?php _e( 'Location', 'jobify' ); ?></label>
				<input type="text" name="search_location" id="search_location" placeholder="<?php esc_attr_e( 'Location', 'jobify' ); ?>" value="<?php echo esc_attr( $location ); ?>" />
			</div>
 -->
			<?php do_action( 'job_manager_job_filters_search_jobs_end', $atts ); ?>
		</div>
	</div>
	<!--
	<?php if ( ! is_tax( 'job_listing_type' ) && empty( $job_types ) ) : ?>
		<ul class="job_types">
			<?php foreach ( get_job_listing_types() as $type ) : ?>
				<li><label for="job_type_<?php echo $type->slug; ?>" class="<?php echo sanitize_title( $type->name ); ?>"><input type="checkbox" name="filter_job_type[]" value="<?php echo $type->slug; ?>" <?php checked( 1, 1 ); ?> id="job_type_<?php echo $type->slug; ?>" /> <?php echo $type->name; ?></label></li>
			<?php endforeach; ?>
		</ul>
	<?php elseif ( $job_types ) : ?>
		<?php foreach ( $job_types as $job_type ) : ?>
			<input type="hidden" name="filter_job_type[]" value="<?php echo sanitize_title( $job_type ); ?>" />
		<?php endforeach; ?>
	<?php endif; ?>
	-->
	<div class="showing_jobs"></div>
</form>
