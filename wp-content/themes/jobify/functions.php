<?php




session_start();
/**
 * Jobify functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Jobify
 * @since Jobify 1.0
 */

/**
 * Sets up the content width value based on the theme's design.
 * @see jobify_content_width() for template-specific adjustments.
 */
if ( ! isset( $content_width ) )
	$content_width = 680;

/**
 * Sets up theme defaults and registers the various WordPress features that
 * Jobify supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for automatic feed links, post
 * formats, admin bar, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_setup() {
	/*
	 * Makes Jobify available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Jobify, use a find and
	 * replace to change 'jobify' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'jobify', get_template_directory() . '/languages' );

	// Editor style
	add_editor_style();

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Add support for custom background
	add_theme_support( 'custom-background', array(
		'default-color'    => '#ffffff'
	) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary'       => __( 'Navigation Menu', 'jobify' ),
		'footer-social' => __( 'Footer Social', 'jobify' )
	) );

	/** Shortcodes */
	add_filter( 'widget_text', 'do_shortcode' );

	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'content-grid', 400, 200, true );
	add_image_size( 'content-job-featured', 1350, 525, true );

	/**
	 * Misc
	 */
	add_filter( 'excerpt_more', '__return_false' );
}
add_action( 'after_setup_theme', 'jobify_setup' );

/**
 * Returns the Google font stylesheet URL, if available.
 *
 * The use of Source Sans Pro and Bitter by default is localized. For languages
 * that use characters not supported by the font, the font can be disabled.
 *
 * @since Jobify 1.0
 *
 * @return string Font stylesheet or empty string if disabled.
 */
function jobify_fonts_url() {
	$fonts_url = '';

	/* Translators: If there are characters in your language that are not
	 * supported by Montserrat, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$montserrat = _x( 'on', 'Montserrat font: on or off', 'jobify' );

	/* Translators: If there are characters in your language that are not
	 * supported by Varela Round, translate this to 'off'. Do not translate into your
	 * own language.
	 */
	$varela = _x( 'on', 'Varela Round font: on or off', 'jobify' );

	if ( 'off' !== $montserrat || 'off' !== $varela ) {
		$font_families = array();

		if ( 'off' !== $montserrat )
			$font_families[] = 'Montserrat:400,700';

		if ( 'off' !== $varela )
			$font_families[] = 'Varela+Round';

		$protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
			'family' => implode( '|', $font_families ),
			'subset' => 'latin',
		);
		$fonts_url = add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" );
	}

	return $fonts_url;
}

/**
 * Adds additional stylesheets to the TinyMCE editor if needed.
 *
 * @uses jobify_fonts_url() to get the Google Font stylesheet URL.
 *
 * @since Jobify 1.0
 *
 * @param string $mce_css CSS path to load in TinyMCE.
 * @return string
 */
function jobify_mce_css( $mce_css ) {
	$fonts_url = jobify_fonts_url();

	if ( empty( $fonts_url ) )
		return $mce_css;

	if ( ! empty( $mce_css ) )
		$mce_css .= ',';

	$mce_css .= esc_url_raw( str_replace( ',', '%2C', $fonts_url ) );

	return $mce_css;
}
add_filter( 'mce_css', 'jobify_mce_css' );

/**
 * Loads our special font CSS file.
 *
 * To disable in a child theme, use wp_dequeue_style()
 * function mytheme_dequeue_fonts() {
 *     wp_dequeue_style( 'jobify-fonts' );
 * }
 * add_action( 'wp_enqueue_scripts', 'mytheme_dequeue_fonts', 11 );
 *
 * Also used in the Appearance > Header admin panel:
 * @see twentythirteen_custom_header_setup()
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_fonts() {
	$fonts_url = jobify_fonts_url();

	if ( ! empty( $fonts_url ) )
		wp_enqueue_style( 'jobify-fonts', esc_url_raw( $fonts_url ), array(), null );
}
add_action( 'wp_enqueue_scripts', 'jobify_fonts' );

/**
 * Enqueues scripts and styles for front end.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_scripts_styles() {
	global $wp_styles, $edd_options;

	/*
	 * Adds JavaScript to pages with the comment form to support sites with
	 * threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	wp_deregister_script( 'wp-job-manager-job-application' );

	$deps = array( 'jobify-vendor' );

	if ( class_exists( 'WooCommerce' ) )
		$deps[] = 'woocommerce';

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jobify-vendor', get_template_directory_uri() . '/js/vendor.min.js' );
	wp_enqueue_script( 'jobify', get_template_directory_uri() . '/js/jobify.min.js', $deps, 20130718 );

	/**
	 * Localize/Send data to our script.
	 */
	$jobify_settings = array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'i18n'    => array(

		),
		'pages'   => array(
			'is_widget_home'  => is_page_template( 'page-templates/jobify.php' ),
			'is_job'          => is_singular( 'job_listing' ),
			'is_resume'       => is_singular( 'resume' ),
			'is_testimonials' => is_page_template( 'page-templates/testimonials.php' ) || is_post_type_archive( 'testimonial' )
		),
		'widgets' => array()
	);

	foreach ( jobify_homepage_widgets() as $widget ) {
		$options = get_option( 'widget_' . $widget[ 'classname' ] );

		if ( ! isset( $widget[ 'callback' ][0] ) )
			continue;

		$options = $options[ $widget[ 'callback' ][0]->number ];

		$jobify_settings[ 'widgets' ][ $widget[ 'classname' ] ] = array(
			'animate' => isset ( $options[ 'animations' ] ) && 1 == $options[ 'animations' ] ? 1 : 0
		);
	}

	wp_localize_script( 'jobify', 'jobifySettings', $jobify_settings );

	/** Styles */
	wp_enqueue_style( 'jobify-vendor', get_template_directory_uri() . '/css/vendor.min.css' );
	wp_enqueue_style( 'jobify-entypo', get_template_directory_uri() . '/css/entypo.min.css' );
	wp_enqueue_style( 'jobify', get_stylesheet_uri() );

	if ( jobify_theme_mod( 'jobify_general', 'responsive' ) ) {
		wp_enqueue_style( 'jobify-responsive', get_template_directory_uri() . '/css/responsive.min.css', array( 'jobify' ) );
	}

	wp_dequeue_style( 'wp-job-manager-frontend' );
	wp_dequeue_style( 'wp-job-manager-resume-frontend' );
}
add_action( 'wp_enqueue_scripts', 'jobify_scripts_styles' );

/**
 * Get all widgets used on the home page.
 *
 * @since Jobify 1.0
 *
 * @return array $_widgets An array of active widgets
 */
function jobify_homepage_widgets() {
	global $wp_registered_sidebars, $wp_registered_widgets;

	$index            = 'widget-area-front-page';
	$sidebars_widgets = wp_get_sidebars_widgets();
	$_widgets         = array();

	if ( empty( $sidebars_widgets ) || empty($wp_registered_sidebars[$index]) || !array_key_exists($index, $sidebars_widgets) || !is_array($sidebars_widgets[$index]) || empty($sidebars_widgets[$index]) )
		return $_widgets;

	foreach ( (array) $sidebars_widgets[$index] as $id ) {
		$_widgets[] = isset( $wp_registered_widgets[$id] ) ? $wp_registered_widgets[$id] : null;
	}

	return $_widgets;
}

/**
 * Adjust page when responsive is off to normal scale.
 *
 * @since Jobify 1.1
 */
function jobify_nonresponsive_viewport() {
	if ( ! jobify_theme_mod( 'jobify_general', 'responsive' ) )
		return;

	echo '<meta name="viewport" content="initial-scale=1">';
}
add_action( 'wp_head', 'jobify_nonresponsive_viewport' );

/**
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Jobify 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string Filtered title.
 */
function jobify_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'jobify' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'jobify_wp_title', 10, 2 );

/**
 * Registers widgets, and widget areas.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_widgets_init() {
	register_widget( 'Jobify_Widget_Callout' );
	register_widget( 'Jobify_Widget_Video' );
	register_widget( 'Jobify_Widget_Blog_Posts' );
	register_widget( 'Jobify_Widget_Slider_Generic' );

	if ( function_exists( 'soliloquy' ) ) {
		register_widget( 'Jobify_Widget_Slider' );
		register_widget( 'Jobify_Widget_Slider_Hero' );
	}

	if ( ! ( defined( 'RCP_PLUGIN_VERSION' ) || class_exists( 'WooCommerce' ) ) ) {
		register_widget( 'Jobify_Widget_Price_Table' );
		register_widget( 'Jobify_Widget_Price_Option' );
	}

	register_sidebar( array(
		'name'          => __( 'Homepage Widget Area', 'jobify' ),
		'id'            => 'widget-area-front-page',
		'description'   => __( 'Choose what should display on the custom static homepage.', 'jobify' ),
		'before_widget' => '<section id="%1$s" class="homepage-widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="homepage-widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'jobify' ),
		'id'            => 'sidebar-blog',
		'description'   => __( 'Choose what should display on blog pages.', 'jobify' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="sidebar-widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'jobify' ),
		'id'            => 'widget-area-footer',
		'description'   => __( 'Display columns of widgets in the footer.', 'jobify' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="footer-widget-title">',
		'after_title'   => '</h3>',
	) );

	if ( ! ( defined( 'RCP_PLUGIN_VERSION' ) || class_exists( 'WooCommerce' ) ) ) {
		register_sidebar( array(
			'name'          => __( 'Price Table', 'jobify' ),
			'id'            => 'widget-area-price-options',
			'description'   => __( 'Drag multiple "Price Option" widgets here. Then drag the "Pricing Table" widget to the "Homepage Widget Area".', 'jobify' ),
			'before_widget' => '<div id="%1$s" class="pricing-table-widget %2$s">',
			'after_widget'  => '</div>'
		) );
	}
}
add_action( 'widgets_init', 'jobify_widgets_init' );

/**
 * Extends the default WordPress body class to denote:
 * 1. Custom fonts enabled.
 *
 * @since Jobify 1.0
 *
 * @param array $classes Existing class values.
 * @return array Filtered class values.
 */
function jobify_body_class( $classes ) {
	if ( wp_style_is( 'jobify-fonts', 'queue' ) )
		$classes[] = 'custom-font';

	if ( get_option( 'job_manager_enable_categories' ) )
		$classes[] = 'wp-job-manager-categories';

	if ( get_option( 'resume_manager_enable_categories' ) )
		$classes[] = 'wp-resume-manager-categories';

	if ( class_exists( 'WP_Job_Manager_Job_Tags' ) )
		$classes[] = 'wp-job-manager-tags';

	return $classes;
}
add_filter( 'body_class', 'jobify_body_class' );

/**
 * Extends the default WordPress comment class to add 'no-avatars' class
 * if avatars are disabled in discussion settings.
 *
 * @since Jobify 1.0
 *
 * @param array $classes Existing class values.
 * @return array Filtered class values.
 */
function jobify_comment_class( $classes ) {
	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	return $classes;
}
add_filter( 'comment_class', 'jobify_comment_class' );

/**
 * Adds a class to menu items that have children elements
 * so that they can be styled
 *
 * @since Jobify 1.0
 */
function jobify_add_menu_parent_class( $items ) {
	$parents = array();

	foreach ( $items as $item ) {
		if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
			$parents[] = $item->menu_item_parent;
		}
	}

	foreach ( $items as $item ) {
		if ( in_array( $item->ID, $parents ) ) {
			$item->classes[] = 'has-children';
		}
	}

	return $items;
}
add_filter( 'wp_nav_menu_objects', 'jobify_add_menu_parent_class' );

/**
 * Append modal boxes to the bottom of the the page that
 * will pop up when certain links are clicked.
 *
 * Login/Register pages must be set in EDD settings for this to work.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_inline_modals() {
	if ( ! jobify_is_job_board() )
		return;

	$login = jobify_find_page_with_shortcode( array( 'jobify_login_form', 'login_form' ) );

	if ( 0 != $login )
		get_template_part( 'modal', 'login' );

	$register = jobify_find_page_with_shortcode( array( 'jobify_register_form', 'register_form' ) );

	if ( 0 != $register )
		get_template_part( 'modal', 'register' );
}
add_action( 'wp_footer', 'jobify_inline_modals' );

/**
 * If the menu item has a custom class, that means it is probably
 * going to be triggering a modal. The ID will be used to determine
 * the inline content to be displayed, so we need it to provide context.
 * This uses the specificed class name instead of `menu-item-x`
 *
 * @since Jobify 1.0
 *
 * @param string $id The ID of the current menu item
 * @param object $item The current menu item
 * @param array $args Arguments
 * @return string $id The modified menu item ID
 */
function jobify_nav_menu_item_id( $id, $item, $args ) {
	if ( ! empty( $item->classes[0] ) ) {
		return current($item->classes) . '-modal';
	}

	return $id;
}
add_filter( 'nav_menu_item_id', 'jobify_nav_menu_item_id', 10, 3 );

/**
 * Pagination
 *
 * After the loop, attach pagination to the current query.
 *
 * @since Jobify 1.0
 *
 * @return void
 */
function jobify_pagination() {
	global $wp_query;

	$big = 999999999; // need an unlikely integer

	$links = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'prev_text' => '<i class="icon-left-open-big"></i>',
		'next_text' => '<i class="icon-right-open-big"></i>'
	) );
?>
	<div class="paginate-links container">
		<?php echo $links; ?>
	</div>
<?php
}
add_action( 'jobify_loop_after', 'jobify_pagination' );

if ( ! function_exists( 'jobify_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments
 * template simply create your own twentythirteen_comment(), and that function
 * will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param object $comment Comment to display.
 * @param array $args Optional args.
 * @param int $depth Depth of comment.
 * @return void
 */
function jobify_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<p><?php _e( 'Pingback:', 'jobify' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'jobify' ), '<span class="ping-meta"><span class="edit-link">', '</span></span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
	?>
	<li id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
			<div class="comment-avatar">
				<?php echo get_avatar( $comment, 75 ); ?>
			</div><!-- .comment-author -->

			<header class="comment-meta">
				<span class="comment-author vcard"><cite class="fn"><?php comment_author_link(); ?></cite></span>
				<?php echo _x( 'on', 'comment author "on" date', 'jobify' ); ?>
				 <?php
					printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						sprintf( _x( '%1$s at %2$s', 'on 1: date, 2: time', 'jobify' ), get_comment_date(), get_comment_time() )
					);
					edit_comment_link( __( 'Edit', 'jobify' ), '<span class="edit-link"><i class="icon-pencil"></i> ', '<span>' );

					comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'jobify' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );
				?>
			</header><!-- .comment-meta -->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'jobify' ); ?></p>
			<?php endif; ?>

			<div class="comment-content">
				<?php comment_text(); ?>
			</div><!-- .comment-content -->
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // End comment_type check.
}
endif;

if ( ! function_exists( 'shortcode_exists' ) ) :
/**
 * Whether a registered shortcode exists named $tag
 *
 * @since 3.6.0
 *
 * @global array $shortcode_tags
 * @param string $tag
 * @return boolean
 */
function shortcode_exists( $tag ) {
	global $shortcode_tags;
	return array_key_exists( $tag, $shortcode_tags );
}
endif;

if ( ! function_exists( 'has_shortcode' ) ) :
/**
 * Whether the passed content contains the specified shortcode
 *
 * @since 3.6.0
 *
 * @global array $shortcode_tags
 * @param string $tag
 * @return boolean
 */
function has_shortcode( $content, $tag ) {
	if ( shortcode_exists( $tag ) ) {
		preg_match_all( '/' . get_shortcode_regex() . '/s', $content, $matches, PREG_SET_ORDER );
		if ( empty( $matches ) )
			return false;

		foreach ( $matches as $shortcode ) {
			if ( $tag === $shortcode[2] )
				return true;
		}
	}
	return false;
}
endif;

/**
 * Is WP Job Manager active?
 *
 * @since Jobify 1.0.0
 *
 * @return boolean
 */
function jobify_is_job_board() {
	return class_exists( 'WP_Job_Manager' );
}

function jobify_rcp_subscription_selector( $settings ) {
	if ( ! defined( 'RCP_PLUGIN_VERSION' ) ) {
		return $settings;
	}

	$levels  = rcp_get_subscription_levels( 'all' );
	$keys    = wp_list_pluck( $levels, 'id' );
	$names   = wp_list_pluck( $levels, 'name' );

	if ( ! ( is_array( $keys ) && is_array( $names ) ) ) {
		return $settings;
	}

	$options = array_combine( $keys, $names );

	$settings[ 'subscription' ] = array(
		'label'   => __( 'Subscription Level Visibility:', 'jobify' ),
		'std'     => 0,
		'type'    => 'multicheck',
		'options' => $options
	);

	return $settings;
}

/**
 * Find pages that contain shortcodes.
 *
 * To avoid options, try to find pages for them.
 *
 * @since Jobify 1.0
 *
 * @return $_page
 */
function jobify_find_page_with_shortcode( $shortcodes ) {
	if ( ! is_array( $shortcodes ) )
		$shortcode = array( $shortcodes );

	$_page = 0;

	foreach ( $shortcodes as $shortcode ) {
		if ( ! get_option( 'job_manager_page_' . $shortcode ) ) {
			$pages = new WP_Query( array(
				'post_type'              => 'page',
				'post_status'            => 'publish',
				'ignore_sticky_posts'    => 1,
				'no_found_rows'          => true,
				'nopaging'               => true,
				'update_post_meta_cache' => false,
				'update_post_term_cache' => false
			) );

			while ( $pages->have_posts() ) {
				$pages->the_post();

				if ( has_shortcode( get_post()->post_content, $shortcode ) ) {
					$_page = get_post()->ID;

					break;
				}
			}

			add_option( 'job_manager_page_' . $shortcode, $_page );
		} else {
			$_page = get_option( 'job_manager_page_' . $shortcode );
		}

		if ( $_page > 0 )
			break;
	}

	return $_page;
}

/**
 * When a login fails (kinda) redirect them back
 * to the login page.
 *
 * @since Jobify 1.7.0
 */
function jobify_login_fail( $username ){
	$page = jobify_find_page_with_shortcode( array( 'jobify_login_form', 'login_form' ) );
	$page = get_permalink( $page );

	wp_redirect( add_query_arg( 'login', 'failed', $page ) );

    exit();
}

function jobify_authenticate_username_password( $user, $username, $password ) {
	if ( is_a( $user, 'WP_User' ) ) {
		return $user;
	}

	if ( empty($username) || empty($password) ) {
		$page = jobify_find_page_with_shortcode( array( 'jobify_login_form', 'login_form' ) );
		$page = get_permalink( $page );

		wp_redirect( $page );

	    exit();
	}
}

function jobify_theme_login_page() {
	if ( jobify_theme_mod( 'jobify_general', 'theme_login' ) ) {
		add_action( 'wp_login_failed', 'jobify_login_fail' );
		add_filter( 'authenticate', 'jobify_authenticate_username_password', 30, 3);
	}
}
add_action( 'init', 'jobify_theme_login_page' );

/** Widgets */
require_once( get_template_directory() . '/inc/class-widget.php' );
require_once( get_template_directory() . '/inc/widgets/class-widget-callout.php' );
require_once( get_template_directory() . '/inc/widgets/class-widget-video.php' );
require_once( get_template_directory() . '/inc/widgets/class-widget-blog-posts.php' );
require_once( get_template_directory() . '/inc/widgets/class-widget-slider-generic.php' );
require_once( get_template_directory() . '/inc/widgets/class-widget-stats.php' );

if ( ! ( defined( 'RCP_PLUGIN_VERSION' ) || class_exists( 'WooCommerce' ) ) ) {
	require_once( get_template_directory() . '/inc/widgets/class-widget-price-option.php' );
	require_once( get_template_directory() . '/inc/widgets/class-widget-price-table.php' );
}

/** WP Job Manager */
if ( class_exists( 'WP_Job_Manager' ) ) {
	require_once( get_template_directory() . '/inc/integrations/wp-job-manager/wp-job-manager.php' );
}

/** Resume Manager */
if ( class_exists( 'WP_Job_Manager' ) ) {
	require_once( get_template_directory() . '/inc/integrations/wp-resume-manager/wp-resume-manager.php' );
}

/** Restrict Content Pro */
if ( defined( 'RCP_PLUGIN_VERSION' ) ) {
	require_once( get_template_directory() . '/inc/integrations/restrict-content-pro/restrict-content-pro.php' );
}

/** WooCommerce */
if ( class_exists( 'WooCommerce' ) ) {
	require_once( get_template_directory() . '/inc/integrations/woocommerce/woocommerce.php' );
}

/** Testimoniails */
if ( class_exists( 'Woothemes_Testimonials' ) ) {
	require_once( get_template_directory() . '/inc/integrations/woo-testimonials/testimonials.php' );
}

/** Soliloquy */
if ( function_exists( 'soliloquy' ) ) {
	require_once( get_template_directory() . '/inc/widgets/class-widget-slider-content.php' );
	require_once( get_template_directory() . '/inc/widgets/class-widget-slider-hero.php' );
}

/** Custom Header */
require_once( get_template_directory() . '/inc/custom-header.php' );

/** Customizer */
require_once( get_template_directory() . '/inc/theme-customizer.php' );


function fobster_submit_job_form_fields( $fields ) {
     unset( $fields[ 'company' ][ 'company_name' ] );
     unset( $fields[ 'company' ][ 'company_website' ] );
     unset( $fields[ 'company' ][ 'company_twitter' ] );

     return $fields;
}
add_filter( 'submit_job_form_fields', 'fobster_submit_job_form_fields' );

remove_filter( 'submit_job_form_fields', 'jobify_submit_job_form_fields' );
remove_filter( 'the_job_location', 'jobify_the_job_location' );

/**
 * output_pic_jobs function.
 *
 * @param mixed $args
 * @return void
 */
function output_pic_jobs( $atts ) {
	global $job_manager;

	ob_start();

	extract( $atts = shortcode_atts( apply_filters( 'job_manager_output_jobs_defaults', array(
		'per_page'        => get_option( 'job_manager_per_page' ),
		'orderby'         => 'featured',
		'order'           => 'DESC',
		'show_filters'    => true,
		'show_categories' => get_option( 'job_manager_enable_categories' ),
		'categories'      => '',
		'job_types'       => '',
		'location'        => '',
		'keywords'        => ''
	) ), $atts ) );

	$categories = array_filter( array_map( 'trim', explode( ',', $categories ) ) );
	$job_types  = array_filter( array_map( 'trim', explode( ',', $job_types ) ) );

	// Get keywords and location from querystring if set
	if ( ! empty( $_GET['search_keywords'] ) ) {
		$keywords = sanitize_text_field( $_GET['search_keywords'] );
	}
	if ( ! empty( $_GET['search_location'] ) ) {
		$location = sanitize_text_field( $_GET['search_location'] );
	}


	if ( $show_filters && $show_filters !== 'false' ) {

		get_job_manager_template( 'job-filters.php', array( 'per_page' => $per_page, 'orderby' => $orderby, 'order' => $order, 'show_categories' => $show_categories, 'categories' => $categories, 'job_types' => $job_types, 'atts' => $atts, 'location' => $location, 'keywords' => $keywords ) );
		?>
		<div class="container"><div class="entry-content"><ul class="job_listings"></ul><!--<a class="load_more_jobs" href="#" style="display:none;"><strong><?php _e( 'Load more job listings', 'wp-job-manager' ); ?></strong></a>--></div></div>
		<?php

	} else {


		$jobs = get_job_listings( apply_filters( 'job_manager_output_jobs_args', array(
			'search_location'   => $location,
			'search_keywords'   => $keywords,
			'search_categories' => $categories,
			'job_types'         => $job_types,
			'orderby'           => $orderby,
			'order'             => $order,
			'posts_per_page'    => $per_page
		) ) );


		if ( $jobs->have_posts() ) : ?>
			<div class="container">

				<div class="entry-content">
					<ul class="job_listings">

						<?php while ( $jobs->have_posts() ) : $jobs->the_post(); ?>

							<?php get_job_manager_template_part( 'content', 'job_listing' ); ?>

						<?php endwhile; ?>

					</ul>
				</div>
			</div>

			<?php if ( $jobs->found_posts > $per_page ) : ?>

				<?php wp_enqueue_script( 'wp-job-manager-ajax-filters' ); ?>

				<a class="load_more_jobs" href="#"><strong><?php _e( 'Load more job listings', 'wp-job-manager' ); ?></strong></a>

			<?php endif; ?>

		<?php endif;

		wp_reset_postdata();
	}

	return '<div class="job_listings" data-location="' . esc_attr( $location ) . '" data-keywords="' . esc_attr( $keywords ) . '" data-show_filters="' . ( $show_filters && $show_filters !== 'false' ? 1 : 0 ) . '" data-per_page="' . esc_attr( $per_page ) . '" data-orderby="' . esc_attr( $orderby ) . '" data-order="' . esc_attr( $order ) . '" data-categories="' . esc_attr( implode( ',', $categories ) ) . '">' . ob_get_clean() . '</div>';
}
add_shortcode( 'pic_jobs', 'output_pic_jobs');

function TabsFormImagenes()
{
	$output = "";
	$user_ID = get_current_user_id();
	if($user_ID > 0)
	{
	?>
	<script>
	function MostrarDatos()
	{
		document.getElementById("divImagenes").style.display="none";
		document.getElementById("idFormularioPrincipal").style.display="block";
	}

	function MostrarFotos()
	{
		document.getElementById("idFormularioPrincipal").style.display="none";
		document.getElementById("divImagenes").style.display="block";
	}
	</script>
	<?php
	$output = "<input type='button' value='Datos de la Empresa' onclick='MostrarDatos()'>";
	$output .= "<input type='button' value='Fotos' onclick='MostrarFotos()'>";
	}
	return $output;
}
add_shortcode('TabsFormImagenes','TabsFormImagenes');


function AbrirDivFormPrincipal()
{
	$user_ID = get_current_user_id();
	if($user_ID > 0)
	{


		if($_REQUEST['e'] == 1)
		{
			return ("<div id='idFormularioPrincipal' style='display: none'>");
		}
		else
		{
			return ("<div id='idFormularioPrincipal'>");
		}
	}
}
add_shortcode('AbrirDivFormPrincipal', 'AbrirDivFormPrincipal');


function CerrarDivFormPrincipal()
{
	$user_ID = get_current_user_id();
	if($user_ID > 0)
	{
		return ("</div>");
	}
}
add_shortcode('CerrarDivFormPrincipal', 'CerrarDivFormPrincipal');


function AgregarImagen()
{



	$output = "";
	$user_ID = get_current_user_id();
	if($user_ID > 0)
	{

		if($_REQUEST['e'] != 1)
		{
			$output ='
			<div id="divImagenes">';
		}
		else
		{
			$output ='
			<div id="divImagenes">';
		}

		?>
		<script>
		function MostrarSiguiente(categoria,numero)
		{
			document.getElementById("imagen"+categoria+numero).style.display="block";
		}
		</script>
		<?php
		global $wpdb;
		/*$result = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_author = '$_SESSION[idEmpresa]' and post_type = 'job_listing' limit 0,1", ARRAY_N) ;
		foreach( $result as $results )
		{
			$IdEmpresa = $results[0];
		}*/
		$IdEmpresa = $_SESSION['idEmpresa'];
		$cat = 1;
		$result2 = $wpdb->get_results("SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = '$IdEmpresa' ", ARRAY_N) ;
		foreach( $result2 as $results2 )
		{
			$result4 = $wpdb->get_results("SELECT term_id FROM wp_term_taxonomy WHERE taxonomy = 'job_listing_category' and term_taxonomy_id = '$results2[0]' ", ARRAY_N) ;
			foreach( $result4 as $results4 )
			{
				$result5 = $wpdb->get_results("SELECT name FROM wp_terms WHERE term_id = '$results4[0]' ", ARRAY_N) ;
				foreach( $result5 as $results5 )
				{
				if($results5[0] == "Gráfico / Packaging" || $results5[0] == "Interiorismo / Paisajismo" || $results5[0] == "Producto" || $results5[0] == "Textil / Indumentaria" || $results5[0] == "Web / Multimedia" || $results5[0] == "Gráfico/Packaging" || $results5[0] == "Interiorismo/Paisajismo" || $results5[0] == "Textil/Indumentaria" || $results5[0] == "Web/Multimedia" )
				{
					if($cat == 1)
					{
						$Categoria1 = $results5[0];
					}
					else if ($cat == 2)
					{
						$Categoria2 = $results5[0];
					}
					else if ($cat == 3)
					{
						$Categoria3 = $results5[0];
					}
					else if ($cat == 4)
					{
						$Categoria4 = $results5[0];
					}
					else if ($cat == 5)
					{
						$Categoria5 = $results5[0];
					}
					$cat = $cat + 1;
				}
				}
			}

		}

		$output .= "
		<p>Importante: Las imagenes deben tener aproximadamente 1100px de ancho y 490px de alto, y un peso maximo de 500 kb</p>
			<form action='../../upload' method='post' enctype='multipart/form-data'>
				$Categoria1 :<br>
				";

		$output	.= '
			<input type="file" id="imagen11" name="cat11" onclick="MostrarSiguiente(1,2)">
				';

		$output	.= '<input type="file" id="imagen12" name="cat12" onclick="MostrarSiguiente(1,3)" style="display: none" >

				<input type="file" id="imagen13" name="cat13" onclick="MostrarSiguiente(1,4)" style="display: none">

				<input type="file" id="imagen14" name="cat14" onclick="MostrarSiguiente(1,5)" style="display: none">

				<input type="file" id="imagen15" name="cat15" style="display: none">
				</br>
				';
		if ($Categoria2 != "")
		{
		$output .= "
				$Categoria2:<br>";
		$output .= '

				<input type="file" id="imagen21" name="cat21" onclick="MostrarSiguiente(2,2)"  >
				<input type="file" id="imagen22" name="cat22" onclick="MostrarSiguiente(2,3)" style="display: none" >
				<input type="file" id="imagen23" name="cat23" onclick="MostrarSiguiente(2,4)" style="display: none" >
				<input type="file" id="imagen24" name="cat24" onclick="MostrarSiguiente(2,5)" style="display: none" >
				<input type="file" id="imagen25" name="cat25"  style="display: none" >
				<br>';
		}
		if($Categoria3 != '')
		{
		$output .=	"$Categoria3: <br>";

		$output .=	'<input type="file" id="imagen31" name="cat31" onclick="MostrarSiguiente(3,2)" >
				<input type="file" id="imagen32" name="cat32" onclick="MostrarSiguiente(3,3)" style="display: none" >
				<input type="file" id="imagen33" name="cat33" onclick="MostrarSiguiente(3,4)" style="display: none" >
				<input type="file" id="imagen34" name="cat34" onclick="MostrarSiguiente(3,5)" style="display: none" >
				<input type="file" id="imagen35" name="cat35" style="display: none" >
				<br>
		';
		}

		$output .= '<input type="submit" value="Subirimagenes" name="submit">
			</form>
		</div>';
	}
	return $output;
}
add_shortcode('AgregarImagen','AgregarImagen');

function CrearSolilo($Nombre)
{
	echo "Hay que crear el shortcode";
}

function SubirImagenesDesdeAdmin()
{

	if(current_user_can('administrator'))
	{
		global $wpdb;
		if($_REQUEST['subirImagenBoton'] == "")
		{
		$result2 = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = '_featured' and meta_value = '1'", ARRAY_N);
				foreach( $result2 as $results2 )
				{
					$result = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type = 'job_listing' and ID = '$results2[0]'", ARRAY_N) or die(mysql_error());
							foreach( $result as $results )
							{
								echo "<form action=''>";
								echo "<input type='hidden' value='$results[0]' name='idEmpresa' />";
								echo"Empresa: ".$results[5]."<input type='submit' value='Subir Imagenes' name='subirImagenBoton'/></br>";
								echo "</form>";
							}
				}

		}
		else
		{
			$idEmpresa = $_REQUEST['idEmpresa'];
			$resultPrin = $wpdb->get_results("SELECT ID,guid FROM wp_posts WHERE post_parent = '$idEmpresa' and post_mime_type = 'image/jpeg'", ARRAY_N) or die(mysql_error());
			{
				foreach( $resultPrin as $resultsPrin )
				{
					echo '<img src="'.$resultsPrin[1].'"  style="width:220px;height:90px">'."<br>";
				}
			}
			$_SESSION['idEmpresa'] = $idEmpresa;
			echo do_shortcode('[AgregarImagen]');
		}
	}
	else
	{
		wp_redirect( '/'); exit;
	}
}
add_shortcode('SubirImagenesDesdeAdmin','SubirImagenesDesdeAdmin');

function CrearImagenes()
{
	$IdEmpresa = $_SESSION['idEmpresa'];

	if ($_REQUEST['subir'] == 1 || $_REQUEST['submit'] != "" )
	{
		$SubirImagenes = true;
	}
	else
	{
		$SubirImagenes = false;
	}
	if($SubirImagenes)
	{
		$user_ID = get_current_user_id();
		global $wpdb;

		$SubioTodas = true;
        //var_dump($_FILES);
        //echo '<hr/>';
		for($i = 1; $i <= 4; $i++)
		{
			for($j = 1; $j <= 5; $j++)
			{

				$wp_upload_dir = wp_upload_dir();
                
				$NombreImagen = "Imagen".$IdEmpresa.$i.$j;
				$target_dir = $wp_upload_dir['path']."/". basename(trim($_FILES["cat".$i.$j]["name"]));
				$target_file = $wp_upload_dir['url'] ."/". basename(trim($_FILES["cat".$i.$j]["name"]));
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				$attachment = $_FILES["cat".$i.$j];
                //var_dump($_FILES["cat".$i.$j]["name"]);
                //var_dump($attachment);
                
				$attachment['post_title'] = trim($_FILES["cat".$i.$j]["name"]);
				$attachment['post_content'] = '';
				$attachment['post_status'] = 'inherit';
				$attachment['guid'] = $wp_upload_dir['url']."/".sanitize_file_name(trim($_FILES["cat".$i.$j]["name"]));
				$attachment['post_parent'] = $IdEmpresa;
				$attachment['post_mime_type'] = "image/jpeg";
                //var_dump($attachment);
				$uploadOk = 1;
				$Error = "";
				if(isset($_REQUEST["submit_job"]))
				{
					$check = getimagesize($_FILES["cat".$i.$j]["tmp_name"]);
					if($check !== false) {
						$uploadOk = 1;
					} else {
						//$uploadOk = 0;
					}
				}
				/*if ($_FILES["cat".$i.$j]["size"] > 500000) {
					//$uploadOk = 0;
					$Error = "Imagen demasiado grande.";
				}*/
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG"
				&& $imageFileType != "GIF" )
				{
					$uploadOk = 0;
					$Error = "Formato de imagen no aceptado.";
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0)
				{
				// if everything is ok, try to upload file
				}
				else
				{
					$posContent = strrpos($target_file , "wp-content");
					$Path = substr($target_file, $posContent);
					global $wpdb;
					$cant = $wpdb->get_var("select count(*) from wp_posts where guid LIKE CONCAT('%', '$Path' ,'%')");
					if($cant > 0)
					{
						$cant++;
						$LastPosDePunto = strrpos($Path, ".");
						$HastaPunto = substr($Path, 0, $LastPosDePunto);
						$DespuesDePunto = substr($Path, $LastPosDePunto);
						$Path = $HastaPunto."($cant)".$DespuesDePunto;
						
						$LastPosDePunto = strrpos($attachment['guid'], ".");
						$HastaPunto = substr($attachment['guid'], 0, $LastPosDePunto);
						$DespuesDePunto = substr($attachment['guid'], $LastPosDePunto);
						$attachment['guid'] = $HastaPunto."($cant)".$DespuesDePunto;
						
						$LastPosDePunto = strrpos($attachment['name'], ".");
						$HastaPunto = substr($attachment['name'], 0, $LastPosDePunto);
						$DespuesDePunto = substr($attachment['name'], $LastPosDePunto);
						$attachment['name'] = $HastaPunto."($cant)".$DespuesDePunto;
						
						$LastPosDePunto = strrpos($attachment['post_title'], ".");
						$HastaPunto = substr($attachment['post_title'], 0, $LastPosDePunto);
						$DespuesDePunto = substr($attachment['post_title'], $LastPosDePunto);
						$attachment['post_title'] = $HastaPunto."($cant)".$DespuesDePunto;
					}
					var_dump($target_dir);
                    var_dump($target_file);
                    var_dump($attachment['guid']);
                    var_dump(trim($Path));
					die;	
					if (move_uploaded_file($_FILES["cat".$i.$j]["tmp_name"],trim($Path))){			

						$attach_id = wp_insert_attachment( $attachment);
						require_once( ABSPATH . 'wp-admin/includes/image.php' );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $target_dir);
						wp_update_attachment_metadata( $attach_id,  $attach_data );

						$wpdb->insert( 'wp_postmeta',array( 'post_id' => "$attach_id", 'meta_value' => "$i", 'meta_key' => "Categoria"));
						$Texto = $_REQUEST["texto".$i.$j];
						$wpdb->insert( 'wp_postmeta',array( 'post_id' => "$attach_id", 'meta_value' => "$Texto", 'meta_key' => "Texto"));

						$user_ID = get_current_user_id();
					}
					else
					{
						echo "La imagen ". basename( $_FILES["cat".$i.$j]["name"]). "no se ha podido subido.".$Error."<br>";
						$SubioTodas = false;
					}
				}
			}
		}
		if($SubioTodas == true)
		{
		}
		else
		{
			echo "Las imagenes no se subieron correctamente";
		}
	}
}
add_shortcode('CrearImagenes','CrearImagenes');

//////////////////COSAS QUE SE AGREGAN PARA EL SLIDER////////////////////////////////

function ActualizarBase()
{
	global $wpdb;
	$result = $wpdb->get_results("SELECT ID,post_content FROM wp_posts ", ARRAY_N) ;
			foreach( $result as $results )
			{
				$mystring = $results[1];
				$findme   = 'slider';
				$pos = strpos($mystring, $findme);
				//
				if ($pos === false) {
				}
				else
				{
					$wpdb->update( 'wp_posts', array( 'post_content' => "[Slider]"), array( 'ID' => $results[0] ));
					//echo $results[0]."---".$results[1]."<br>";
				}
			}

}
add_shortcode('ActualizarBase','ActualizarBase');


/////////////////////////////////////////////////////////////////////////////////////

//add_filter( 'the_content', 'Slider' );
function MostrarSlider()
{
	global $wpdb;
	global $post;
	$post->ID;
	$result = $wpdb->get_results("SELECT post_type FROM wp_posts WHERE ID = '$post->ID'", ARRAY_N) or die(mysql_error());
	foreach( $result as $results )
	{
		if($results[0] == "job_listing")
		{
			$type = $results[0];
		}
	}
	if($type == "job_listing")
	{
		$result = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE meta_key = '_featured' and meta_value = '1'", ARRAY_N) or die(mysql_error());
		foreach( $result as $results )
		{
				$featured = $results[0];
		}
	}
	if($featured == "1")
	{
		do_shortcode('[Slider]');
	}

}


function Slider()
{
	/*global $post;
	$post->ID;
	$result = $wpdb->get_results("SELECT post_content FROM wp_posts WHERE ID = '$post->ID'", ARRAY_N) or die(mysql_error());
	foreach( $result as $results ) {
		echo $results[0];
	}*/

	?>
      
	<?php
	$IdEmpresa = get_the_ID();
	global $wpdb;
	////////////////////Obtener categorias////////////////////////////////

		$cat = 1;
		$result2 = $wpdb->get_results("SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = '$IdEmpresa' ", ARRAY_N) ;
		foreach( $result2 as $results2 )
		{
			$result4 = $wpdb->get_results("SELECT term_id FROM wp_term_taxonomy WHERE taxonomy = 'job_listing_category' and term_taxonomy_id = '$results2[0]' ", ARRAY_N) ;
			foreach( $result4 as $results4 )
			{
				$result5 = $wpdb->get_results("SELECT name FROM wp_terms WHERE term_id = '$results4[0]' ", ARRAY_N) ;
				foreach( $result5 as $results5 )
				{
				if($results5[0] == "Gráfico / Packaging" || $results5[0] == "Interiorismo / Paisajismo" || $results5[0] == "Producto" || $results5[0] == "Textil / Indumentaria" || $results5[0] == "Web / Multimedia" || $results5[0] == "Gráfico/Packaging" || $results5[0] == "Interiorismo/Paisajismo" || $results5[0] == "Textil/Indumentaria" || $results5[0] == "Web/Multimedia" )
				{
					if($cat == 1)
					{
						$Categoria1 = $results5[0];
					}
					else if ($cat == 2)
					{
						$Categoria2 = $results5[0];
					}
					else if ($cat == 3)
					{
						$Categoria3 = $results5[0];
					}
					else if ($cat == 4)
					{
						$Categoria4 = $results5[0];
					}
					else if ($cat == 5)
					{
						$Categoria5 = $results5[0];
					}
					$cat = $cat + 1;
				}
				}
			}

		}


	//////////////////////////////////////////////////////////////////////

	$output = "";
	$output .= "<div id='SliderPrincipal'>";
	$output .= "<ul>";
	?>
	<script>
	function Mostrar(num)
	{
		//document.getElementsByClassName('bx-viewport').item(0).style.height="600";

			for(i = 0; i < 5 ; i++)
		{
			if(i != num)
			{
				try
				{
					document.getElementById("contTab"+i).style.display = "none";
					document.getElementById("tab"+i).classList.remove("activeTab");
				}
				catch(Exception){}
			}
			else
			{
				try
				{
				document.getElementById("contTab"+num).style.display = "block";
				document.getElementById("tab"+i).classList.add("activeTab");
				}
				catch(Exception){}
			}
		}

		try
		{
			try
			{
				document.getElementsByClassName("bx-viewport").item(0).style.height="490px";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("bx-viewport").item(1).style.height="490px";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("bx-viewport").item(2).style.height="490px";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("bx-viewport").item(3).style.height="490px";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("bx-viewport").item(4).style.height="490px";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("bx-viewport").item(5).style.height="490px";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("bx-viewport").item(6).style.height="490px";
			}
			catch(Exception){};
			/*			
			try
			{
				document.getElementsByClassName("bx-viewport").item(0).style.width="100%";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("bx-viewport").item(1).style.width="100%";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("bx-viewport").item(2).style.width="100%";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("bx-viewport").item(3).style.width="100%";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("bx-viewport").item(4).style.width="100%";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("bx-viewport").item(5).style.width="100%";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("bx-viewport").item(6).style.width="100%";
			}
			catch(Exception){};

			*/

			try
			{
				document.getElementsByClassName("sliderNum0").item(0).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum0").item(1).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum0").item(2).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum0").item(3).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum0").item(4).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum0").item(0).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum0").item(1).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum0").item(2).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum0").item(3).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum0").item(4).style.height="auto";
			}
			catch(Exception){};

			
			try
			{
				document.getElementsByClassName("sliderNum1").item(0).style.width="auto";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("sliderNum1").item(1).style.width="auto";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("sliderNum1").item(2).style.width="auto";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("sliderNum1").item(3).style.width="auto";
			}
			catch(Exception){};
			try
			{
				document.getElementsByClassName("sliderNum1").item(4).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum1").item(0).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum1").item(1).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum1").item(2).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum1").item(3).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum1").item(4).style.height="auto";
			}
			catch(Exception){};



			try
			{
				document.getElementsByClassName("sliderNum2").item(0).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum2").item(1).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum2").item(2).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum2").item(3).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum2").item(4).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum2").item(0).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum2").item(1).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum2").item(2).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum2").item(3).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum2").item(4).style.height="auto";
			}
			catch(Exception){};


			try
			{
			document.getElementsByClassName("sliderNum3").item(0).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum3").item(1).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum3").item(2).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum3").item(3).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum3").item(4).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum3").item(0).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum3").item(1).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum3").item(2).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum3").item(3).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum3").item(4).style.height="auto";
			}
			catch(Exception){};


			try
			{
				document.getElementsByClassName("sliderNum4").item(0).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum4").item(1).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum4").item(2).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum4").item(3).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum4").item(4).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum4").item(0).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum4").item(1).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum4").item(2).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum4").item(3).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum4").item(4).style.height="auto";
			}
			catch(Exception){};


			try
			{
				document.getElementsByClassName("sliderNum5").item(0).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum5").item(1).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum5").item(2).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum5").item(3).style.width="auto";
			}
			catch(Exception){};

			try
			{
			document.getElementsByClassName("sliderNum5").item(4).style.width="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum5").item(0).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum5").item(1).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum5").item(2).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum5").item(3).style.height="auto";
			}
			catch(Exception){};

			try
			{
				document.getElementsByClassName("sliderNum5").item(4).style.height="auto";
			}
			catch(Exception){};



		//document.getElementsByClassName('bx-viewport').item(2).style.height="600";
		}
		catch (Exception)
		{

		}
	}
	</script>
	<?php
	$output .= "<body onload='Mostrar(0);Mostrar(0)'></body>";
	//$output .= "<body onload='document.getElementsByClassName(\"bx-viewport\").style.heigth = \"100%\" ></body>";
		$output .= "<li id='tab0' class='activeTab' onclick='Mostrar(\"0\")'>";
		$output .= "General";
		$output .= "</li>";

	if($Categoria1 != "")
	{
		$cantCat = 1;
		$output .= "<li id='tab1' class='' onclick='Mostrar(\"1\")'>";
		$output .= $Categoria1;
		$output .= "</li>";
	}
	if($Categoria2 != "")
	{
		$cantCat = 2;
		$output .= "<li id='tab2' onclick='Mostrar(\"2\")'>";
		$output .= $Categoria2;
		$output .= "</li>";
	}
	if($Categoria3 != "")
	{
		$cantCat = 3;
		$output .= "<li id='tab3' onclick='Mostrar(\"3\")'>";
		$output .= $Categoria3;
		$output .= "</li>";
	}
	if($Categoria4 != "")
	{
		$cantCat = 4;
		$output .= "<li id='tab4' onclick='Mostrar(\"4\")'>";
		$output .= $Categoria4;
		$output .= "</li>";
	}

	if($cantCat == 1)
	{
		$Cant1 = 5;
		$Cant2 = 0;
		$Cant3 = 0;
		$Cant4 = 0;
	}
	else if($cantCat == 2)
	{
		$Cant1 = 3;
		$Cant2 = 2;
		$Cant3 = 0;
		$Cant4 = 0;
	}
	else if($cantCat == 3)
	{
		$Cant1 = 2;
		$Cant2 = 2;
		$Cant3 = 1;
		$Cant4 = 0;
	}
	else if($cantCat == 4)
	{
		$Cant1 = 2;
		$Cant2 = 1;
		$Cant3 = 1;
		$Cant4 = 1;
	}
	$CantImagenesAtual = 0;
	$ArregloCat1 = array();
	$ArregloCat2 = array();
	$ArregloCat3 = array();
	$ArregloCat4 = array();
	$ArregloCatActual = array();
	$output .= "</ul>";
	$output .= "</div>";
	$output .= "<div id='ContenedorInfoTab' >";
	$output .= "<div id='contTab0'>";
	$CantIm = 0;

	$output .= '<ul id="slider0" class="">';
	$result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = 'Categoria' and meta_value = '1'  ", ARRAY_N);

	foreach( $result as $results )
	{

			$result2 = $wpdb->get_results("SELECT guid,post_parent FROM wp_posts WHERE ID = '$results[0]' ORDER by ID DESC limit 0,5 ", ARRAY_N);
			foreach( $result2 as $results2 )
			{
				//echo $results2[1]."---".$IdEmpresa."<br>";
				if ($results2[1] == $IdEmpresa)
				{
					$CantIm++;
					if($CantIm < 6)
					{

						$result21 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$results[0]' and meta_key = 'Texto'", ARRAY_N);
						foreach( $result21 as $results21 )
						{
							$Texto = $results21[0];//$results2[0];
						}

							$NuevaCelda = array();
							$NuevaCelda[] = $results2[0];
							$NuevaCelda[] = $Texto;
							$ArregloCat1[] = $NuevaCelda;


					}
				}
			}
		}
		$Insertados = array();
		for ($i = 1; $i <= $Cant1; $i++)
		{
		$Esta = true;
		$can = 1;
		while($Esta && $can != 10 )
		{
			$can++;
			$Elem =  $ArregloCat1[array_rand($ArregloCat1)];
			if(!in_array($Elem,$Insertados))
			{
				$Esta = false;
				$Insertados[] = $Elem;

			}
		}
			if($Elem[0] != "")
			{
					$CantImagenesAtual++;
					$output .= '<li class="sliderNum0"><img src="'.$Elem[0].'" style="width:100%;height:auto " >'.''.'<ul class = "textoSlider"><li>'.$Elem[1].'</li></ul></li>';

			}
		}

		$CantIm = 0;
		$result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = 'Categoria' and meta_value = '2' ", ARRAY_N);

		foreach( $result as $results )
		{

			$result2 = $wpdb->get_results("SELECT guid,post_parent FROM wp_posts WHERE ID = '$results[0]' ORDER by ID DESC limit 0,5 ", ARRAY_N);
			foreach( $result2 as $results2 )
			{
				//echo $results2[1]."---".$IdEmpresa."<br>";
				if ($results2[1] == $IdEmpresa)
				{
					$CantIm++;
					if($CantIm < 6)
					{

						$result21 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$results[0]' and meta_key = 'Texto'", ARRAY_N);
						foreach( $result21 as $results21 )
						{
							$Texto = $results21[0];//$results2[0];
						}


						//$output .= '<li><img src="'.$results2[0].'" style="width:1100px;height:490px " >'.''.'<ul class = "textoSlider"><li>'.$Texto.'</li></ul></li>';
						$NuevaCelda = array();
						$NuevaCelda[] = $results2[0];
						$NuevaCelda[] = $Texto;
						$ArregloCat2[] = $NuevaCelda;

					}
				}
			}
		}

		for ($i = 1; $i <= $Cant2; $i++)
		{
//			echo "Entra aca2";
		//$Elem = "";
		//while ( $Elem == "")
			$Elem =  $ArregloCat2[array_rand($ArregloCat2)];
			if($Elem[0] != "")
			{
				$CantImagenesAtual++;
				$output .= '<li class="sliderNum0"><img src="'.$Elem[0].'" style="width:auto;height:auto " >'.''.'<ul class = "textoSlider"><li>'.$Elem[1].'</li></ul></li>';
			}
		$ArregloCat2 = array_diff($ArregloCat2, $Elem);
		//print_r($ArregloCat1);
		}

		$CantIm = 0;
		$result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = 'Categoria' and meta_value = '3' ", ARRAY_N);

		foreach( $result as $results )
		{

			$result2 = $wpdb->get_results("SELECT guid,post_parent FROM wp_posts WHERE ID = '$results[0]' ORDER by ID DESC limit 0,5 ", ARRAY_N);
			foreach( $result2 as $results2 )
			{
				//echo $results2[1]."---".$IdEmpresa."<br>";
				if ($results2[1] == $IdEmpresa)
				{
					$CantIm++;
					if($CantIm < 6)
					{

						$result21 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$results[0]' and meta_key = 'Texto'", ARRAY_N);
						foreach( $result21 as $results21 )
						{
							$Texto = $results21[0];//$results2[0];
						}



							$NuevaCelda = array();
							$NuevaCelda[] = $results2[0];
							$NuevaCelda[] = $Texto;
							$ArregloCat3[] = $NuevaCelda;


					}
				}
			}
		}
		//$Insertados = array();
		for ($i = 1; $i <= $Cant3; $i++)
		{
		$Esta = true;
		$can = 1;
		while($Esta && $can != 10 )
		{
			$can++;
			$Elem =  $ArregloCat3[array_rand($ArregloCat3)];
			if(!in_array($Elem,$Insertados))
			{
				$Esta = false;
				$Insertados[] = $Elem;

			}
		}
			if($Elem[0] != "")
			{
			$CantImagenesAtual++;
			$output .= '<li class="sliderNum0"><img src="'.$Elem[0].'" style="width:auto;height:auto " >'.''.'<ul class = "textoSlider"><li>'.$Elem[1].'</li></ul></li>';
			}
		}
		$CantIm = 0;
		$result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = 'Categoria' and meta_value = '4' ", ARRAY_N);

		foreach( $result as $results )
		{

			$result2 = $wpdb->get_results("SELECT guid,post_parent FROM wp_posts WHERE ID = '$results[0]' ORDER by ID DESC limit 0,5 ", ARRAY_N);
			foreach( $result2 as $results2 )
			{
				//echo $results2[1]."---".$IdEmpresa."<br>";
				if ($results2[1] == $IdEmpresa)
				{
					$CantIm++;
					if($CantIm < 6)
					{

						$result21 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$results[0]' and meta_key = 'Texto'", ARRAY_N);
						foreach( $result21 as $results21 )
						{
							$Texto = $results21[0];//$results2[0];
						}

							$NuevaCelda = array();
							$NuevaCelda[] = $results2[0];
							$NuevaCelda[] = $Texto;
							$ArregloCat4[] = $NuevaCelda;


					}
				}
			}
		}
		//$Insertados = array();
		for ($i = 1; $i <= $Cant4; $i++)
		{
		$Esta = true;

		$can = 1;
		while($Esta && $can != 10 )
		{
			$can++;
			$Elem =  $ArregloCat4[array_rand($ArregloCat4)];
			if(!in_array($Elem,$Insertados))
			{
				$Esta = false;
				$Insertados[] = $Elem;

			}
		}
			if($Elem[0] != "")
			{
			$CantImagenesAtual++;
			$output .= '<li class="sliderNum0"><img src="'.$Elem[0].'" style="width:auto;height:auto " >'.''.'<ul class = "textoSlider"><li>'.$Elem[1].'</li></ul></li>';
			}
		}
		$CantIm = 0;
		$CantImQueFaltan = 5 - $CantImagenesAtual;
		for($i; $i <= $CantImQueFaltan; $i++)
		{
			$result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = 'Categoria' ", ARRAY_N);

			foreach( $result as $results )
			{

				$result2 = $wpdb->get_results("SELECT guid,post_parent FROM wp_posts WHERE ID = '$results[0]' ORDER by ID DESC limit 0,5 ", ARRAY_N);
				foreach( $result2 as $results2 )
				{
					//echo $results2[1]."---".$IdEmpresa."<br>";
					if ($results2[1] == $IdEmpresa)
					{
						$CantIm++;
						if($CantIm <= 15)
						{

							$result21 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$results[0]' and meta_key = 'Texto'", ARRAY_N);
							foreach( $result21 as $results21 )
							{
								$Texto = $results21[0];//$results2[0];
							}

								$NuevaCelda = array();
								$NuevaCelda[] = $results2[0];
								$NuevaCelda[] = $Texto;
								$ArregloCatActual[] = $NuevaCelda;




					}
				}
			}

		}


		}


		for ($i = 1; $i <= $CantImQueFaltan ; $i++)
		{
			$Esta = true;
			$cont = 1;
			while($Esta)
			{
				$cont++;
				$Elem =  $ArregloCatActual[array_rand($ArregloCatActual)];
				if(!in_array($Elem,$Insertados) || $cont == 10)
				{

					$Esta = false;
					$Insertados[] = $Elem;
				}
				if($cont == 10)
				{
					$Elem[0] = "";
				}
			}
			if($Elem[0] != "")
			{
				$CantImagenesAtual++;
				$output .= '<li class="sliderNum0"><img src="'.$Elem[0].'" style="width:auto;height:auto " >'.''.'<ul class = "textoSlider"><li>'.$Elem[1].'</li></ul></li>';
			}
		}

		$output .= '</ul>';
	$output .= "</div>";
	$output .= "</div>";

		$output .= "<div style='display: none' id='contTab1'>";
		$result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = 'Categoria' and meta_value = '1' ", ARRAY_N);
		$CantIm = 0;
		$clase = sanitize_title($Categoria1);
		$output .= '<ul id="slider1" class="sliderBefore '.$clase.'">';
		foreach( $result as $results )
		{

			$result2 = $wpdb->get_results("SELECT guid,post_parent FROM wp_posts WHERE ID = '$results[0]' ORDER by ID DESC limit 0,5 ", ARRAY_N);
			foreach( $result2 as $results2 )
			{
				//echo $results2[1]."---".$IdEmpresa."<br>";
				if ($results2[1] == $IdEmpresa)
				{
					$CantIm++;
					if($CantIm < 6)
					{

						$result21 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$results[0]' and meta_key = 'Texto'", ARRAY_N);
						foreach( $result21 as $results21 )
						{
							$Texto = $results21[0];//$results2[0];
						}


						$output .= '<li class="sliderNum1"><img src="'.$results2[0].'" style="width:auto;height:auto " >'.''.'<ul class = "textoSlider"><li>'.$Texto.'</li></ul></li>';
					}
				}
			}
		}
		$output .= '</ul>';
		$output .= "</div>";
		$output .= "</div>";
		$output .= "<div id='contTab2' style='display: none'>";
		$output .= "<div id='contTab2'>";
		$result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = 'Categoria' and meta_value = '2'", ARRAY_N);
		$CantIm = 0;
		$clase = sanitize_title($Categoria2);
		$output .= '<ul id="slider2" class="sliderBefore '.$clase.'">';
		foreach( $result as $results )
		{
			$result2 = $wpdb->get_results("SELECT guid,post_parent FROM wp_posts WHERE ID = '$results[0]' ", ARRAY_N);
			foreach( $result2 as $results2 )
			{
				//echo $results2[1]."---".$IdEmpresa."<br>";
				if ($results2[1] == $IdEmpresa)
				{
					$result21 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$results[0]' and meta_key = 'Texto'", ARRAY_N);
						foreach( $result21 as $results21 )
						{
							$Texto = $results21[0];//$results2[0];
						}
					$CantIm++;
					if($CantIm < 6)
					{

						$output .= '<li class="sliderNum2"><img src="'.$results2[0].'" style="width:auto;height:auto "><ul class = "textoSlider"><li>'.$Texto.'</li></ul></li>';

					}
				}
			}
		}
		$output .= '</ul>';

	$output .= "</div></div>";
	$output .= "<div id='contTab3' style='display: none'>";
		$result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = 'Categoria' and meta_value = '3'", ARRAY_N);
		$CantIm = 0;
		$clase = sanitize_title($Categoria3);
		$output .= '<ul id="slider3" class="sliderBefore '.$clase.'">';

		foreach( $result as $results )
		{
			$result2 = $wpdb->get_results("SELECT guid,post_parent FROM wp_posts WHERE ID = '$results[0]' ", ARRAY_N);
			foreach( $result2 as $results2 )
			{
				//echo $results2[1]."---".$IdEmpresa."<br>";
				if ($results2[1] == $IdEmpresa)
				{
					$result21 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$results[0]' and meta_key = 'Texto'", ARRAY_N);
						foreach( $result21 as $results21 )
						{
							$Texto = $results21[0];//$results2[0];
						}
					$CantIm++;
					if($CantIm < 6)
					{
						$output .= '<li class="sliderNum3"><img src="'.$results2[0].'" style="width:auto ;height:auto"><ul class = "textoSlider"><li>'.$Texto.'</li></ul></li>';
					}
				}
			}
		}

		$output .= "</ul>";
		$output .= "</div>";
		//$output .= "</div>";
		$output .= "<div id='contTab4' style='display: none'>";
		$result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = 'Categoria' and meta_value = '4'", ARRAY_N);
		$CantIm = 0;
		$clase = sanitize_title($Categoria4);
		$output .= '<ul id="slider4" class="sliderBefore '.$clase.'">';

		foreach( $result as $results )
		{
			$result2 = $wpdb->get_results("SELECT guid,post_parent FROM wp_posts WHERE ID = '$results[0]' ", ARRAY_N);
			foreach( $result2 as $results2 )
			{
				//echo $results2[1]."---".$IdEmpresa."<br>";
				if ($results2[1] == $IdEmpresa)
				{
					$result21 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$results[0]' and meta_key = 'Texto'", ARRAY_N);
						foreach( $result21 as $results21 )
						{
							$Texto = $results21[0];//$results2[0];
						}
					$CantIm++;
					if($CantIm < 6)
					{
						$output .= '<li class="sliderNum4"><img src="'.$results2[0].'" style="width:auto ;height:auto"><ul class = "textoSlider"><li>'.$Texto.'</li></ul></li>';
					}
				}
			}
		}

		$output .= "</ul>";
		$output .= "</div>";


		return $output;

}
add_shortcode('Slider','Slider');



function insertSlider() {
	$IdEmpresa = get_the_ID();
	global $wpdb;
	global $post;
	$post->ID;
	$result = $wpdb->get_results("SELECT post_type FROM wp_posts WHERE ID = '$post->ID'", ARRAY_N) or die(mysql_error());
	foreach( $result as $results )
	{
		if($results[0] == "job_listing")
		{
			$type = $results[0];
		}
	}
	if($type == "job_listing")
	{
		$result = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE meta_key = '_featured' and meta_value = '1'", ARRAY_N) or die(mysql_error());
		foreach( $result as $results )
		{
				$featured = $results[0];
		}
	}
	if($featured == "1")
	{

      echo do_shortcode('[Slider]');
	}


}
add_shortcode('insertSlider','insertSlider');


function ReActualizarBase()
{
	global $wpdb;
	$result = $wpdb->get_results("SELECT ID,post_content FROM wp_posts ", ARRAY_N) ;
			foreach( $result as $results )
			{
				$mystring = $results[1];
				$findme   = 'Slider';
				$pos = strpos($mystring, $findme);
				//
				if ($pos === false) {
				}
				else
				{
					$wpdb->update( 'wp_posts', array( 'post_content' => ""), array( 'ID' => $results[0] ));
					//echo $results[0]."---".$results[1]."<br>";
				}
			}

}
add_shortcode('ReActualizarBase','ReActualizarBase');



add_action( 'wp_ajax_nopriv_load_job_subcategories', 'get_job_subcategories' );
add_action( 'wp_ajax_load_job_subcategories', 'get_job_subcategories' );
function get_job_subcategories(){
	global $wpdb;
	$parent = isset( $_POST['parent'] ) ? $_POST['parent'] : '';

	ob_start();
	wp_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'child_of' => $parent, 'hierarchical' => 1, 'show_option_all' => __( 'Subcategory', 'jobify' ), 'name' => 'search_subcategories', 'orderby' => 'name' ) );
	$buffer = ob_get_contents();
	ob_end_clean();

	$text2print = marsminds_translate_live($buffer);
	if(!$text2print)
		$text2print = '<select class="postform" id="search_subcategories" name="search_subcategories"></select>';

	echo $text2print;
	die();
}

add_action( 'wp_ajax_nopriv_get_custom_job_listings', 'get_custom_listings');
add_action( 'wp_ajax_get_custom_job_listings', 'get_custom_listings');

/**
 * Get listings via ajax
 */
function get_custom_listings() {

	global $job_manager, $wpdb;
    //die('aca');
	$result            		= array();
	$search_location   		= sanitize_text_field( stripslashes( $_POST['search_location'] ) );

	$search_keywords   		= sanitize_text_field( stripslashes( $_POST['search_keywords'] ) );
	//$search_keywords 		= marsminds_translate_live($search_keywords, $to_lang='es');

	//echo '<pre>';
	//print_r($search_keywords);
	//echo '</pre>';

	$search_categories 		= isset( $_POST['search_categories'] ) ? $_POST['search_categories'] : '';
	$search_subcategories 	= isset( $_POST['search_subcategories'] ) ? $_POST['search_subcategories'] : '';
	$search_job_types		= isset( $_POST['search_job_types'] ) ? $_POST['search_job_types'] : '';
	$filter_job_types  		= isset( $_POST['filter_job_type'] ) ? array_filter( array_map( 'sanitize_title', (array) $_POST['filter_job_type'] ) ) : null;

	if ( is_array( $search_job_types ) ) {
		$search_job_types = array_filter( array_map( 'sanitize_text_field', array_map( 'stripslashes', $search_job_types ) ) );
	} else {
		$search_job_types = array_filter( array( sanitize_text_field( stripslashes( $search_job_types ) ) ) );
	}

	if ( is_array( $search_categories ) ) {
		$search_categories = array_filter( array_map( 'sanitize_text_field', array_map( 'stripslashes', $search_categories ) ) );
	} else {
		$search_categories = array_filter( array( sanitize_text_field( stripslashes( $search_categories ) ) ) );
	}

	if ( is_array( $search_subcategories ) ) {
		$search_subcategories = array_filter( array_map( 'sanitize_text_field', array_map( 'stripslashes', $search_subcategories ) ) );
	} else {
		$search_subcategories = array_filter( array( sanitize_text_field( stripslashes( $search_subcategories ) ) ) );
	}
    
	$_SESSION['entro2'] = $search_location;
	$args = array(
		'search_location'   	=> $search_location,
		'search_keywords'   	=> $search_keywords,
		'search_categories' 	=> $search_categories,
		'search_subcategories' 	=> $search_subcategories,
		//'job_types'         	=> is_null( $filter_job_types ) ? '' : $filter_job_types + array( 0 ),
		'search_job_types'		=> $search_job_types,
		'orderby'           	=> sanitize_text_field( $_POST['orderby'] ),
		'order'            	 	=> sanitize_text_field( $_POST['order'] ),
		'offset'            	=> ( absint( $_POST['page'] ) - 1 ) * absint( $_POST['per_page'] ),
		'posts_per_page'    	=> absint( $_POST['per_page'] )
	);
    
	$jobs = get_custom_job_listings( apply_filters( 'job_manager_get_listings_args', $args ) );
	$result['found_jobs'] = false;

	$job_cats = get_terms('job_listing_category', array( 'parent' => 0 ));
	$cur_cat = current($job_cats);
	ob_start();
	if(is_array($jobs)){
		foreach ($jobs as $job) {
			if ( $job->have_posts() ) : $result['found_jobs'] = true; ?>
				<li>
					<?php $post_category = $cur_cat;?>
					<ul class="cat-container lista-<?php echo $post_category->slug;?>">
						<li>

							<div class="rectangulo-<?php echo $post_category->slug;?>"><p align="left"><?php echo $post_category->name;?></p><div>
						</li>
						<?php while ( $job->have_posts() ) : $job->the_post(); ?>
							<?php get_job_manager_template_part( 'content', 'job_listing' ); ?>
						<?php endwhile; ?>
					</ul>
				</li>
			<?php endif;
			$cur_cat = next($job_cats);
		}
	}
	else{
		if ( $jobs->have_posts() ) : $result['found_jobs'] = true; ?>

			<?php while ( $jobs->have_posts() ) : $jobs->the_post(); ?>

				<?php get_job_manager_template_part( 'content', 'job_listing' ); ?>

			<?php endwhile; ?>

		<?php else : ?>

			<?php get_job_manager_template_part( 'content', 'no-jobs-found' ); ?>

		<?php endif;
	}


	$result['html'] = ob_get_clean();

	// Generate 'showing' text
	$types = get_job_listing_types();

	if ( sizeof( $filter_job_types ) > 0 && ( sizeof( $filter_job_types ) !== sizeof( $types ) || $search_keywords || $search_location || $search_categories || apply_filters( 'job_manager_get_listings_custom_filter', false ) ) ) {
		$showing_types = array();
		$unmatched     = false;

		foreach ( $types as $type ) {
			if ( in_array( $type->slug, $filter_job_types ) )
				$showing_types[] = $type->name;
			else
				$unmatched = true;
		}

		if ( ! $unmatched )
			$showing_types  = '';
		elseif ( sizeof( $showing_types ) == 1 ) {
			$showing_types  = implode( ', ', $showing_types ) . ' ';
		} else {
			$last           = array_pop( $showing_types );
			$showing_types  = implode( ', ', $showing_types );
			$showing_types .= " &amp; $last ";
		}

		$showing_categories = array();

		if ( $search_categories ) {
			foreach ( $search_categories as $category ) {
				if ( ! is_numeric( $category ) ) {
					$category_object = get_term_by( 'slug', $category, 'job_listing_category' );
				}
				if ( is_numeric( $category ) || is_wp_error( $category_object ) || ! $category_object ) {
					$category_object = get_term_by( 'id', $category, 'job_listing_category' );
				}
				if ( ! is_wp_error( $category_object ) ) {
					$showing_categories[] = $category_object->name;
				}
			}
		}

		if ( $search_keywords ) {
			$showing_jobs  = sprintf( __( 'Showing %s&ldquo;%s&rdquo; %sjobs', 'wp-job-manager' ), $showing_types, $search_keywords, implode( ', ', $showing_categories ) );
		} else {
			$showing_jobs  = sprintf( __( 'Showing all %s%sjobs', 'wp-job-manager' ), $showing_types, implode( ', ', $showing_categories ) . ' ' );
		}

		$showing_location  = $search_location ? sprintf( ' ' . __( 'located in &ldquo;%s&rdquo;', 'wp-job-manager' ), $search_location ) : '';

		$result['showing'] = apply_filters( 'job_manager_get_listings_custom_filter_text', $showing_jobs . $showing_location );

	} else {
		$result['showing'] = '';
	}

	// Generate RSS link
	$result['showing_links'] = job_manager_get_filtered_links( array(
		'filter_job_types'  => $filter_job_types,
		'search_location'   => $search_location,
		'search_categories' => $search_categories,
		'search_keywords'   => $search_keywords
	) );

	$result['max_num_pages'] = $jobs->max_num_pages;

	//$result['html'] = marsminds_translate_live($result['html']);
	//$result['showing_links'] = marsminds_translate_live($result['showing_links']);

	echo '<!--WPJM-->';
	//Patch when filtering by keyword and no category
	if($result['found_jobs'] == false && $jobs->max_num_pages == null){
		$result['html'] = '<li class="no_job_listings_found">No hay empresas para su selección.</li>';
		echo json_encode($result);
	}
	else{
		echo json_encode( apply_filters( 'job_manager_get_listings_result', $result ) );
	}

	echo '<!--WPJM_END-->';

	die();

}

if ( ! function_exists( 'get_custom_job_listings' ) ) :
/**
 * Queries job listings with certain criteria and returns them
 *
 * @access public
 * @return void
 */
function get_custom_job_listings( $args = array() ) {
	global $wpdb;
    
	$args = wp_parse_args( $args, array(
		'search_location'   => '',
		'search_keywords'   => '',
		'search_categories' => array(),
		'search_subcategories' => array(),
		'job_types'         => array(),
		'offset'            => '',
		'posts_per_page'    => '',
		'orderby'           => 'date',
		'order'             => 'DESC'
	) );
  
	$query_args = array(
		'post_type'           => 'job_listing',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'offset'              => absint( $args['offset'] ),
		'posts_per_page'      => -1,
		'orderby'             => $args['orderby'],
		'order'               => $args['order'],
		'tax_query'           => array(),
		'meta_query'          => array()
	);

	// if ( ! empty( $args['job_types'] ) )
	// 	$query_args['tax_query'][] = array(
	// 		'taxonomy' => 'job_listing_type',
	// 		'field'    => 'slug',
	// 		'terms'    => $args['job_types']
	// 	);

	if ( ! empty( $args['search_categories'] ) ) {
		if( ! empty( $args['search_subcategories'] ) ) {
			$field = is_numeric( $args['search_subcategories'][0] ) ? 'term_id' : 'slug';
			$query_args['tax_query'][] = array(
				'taxonomy' => 'job_listing_category',
				'field'    => $field,
				'terms'    => $args['search_subcategories']
			);
		}
		else{
			$field = is_numeric( $args['search_categories'][0] ) ? 'term_id' : 'slug';

			$query_args['tax_query'][] = array(
				'taxonomy' => 'job_listing_category',
				'field'    => $field,
				'terms'    => $args['search_categories']
			);
			// $query_args['meta_query'][] = array(
			// 	'key' => '_job_primarycategory',
			// 	'value'    => $args['search_categories'][0],
			// 	'compare'    => '='
			// );
		}
	}

	if ( ! empty( $args['search_job_types'] ) ) {
		$field = is_numeric( $args['search_job_types'][0] ) ? 'term_id' : 'slug';

		$query_args['tax_query'][] = array(
			'taxonomy' => 'job_listing_type',
			'field'    => $field,
			'terms'    => $args['search_job_types']
		);
	}
    $keyword_post_ids = array();
    
	if ( ! empty( $args['search_keywords'] ) )
	{
	  // Custom queries.
	  $new_args = array(
			'post_type'		=>	'job_listing',
			'post_status'         => 'publish',
            'ignore_sticky_posts' => 1,
			'meta_query'	=>	array(
				array(
					'key'	=>	'etiqueta_personal',
					'value'	=>	$args['search_keywords'],
                    'compare' => 'LIKE'
				)
			)
		);
        $postTitles = $wpdb->get_results( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE LOWER(post_title) LIKE %s AND post_type='job_listing'", '%'.strtolower($args['search_keywords']).'%' ));
        foreach($postTitles as $matchTitle){
          $keyword_post_ids[] = $matchTitle->ID;
        }
        
        $q2 = get_posts($new_args);
        foreach($q2 as $mPost)
        {
          $keyword_post_ids[] = $mPost->ID;
        }

		// taxonomies
		$taxes = array('tag');
		$tax_map = array();
		foreach ( $taxes as $tax )
		{
		    $terms = get_terms( $tax, array('name__like' => $args['search_keywords']) );

		    foreach ( $terms as $term )
			{
				$tax_map[$tax][] = $term->term_taxonomy_id;
		    }
		}

		$field = 'term_taxonomy_id';

		if(isset($tax_map['tag'])){
			$query_args['tax_query'] = array(
				'taxonomy' => 'tag',
				'field'    => $field,
				'terms'    => $tax_map['tag'],
				'operator' => 'IN'
			);
		}
		// Hack to show no results when no matches - dirty
		if(empty($tax_map)){
			$query_args['tax_query'] = array(
				'taxonomy' => 'tag',
				'field'    => $field,
				'terms'    => $tax_map['tag'],
				'operator' => 'IN'
			);
		}
	}

	if ( get_option( 'job_manager_hide_filled_positions' ) == 1 )
		$query_args['meta_query'][] = array(
			'key'     => '_filled',
			'value'   => '1',
			'compare' => '!='
		);

	// Location search - search geolocation data and location meta
	if ( $args['search_location']  ) {


		$location_post_ids = array_merge( $wpdb->get_col( $wpdb->prepare( "
		    SELECT DISTINCT post_id FROM {$wpdb->postmeta}
		    WHERE meta_key IN ( 'geolocation_city', 'geolocation_country_long', 'geolocation_country_short', 'geolocation_formatted_address', 'geolocation_state_long', 'geolocation_state_short', 'geolocation_street', 'geolocation_zipcode', '_job_location' )
		    AND meta_value LIKE '%%%s%%'
		", $args['search_location'] ) ), array( 0 ) );
	} else {


		$location_post_ids = array();

	}
	// Merge post ids
	if ( ! empty( $location_post_ids ) && ! empty( $keyword_post_ids )  ) {
		$_SESSION['e1'] = "entro1";
		$query_args['post__in'] = array_intersect( $location_post_ids, $keyword_post_ids );
	} elseif ( ! empty( $location_post_ids ) || ! empty( $keyword_post_ids ) ) {
		$_SESSION['e1'] = "entro2";
		$query_args['post__in'] = array_merge( $location_post_ids, $keyword_post_ids );
	}

    


	$query_args = apply_filters( 'job_manager_get_listings', $query_args );



	if ( empty( $query_args['meta_query'] ) )
		unset( $query_args['meta_query'] );

	if ( empty( $query_args['tax_query'] ) )
		unset( $query_args['tax_query'] );

	if ( $args['orderby'] == 'featured' ) {
		$query_args['orderby'] = 'meta_key';
		$query_args['meta_key'] = '_featured';
	}

	if ( empty( $args['search_categories'] ) && empty($query_args['post__in'])) {
		$terms = get_terms('job_listing_category', array( 'parent' => 0 ));
		foreach ($terms as $term) {
			$query_args['meta_query'][] = array(
				'key' => '_job_primarycategory',
				'value'    => $term->term_id,
				'compare'    => '='
			);

			add_filter( 'posts_clauses', 'order_featured_job_listing' );

			$_SESSION['A1'] = $query_args;
			// Filter args by category
			$query_args = apply_filters( 'get_job_listings_query_args', $query_args );
			$_SESSION['A2'] = $query_args;


			do_action( 'before_get_job_listings', $query_args );

			//$query_args['post__in'] = $_SESSION['PruebaFinal'];
			$res = new WP_Query( $query_args );
			//$res = new WP_Query( array('post_type' => 'job_listing','post__in' => $_SESSION['PruebaFinal']) );
			////////////////////////////////////////////////////////////////////////



			////////////////////////////////////////////////////////////////////////


			do_action( 'after_get_job_listings', $query_args );

			remove_filter( 'posts_clauses', 'order_featured_job_listing' );

			unset( $query_args['meta_query'] );

			$result[] = $res;
		}

	}
	else
	{



		add_filter( 'posts_clauses', 'order_featured_job_listing' );

		// Filter args
		$query_args = apply_filters( 'get_job_listings_query_args', $query_args );

		do_action( 'before_get_job_listings', $query_args );

		$result = new WP_Query( $query_args );
		do_action( 'after_get_job_listings', $query_args );

		remove_filter( 'posts_clauses', 'order_featured_job_listing' );


	}

	return $result;
}
endif;

add_action( 'ninja_forms_pre_process', 'change_ninja_forms_email' );
function change_ninja_forms_email(){
  global $ninja_forms_processing; // The global variable gives us access to all the form and field settings.

  $form_id = $ninja_forms_processing->get_form_ID(); // Gets the ID of the form we are currently processing.
  if( $form_id == 1 ){ // Check to make sure that this form has the same ID as the one we got earlier.
    $c_id = $ninja_forms_processing->get_field_value( 16 );
    $c_mail = get_post_meta( $c_id, '_application', true );

    $admin_mailto = $ninja_forms_processing->get_form_setting('admin_mailto');
    array_push($admin_mailto, $c_mail);

    $ninja_forms_processing->update_form_setting( 'admin_mailto', $admin_mailto );
  }
}

function custom_admin_js() {
    $url = get_bloginfo('template_directory') . '/js/wp-admin.js';
    echo '"<script type="text/javascript" src="'. $url . '"></script>"';
}
add_action('admin_footer', 'custom_admin_js');


function delete_obsolete_job_tags($data, $postarr)
{
	$post_id = $postarr['ID'];

	if (get_post_type($post_id) == 'job_listing') {

		$title = get_the_title($post_id);
		$code  = get_post_meta($post_id, '_job_code', true);

		$tax_title = get_term_by('name', $title, 'tag');
		$tax_code = get_term_by('name', $code, 'tag');

		$childs = get_term_children( $tax_title->term_id, 'tag' );
		if(empty($childs)) {
			wp_delete_term($tax_title->term_id, 'tag');
		}
		wp_delete_term($tax_code->term_id, 'tag');
	}

	return $data;
}
add_filter( 'wp_insert_post_data', 'delete_obsolete_job_tags', '99', 2 );

function create_job_tags($post_id)
{
	if (get_post_type($post_id) == 'job_listing') {
		$title = get_the_title($post_id);
		$code  = get_post_meta($post_id, '_job_code', true);
		wp_set_object_terms($post_id, array($title, $code), 'tag', true);
	}
}
add_action('save_post', 'create_job_tags');

function marsminds_translate_live($text, $to_lang=false) {
	$referer_uri = str_replace('http://'.$_SERVER['SERVER_NAME'], '', $_SERVER['HTTP_REFERER']);
	$url_parts = explode('/', $referer_uri);
	$lang = $url_parts[1];
	if(strlen($lang)!=2)
		$lang = 'es';

	if($lang=='es' || $lang=='')
		return $text;
	elseif(trim($text)) {
		if($to_lang)
			$lang = $to_lang;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://renjith.co.in/translate/translator.php?text='.urlencode($text).'&to='.$lang);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$result_array = json_decode($result, true);

		$text2print = str_replace('<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">', '', $result_array['translation']);
		$text2print = str_replace('\\\'', '', $text2print);
		$text2print = str_replace('\\"', '', $text2print);
		$text2print = str_replace('</string>', '', $text2print);
		$text2print = str_replace('&lt;', '<', $text2print);
		$text2print = str_replace('&gt;', '>', $text2print);

		return $text2print;
	} else {
		return false;
	}
}

add_filter('home_url', 'marsminds_add_translation_support');
function marsminds_add_translation_support($string) {
	$referer_uri = str_replace('http://'.$_SERVER['SERVER_NAME'], '', $_SERVER['REQUEST_URI']);
	$url_parts = explode('/', $referer_uri);
	$lang = $url_parts[1];
	if(strlen($lang)!=2)
		$lang = 'es';

	if($lang!='es')
		$string = str_replace($_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST'].'/'.$lang, $string);

	return $string;
}

function randomNumbersWithinRange($min, $max, $quantity) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}

/**
* Function to change ajax post.
**/
function change_image_text(){
	$result = update_post_meta($_POST['id'], 'Texto', $_POST['text']);
	echo json_encode(array('success' => true, 'result' => $result));
	die;
}

add_action('wp_ajax_change_image_text', 'change_image_text');