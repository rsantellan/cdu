<?php
/**
 *
 */

if ( 'side' == jobify_theme_mod( 'jobify_listings', 'jobify_listings_display_area' ) )
	return;

$columns = jobify_theme_mod( 'jobify_listings', 'jobify_listings_topbar_columns' );
$colspan = jobify_theme_mod( 'jobify_listings', 'jobify_listings_topbar_colspan' );
$colspan = array_map( 'trim', explode( ' ', $colspan ) );

$args    = array(
	'before_widget' => '<aside class="job_listing-widget-top default-widget">',
	'after_widget'  => '</aside>',
	'before_title'  => '<h3 class="job_listing-widget-title-top">',
	'after_title'   => '</h3>'
);
?>

<div class="job-meta-top row">
	<?php $colums_size = array(1 => 2, 2 => 6, 3 => 4); ?>

	<?php for ( $i = 1; $i <= $columns; $i++ ) :?>
		<div class="col-md-<?php echo $colums_size[$i]; ?> col-sm-6 col-xs-12">
			<?php if ( ! is_active_sidebar( 'single-job_listing-top-' . $i ) ) : ?>

				<?php if ( 1 == $i ) : ?>

					<?php the_widget( 'Jobify_Widget_Job_Company_Logo', array(), $args ); ?>

				<?php elseif ( 2 == $i ) : ?>
					<aside class="job_listing-widget-top default-widget" style="margin-bottom: 15px;">
						<?php echo '<p>' . get_post_meta(get_the_ID(), '_job_information', true) . '</p>'; ?>
					</aside>
					<?php if( get_the_job_type()->slug != 'instituto-de-formacion'): ?>
					
						<?php if ( ! class_exists( 'WP_Job_Manager_Job_Tags' ) ) : ?>
							<?php the_widget( 'Jobify_Widget_Job_Categories', '', $args ); ?>
						<?php else : ?>
							<?php the_widget( 'Jobify_Widget_Job_Tags', array( 'title' => __( 'Job Tags', 'jobify' ) ), $args ); ?>
						<?php endif; ?>
					<?php endif; ?>

					<?php // the_widget( 'Jobify_Widget_Job_Share', array( 'title' => __( 'Share Job Posting', 'jobify' ) ), $args ); ?>

				<?php elseif ( 3 == $i ) : ?>
					<?php
					$code 		= get_post_meta(get_the_ID(), '_job_code', true);
					$year 		= get_post_meta(get_the_ID(), '_job_creation_year', true);
					$employees 	= get_post_meta(get_the_ID(), '_job_employees', true);
					$bilingual	= get_post_meta(get_the_ID(), '_job_bilingual', true);
					$address	= get_post_meta(get_the_ID(), '_job_address', true);
					$city 		= get_post_meta(get_the_ID(), '_job_city', true);
					$phone 		= get_post_meta(get_the_ID(), '_job_phone', true);
					$mobile 	= get_post_meta(get_the_ID(), '_job_mobile', true);
					?>
					<aside class="job_listing-widget-top default-widget" style="margin-bottom: 15px;">
						<?php if(!empty($code)) 		echo '<b>' . __( 'Job code', 'wp-job-manager' ) 			.':</b> ' . get_post_meta(get_the_ID(), '_job_code', true) . '</br>'; ?>
						<?php if(!empty($year)) 		echo '<b>' . __( 'Job creation year', 'wp-job-manager' ) 	.':</b> ' . get_post_meta(get_the_ID(), '_job_creation_year', true) . '</br>'; ?>
						<?php if(!empty($employees)) 	echo '<b>' . __( 'Job employees', 'wp-job-manager' ) 		.':</b> ' . get_post_meta(get_the_ID(), '_job_employees', true) . '</br>'; ?>
						<?php if(!empty($address)) 		echo '<b>' . __( 'Job address', 'wp-job-manager' ) 			.':</b> ' . get_post_meta(get_the_ID(), '_job_address', true) . '</br>'; ?>
						<?php if(!empty($city)) 		echo '<b>' . __( 'Job city', 'wp-job-manager' ) 			.':</b> ' . get_post_meta(get_the_ID(), '_job_city', true) . '</br>'; ?>
						<?php if(!empty($phone)) 		echo '<b>' . __( 'Job phone', 'wp-job-manager' ) 			.':</b> ' . get_post_meta(get_the_ID(), '_job_phone', true) . '</br>'; ?>
						<?php if(!empty($mobile)) 		echo '<b>' . __( 'Job mobile', 'wp-job-manager' ) 			.':</b> ' . get_post_meta(get_the_ID(), '_job_mobile', true) . '</br>'; ?>
						<?php if(!empty($bilingual) && get_post_meta(get_the_ID(), '_job_bilingual', true)) echo '<b>' . __( 'Job bilingual', 'wp-job-manager' ) 		.'</b> ' . '</br>'; ?>
					</aside>
					
					<?php the_widget( 'Jobify_Widget_Job_Company_Social', '', $args ); ?>

					<?php the_widget( 'Jobify_Widget_Job_Apply', array(), $args ); ?>

				<?php endif; ?>

			<?php else : ?>

				<?php dynamic_sidebar( 'single-job_listing-top-' . $i ); ?>

			<?php endif; ?>
		</div>
	<?php endfor; ?>

</div>