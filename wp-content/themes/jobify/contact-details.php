<div class="resume_contact">
	<input class="resume_contact_button" type="button" value="<?php esc_attr_e( 'Contact', 'jobify' ); ?>" />

	<div class="modal resume_contact_details">
		<h2 class="modal-title"><?php _e( 'Contact', 'jobify' ); ?></h2>

		<div class="resume-contact-content">
			<?php if ( resume_manager_user_can_view_contact_details( $post->ID ) ) : ?>
				<?php do_action( 'jobify_resume_manager_application_details_email' ); ?>
			<?php else : ?>
				<?php _e( 'Sorry, you do not have permission to contact this candidate.', 'jobify' ); ?>
			<?php endif; ?>
		</div>
	</div>
</div>