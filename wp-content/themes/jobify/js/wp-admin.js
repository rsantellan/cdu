jQuery('#tagchecklist').find('input').each(function(index, input) {
    jQuery(input).bind('change', function() {
      var checkbox = jQuery(this);
      var is_checked = jQuery(checkbox).is(':checked');
      if(is_checked) {
        jQuery(this).parent('label').next().find('input').prop('checked', true);
      } else {
        jQuery(this).parent('label').next().find('input').prop('checked', false);
      }
    });
  });

jQuery('#job_listing_categorychecklist').find('input').each(function(index, input) {
    jQuery(input).bind('change', function() {
      var checkbox = jQuery(this);
      var is_checked = jQuery(checkbox).is(':checked');
      if(is_checked) {
        jQuery(this).parent('label').next().find('input').prop('checked', true);
      } else {
        jQuery(this).parent('label').next().find('input').prop('checked', false);
      }
    });
  });