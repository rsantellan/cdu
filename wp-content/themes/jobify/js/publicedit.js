var $j = jQuery.noConflict();

$j(function(){
  sacaroption();
  startSliders();
});

function startSliders()
{
  if($j('#slider0').length > 0)
  {
    $j('#slider0').bxSlider({
          mode: 'fade',
          pager: true,
          auto: true,
          slideHeight: 490,
          slideWidth: 1100,
          //autoControls: true,
          responsive: true,
          pause: 4000
        });
  }  
  if($j('#slider1').length > 0)
  {
    $j('#slider1').bxSlider({
          mode: 'fade',
          pager: true,
          auto: true,
          slideHeight: 490,
          slideWidth: 1100,
          //autoControls: true,
          responsive: true,
          pause: 4000
        });
  }  
  if($j('#slider2').length > 0)
  {
    $j('#slider2').bxSlider({
          mode: 'fade',
          pager: true,
          auto: true,
          slideHeight: 490,
          slideWidth: 1100,
          //autoControls: true,
          responsive: true,
          pause: 4000
        });
  }  
  if($j('#slider3').length > 0)
  {
    $j('#slider3').bxSlider({
          mode: 'fade',
          pager: true,
          auto: true,
          slideHeight: 490,
          slideWidth: 1100,
          //autoControls: true,
          responsive: true,
          pause: 4000
        });
  }  
  if($j('#slider4').length > 0)
  {
    $j('#slider4').bxSlider({
          mode: 'fade',
          pager: true,
          auto: true,
          slideHeight: 490,
          slideWidth: 1100,
          //autoControls: true,
          responsive: true,
          pause: 4000
        });
  }  
  
}


        

function ModificarTextoAjax(id)
{
    var data = {
		action: 'change_image_text',
		id: id, 
		text: $j("#ModTextArea"+id).val()
	};
	var ajax_url = "/wp-admin/admin-ajax.php";
	$j.ajax({
		url: ajax_url,
		data: data,
		type: 'post',
		dataType: 'json',
		success: function(json){
		  if(json.result || json.result == 'true')
		  {
			//alert('Texto modificado');
			$j('#ModTextAreaContent'+id).html($j("#ModTextArea"+id).val());
			$j('#ModTextArea'+id).hide();
			$j('#btntxtMod'+id).hide();
			$j('#botMostTxt'+id).show();
			
		  }
		}, 
		complete: function()
		{
		}
	  });
}

function borrarImagen(img,ruta)
{
  $j.get( "/BorrarImagen.php?img="+img+"&imagen="+ruta, function( data ) {
    //console.log(data );
    var obj = jQuery.parseJSON(data);
    console.log(obj);
    console.log(obj.result);
    if(obj.result == true){
      console.log('aca');
       $j('#divId'+obj.id).remove();
    }
    
   });
  return ;
}

function MostrarSiguiente(categoria,numero){
  document.getElementById("imagen"+categoria+numero).style.display="block";
  document.getElementById("texto"+categoria+numero).style.display="block";
}

function MostrarCampo(id,id2,id3)
{
    document.getElementById(id).style.display="block";
    document.getElementById(id2).style.display="none";
    document.getElementById(id3).style.display="block";

}

function sacaroption()
{
    var id = $j('#job_primarycategory').val();
    $j('#chk'+id).hide();
    $j('#job_primarycategory').prop('checked', true);
    switch(id){
      case "graficopackaging":
        $j("chkinstitutos-de-formacion").show();
        $j("chkinteriorismo-paisajismo").show();
        $j("chkproducto").show();
        $j("chktextil-indumentaria").show();
        $j("chkweb-multimedia").show();
        break;
      case "institutos-de-formacion":
        $j("chkgraficopackaging").show();
        $j("chkinteriorismo-paisajismo").show();
        $j("chkproducto").show();
        $j("chktextil-indumentaria").show();
        $j("chkweb-multimedia").show();
        break;
      case "interiorismo-paisajismo":
        $j("chkgraficopackaging").show();
        $j("chkinteriorismo-paisajismo").show();
        $j("chkproducto").show();
        $j("chktextil-indumentaria").show();
        $j("chkweb-multimedia").show();
        break;
      case "producto":
        $j("chkgraficopackaging").show();
        $j("chkinteriorismo-paisajismo").show();
        $j("chkproducto").show();
        $j("chktextil-indumentaria").show();
        $j("chkweb-multimedia").show();
        break;
      case "textil-indumentaria":
        $j("chkgraficopackaging").show();
        $j("chkinteriorismo-paisajismo").show();
        $j("chkproducto").show();
        $j("chktextil-indumentaria").show();
        $j("chkweb-multimedia").show();
        break;
      case "web-multimedia":
        $j("chkgraficopackaging").show();
        $j("chkinteriorismo-paisajismo").show();
        $j("chkproducto").show();
        $j("chktextil-indumentaria").show();
        $j("chkweb-multimedia").show();
        break;
    }
    return;
}
