<?php
/**
 * Job Type
 *
 * @package Jobify
 * @since Jobify 1.0
 */

$taxonomy = get_taxonomy( get_queried_object()->taxonomy );

get_header(); ?>
	<style>
		#jobify_widget_map{
			margin-bottom: 0px;
		}
	</style>

	<div class="application">
		<div class="dir_application_details animated modal">
			<h2 class="modal-title"><?php _e( 'Apply', 'jobify' ); ?></h2>

			<div class="application-content">
			<?php
				/**
				 * job_manager_application_details_email or job_manager_application_details_url hook
				 */
				do_action( 'job_manager_application_details_email', false );
			?>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<?php
			the_widget(
				'Jobify_Widget_Map',
				apply_filters( 'jobify_widget_map_page_template', array(
					'search'        => '',
					'pins'          => 20,
					'scrollWheel'   => false,
					'clusterColor'  => jobify_theme_mod( 'colors', 'primary' ),
					'clusterRadius' => 200,
					'zoom'          => 'auto',
					'center'        => 'autofit',
				) ),
				array(
					'before_widget' => sprintf( '<section id="%1$s" class="homepage-widget %2$s">', 'jobify_widget_map', 'jobify_widget_map' ),
					'after_widget'  => '</section>',
					'before_title'  => '<h3 class="homepage-widget-title">',
					'after_title'   => '</h3>',
					'widget_id' => 'jobify_widget_map-999'
				)
			);
		?>
		<div id="content" class="container" role="main">

			<div class="entry-content">

				<?php
				add_filter( 'posts_clauses', 'order_featured_job_listing' );

				global $wp_query;
				query_posts(
					array_merge(
						$wp_query->query,
						array(
							'orderby' => 'meta_key',
							'meta_key' => '_featured',
							'order' => 'DESC',
							'posts_per_page' => -1
						)
					)
				);

				remove_filter( 'posts_clauses', 'order_featured_job_listing' );
				?>

				<?php if ( have_posts() ) : ?>
				<div class="job_listings">
					<ul class="job_listings">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_job_manager_template_part( 'content', 'job_listing' ); ?>
						<?php endwhile; ?>
					</ul>
				</div>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>

			</div>
		</div><!-- #content -->

		<?php do_action( 'jobify_loop_after' ); ?>
	</div><!-- #primary -->

<?php get_footer(); ?>
