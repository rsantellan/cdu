<?php

if( !function_exists('get_graphics_categories'))
{
  function get_graphics_categories()
  {
    $SubGrafico = array();
	$SubGrafico[] = array(sanitize_title("Cartelería Señalética"), 312, "Cartelería Señalética");
	$SubGrafico[] = array(sanitize_title("Catálogos"), 313, "Catálogos");
	$SubGrafico[] = array(sanitize_title("Diseño Editorial"), 137, "Diseño Editorial");
	$SubGrafico[] = array(sanitize_title("Gráfica publicitaria"), 314, "Gráfica publicitaria");
	$SubGrafico[] = array(sanitize_title("Identidad corporativa"), 315, "Identidad corporativa");
	$SubGrafico[] = array(sanitize_title("Ilustración"), 106, "Ilustración");
	$SubGrafico[] = array(sanitize_title("Infografía"), 213, "Infografía");
	$SubGrafico[] = array(sanitize_title("Packaging y Promocional"), 101, "Packaging y Promocional");
	$SubGrafico[] = array(sanitize_title("Tipografía"), 138, "Tipografía");
    return $SubGrafico;
  }
}

if( !function_exists('get_interiors_categories'))
{
  function get_interiors_categories()
  {
    $SubInteriorismo = array();
	$SubInteriorismo[] = array(sanitize_title("Comercial"), 319, "Comercial");
	$SubInteriorismo[] = array(sanitize_title("Escenografía"), 231, "Escenografía");
	$SubInteriorismo[] = array(sanitize_title("Iluminación"), 317, "Iluminación");
	$SubInteriorismo[] = array(sanitize_title("Montajes Expositivos"), 316, "Montajes Expositivos");
	$SubInteriorismo[] = array(sanitize_title("Stands"), 153, "Stands");
	$SubInteriorismo[] = array(sanitize_title("Vivienda"), 320, "Vivienda");
    return $SubInteriorismo;
  }
}

if( !function_exists('get_products_categories'))
{
  function get_products_categories()
  {
    $SubProducto = array();
	$SubProducto[] = array(sanitize_title("Aparatos eléctricos y electrónicos"), 321, "Aparatos eléctricos y electrónicos");
	$SubProducto[] = array(sanitize_title("Envase y embalaje"), 322, "Envase y embalaje");
	$SubProducto[] = array(sanitize_title("Equipamiento urbano"), 323, "Equipamiento urbano");
	$SubProducto[] = array(sanitize_title("Juegos"), 324, "Juegos");
	$SubProducto[] = array(sanitize_title("Mobiliario e iluminación"), 177, "Mobiliario e iluminación");
	$SubProducto[] = array(sanitize_title("Regalos empresariales"), 325, "Regalos empresariales");
	$SubProducto[] = array(sanitize_title("Transporte"), 326, "Transporte");
    return $SubProducto;
  }
}

if( !function_exists('get_textils_categories'))
{
  function get_textils_categories()
  {
    $SubTextil = array();
	$SubTextil[] = array(sanitize_title("Accesorios"), 201, "Accesorios");
	$SubTextil[] = array(sanitize_title("Alta costura"), 329, "Alta costura");
	$SubTextil[] = array(sanitize_title("Calzado"), 200, "Calzado");
	$SubTextil[] = array(sanitize_title("Lencería"), 327, "Lencería");
	$SubTextil[] = array(sanitize_title("Moda Urbana"), 331, "Moda Urbana");
	$SubTextil[] = array(sanitize_title("Moldería"), 202, "Moldería");
	$SubTextil[] = array(sanitize_title("Sastrería"), 330, "Sastrería");
	$SubTextil[] = array(sanitize_title("Tejidos"), 197, "Tejidos");
	$SubTextil[] = array(sanitize_title("Vestuario"), 328, "Vestuario");
	return $SubTextil;
  }
}

if( !function_exists('get_web_categories'))
{
  function get_web_categories()
  {
    $SubWeb = array();
	$SubWeb[] = array(sanitize_title("Aplicaciones"), 211, "Aplicaciones");
	$SubWeb[] = array(sanitize_title("Experiencia de usuario / UX"), 333, "Experiencia de usuario / UX");
	$SubWeb[] = array(sanitize_title("Infografía"), 334, "Infografía");
	return $SubWeb;
  }
}

if( !function_exists('get_web_tags'))
{
  function get_web_tags()
  {
    $TagWeb = array();
    $TagWeb[212] = "Animación";
    $TagWeb[211] = "Aplicaciones";
    $TagWeb[222] = "Desarrollo web";
    $TagWeb[214] = "Diseño de Sitios";
    $TagWeb[210] = "Diseño Multimedia";
    $TagWeb[209] = "Diseño Web";
    $TagWeb[224] = "Efectos Visuales";
    $TagWeb[219] = "Experiencia de usuario";
    $TagWeb[216] = "Gráfica audiovisual";
    $TagWeb[213] = "Infografía";
    $TagWeb[220] = "Interacción";
    $TagWeb[218] = "Juegos interactivos";
    $TagWeb[217] = "Medios audiovisuales";
    $TagWeb[215] = "Programación";
    $TagWeb[223] = "UI / UX";
    $TagWeb[221] = "UX";
    return $TagWeb;
  }
}

if( !function_exists('get_textil_tags'))
{
  function get_textil_tags()
  {
    $TagTextil = array();
    $TagTextil[201] = "Accesorios";
    $TagTextil[200] = "Calzado";
    $TagTextil[198] = "Complementos personales";
    $TagTextil[204] = "Cuero";
    $TagTextil[194] = "Diseño de Indumentaria";
    $TagTextil[195] = "Diseño de Moda";
    $TagTextil[226] = "Diseño de vestuario";
    $TagTextil[193] = "Diseño Textil";
    $TagTextil[196] = "Estampados";
    $TagTextil[205] = "Hilados";
    $TagTextil[202] = "Moldería";
    $TagTextil[207] = "Ropa Interior";
    $TagTextil[206] = "Tejido de punto";
    $TagTextil[197] = "Tejidos";
    $TagTextil[203] = "Uniformes";
    $TagTextil[199] = "Vestidos";
    return $TagTextil;
  }
}


if( !function_exists('get_productos_tags'))
{
  function get_productos_tags()
  {
    $TagProducto = array();
    $TagProducto[167] = "Aparatos electrónicos";
    $TagProducto[181] = "Camas";
    $TagProducto[169] = "Desarrollo";
    $TagProducto[225] = "Diseño Automotriz";
    $TagProducto[229] = "Diseño de Juegos";
    $TagProducto[228] = "Diseño de Muebles";
    $TagProducto[192] = "Diseño de Producto";
    $TagProducto[191] = "Diseño Industrial";
    //$TagProducto[] = "Elementos de instalaciones y construcción"; // NO EXISTE
    $TagProducto[174] = "Envase y embalaje estructural";
    $TagProducto[186] = "Envases";
    $TagProducto[166] = "Equipamiento";
    $TagProducto[172] = "Equipamiento doméstico";
    $TagProducto[171] = "Equipamiento para el ocio";
    $TagProducto[173] = "Equipamiento urbano y de uso público";
    $TagProducto[233] = "Fotografía";
    $TagProducto[175] = "Gráfica aplicada a producto";
    $TagProducto[185] = "Joyería";
    $TagProducto[183] = "Lámparas";
    $TagProducto[176] = "Máquina y herramienta";
    $TagProducto[180] = "Mesas";
    $TagProducto[177] = "Mobiliario e iluminación";
    $TagProducto[178] = "Objetos promocionales";
    $TagProducto[184] = "Orfebrería";
    $TagProducto[187] = "Packaging";
    $TagProducto[234] = "Retoque fotográfico";
    $TagProducto[179] = "Sillas";
    $TagProducto[182] = "Sillones";
    $TagProducto[168] = "Transportes";
    return $TagProducto;
  }
}

if( !function_exists('get_interiores_tags'))
{
  function get_interiores_tags()
  {
    $TagInteriorismo = array();
    $TagInteriorismo[311] = "Arquitectura";
    $TagInteriorismo[162] = "Bares";
    $TagInteriorismo[156] = "Comercios";
    $TagInteriorismo[227] = "Diseño de exposiciones";
    $TagInteriorismo[158] = "Diseño de iluminación";
    $TagInteriorismo[165] = "Diseño de interiores";
    $TagInteriorismo[189] = "Diseño de Interiorismo";
    $TagInteriorismo[190] = "Diseño de Paisaje";
    $TagInteriorismo[152] = "Diseño del paisaje";
    $TagInteriorismo[151] = "Escaparatismo";
    $TagInteriorismo[231] = "Escenografía";
    $TagInteriorismo[159] = "Exposiciones";
    $TagInteriorismo[154] = "Hábitat";
    $TagInteriorismo[164] = "Hogares";
    $TagInteriorismo[161] = "Hoteles";
    $TagInteriorismo[160] = "Museos";
    $TagInteriorismo[155] = "Oficinas";
    $TagInteriorismo[157] = "Puntos de venta";
    $TagInteriorismo[163] = "Restaurantes";
    $TagInteriorismo[153] = "Stands";
    return $TagInteriorismo;
  }
}

if( !function_exists('get_graficos_tags'))
{
  function get_graficos_tags()
  {
    $TagGrafico = array();
    $TagGrafico[135] = "Afiches";
    $TagGrafico[144] = "Campañas publicitarias";
    $TagGrafico[134] = "Cartelería";
    $TagGrafico[313] = "Catálogos";
    $TagGrafico[310] = "Diseño de Iconografía";
    $TagGrafico[230] = "Diseño de impresión";
    $TagGrafico[139] = "Diseño de la información";
    $TagGrafico[132] = "Diseño de Packaging";
    $TagGrafico[137] = "Diseño Editorial";
    $TagGrafico[131] = "Diseño Gráfico";
    $TagGrafico[143] = "Embalaje gráfico";
    $TagGrafico[142] = "Envase";
    $TagGrafico[136] = "Flyers";
    $TagGrafico[232] = "Fotografía";
    $TagGrafico[140] = "Identidad corporativa";
    $TagGrafico[106] = "Ilustración";
    $TagGrafico[141] = "Imagen gráfica";
    $TagGrafico[148] = "Logo";
    $TagGrafico[145] = "Marketing directo";
    $TagGrafico[133] = "Packaging";
    $TagGrafico[146] = "Promociones publicitarias";
    $TagGrafico[235] = "Retoque fotográfico";
    $TagGrafico[147] = "Señalización del espacio";
    $TagGrafico[138] = "Tipografía";
    return $TagGrafico;
  }
}

if ( ! function_exists( 'get_job_listings' ) ) :
/**
 * Queries job listings with certain criteria and returns them
 *
 * @access public
 * @return void
 */
function get_job_listings( $args = array() ) {
	global $wpdb;

	$args = wp_parse_args( $args, array(
		'search_location'   => '',
		'search_keywords'   => '',
		'search_categories' => array(),
		'job_types'         => array(),
		'offset'            => '',
		'posts_per_page'    => '-1',
		'orderby'           => 'date',
		'order'             => 'DESC'
	) );

	$query_args = array(
		'post_type'           => 'job_listing',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'offset'              => absint( $args['offset'] ),
		'posts_per_page'      => intval( $args['posts_per_page'] ),
		'orderby'             => $args['orderby'],
		'order'               => $args['order'],
		'tax_query'           => array(),
		'meta_query'          => array()
	);

	// if ( ! empty( $args['job_types'] ) )
	// 	$query_args['tax_query'][] = array(
	// 		'taxonomy' => 'job_listing_type',
	// 		'field'    => 'slug',
	// 		'terms'    => $args['job_types']
	// 	);

	if ( ! empty( $args['search_categories'] ) ) {
		$field = is_numeric( $args['search_categories'][0] ) ? 'term_id' : 'slug';
		
		$query_args['tax_query'][] = array(
			'taxonomy' => 'job_listing_category',
			'field'    => $field,
			'terms'    => $args['search_categories']
		);
	}

	if ( ! empty( $args['search_job_types'] ) ) {
		$field = is_numeric( $args['search_job_types'][0] ) ? 'term_id' : 'slug';

		$query_args['tax_query'][] = array(
			'taxonomy' => 'job_listing_type',
			'field'    => $field,
			'terms'    => $args['search_job_types']
		);
	}

	if ( get_option( 'job_manager_hide_filled_positions' ) == 1 )
		$query_args['meta_query'][] = array(
			'key'     => '_filled',
			'value'   => '1',
			'compare' => '!='
		);

	// Location search - search geolocation data and location meta
	if ( $args['search_location'] ) {
		$location_post_ids = array_merge( $wpdb->get_col( $wpdb->prepare( "
		    SELECT DISTINCT post_id FROM {$wpdb->postmeta}
		    WHERE meta_key IN ( 'geolocation_city', 'geolocation_country_long', 'geolocation_country_short', 'geolocation_formatted_address', 'geolocation_state_long', 'geolocation_state_short', 'geolocation_street', 'geolocation_zipcode', '_job_location' ) 
		    AND meta_value LIKE '%%%s%%'
		", $args['search_location'] ) ), array( 0 ) );
	} else {
		$location_post_ids = array();
	}

	// Keyword search - search meta as well as post content
	if ( $args['search_keywords'] ) {
		$search_keywords              = array_map( 'trim', explode( ',', $args['search_keywords'] ) );
		$posts_search_keywords_sql    = array();
		$postmeta_search_keywords_sql = array();

		foreach ( $search_keywords as $keyword ) {
			$postmeta_search_keywords_sql[] = " meta_value LIKE '%" . esc_sql( $keyword ) . "%' ";
			$posts_search_keywords_sql[]    = " 
				post_title LIKE '%" . esc_sql( $keyword ) . "%' 
				OR post_content LIKE '%" . esc_sql( $keyword ) . "%' 
			";
		}

		$keyword_post_ids = $wpdb->get_col( "
		    SELECT DISTINCT post_id FROM {$wpdb->postmeta}
		    WHERE " . implode( ' OR ', $postmeta_search_keywords_sql ) . "
		" );

		$keyword_post_ids = array_merge( $keyword_post_ids, $wpdb->get_col( "
		    SELECT ID FROM {$wpdb->posts}
		    WHERE ( " . implode( ' OR ', $posts_search_keywords_sql ) . " )
		    AND post_type = 'job_listing'
		    AND post_status = 'publish'
		" ), array( 0 ) );
	} else {
		$keyword_post_ids = array();
	}

	// Merge post ids
	if ( ! empty( $location_post_ids ) && ! empty( $keyword_post_ids ) ) {
		$query_args['post__in'] = array_intersect( $location_post_ids, $keyword_post_ids );
	} elseif ( ! empty( $location_post_ids ) || ! empty( $keyword_post_ids ) ) {
		$query_args['post__in'] = array_merge( $location_post_ids, $keyword_post_ids );
	}

	$query_args = apply_filters( 'job_manager_get_listings', $query_args );

	if ( empty( $query_args['meta_query'] ) )
		unset( $query_args['meta_query'] );

	if ( empty( $query_args['tax_query'] ) )
		unset( $query_args['tax_query'] );

	if ( $args['orderby'] == 'featured' ) {
		$query_args['orderby'] = 'meta_key';
		$query_args['meta_key'] = '_featured';
		add_filter( 'posts_clauses', 'order_featured_job_listing' );
	}

	// Filter args
	$query_args = apply_filters( 'get_job_listings_query_args', $query_args );

	do_action( 'before_get_job_listings', $query_args );

	$result = new WP_Query( $query_args );

	do_action( 'after_get_job_listings', $query_args );

	remove_filter( 'posts_clauses', 'order_featured_job_listing' );

	return $result;
}
endif;

if ( ! function_exists( 'order_featured_job_listing' ) ) :
	/**
	 * WP Core doens't let us change the sort direction for invidual orderby params - http://core.trac.wordpress.org/ticket/17065
	 *
	 * @access public
	 * @param array $args
	 * @return array
	 */
	function order_featured_job_listing( $args ) {
		global $wpdb;

		$args['orderby'] = "$wpdb->postmeta.meta_value+0 DESC, $wpdb->posts.post_title ASC";

		return $args;
	}
endif;

if ( ! function_exists( 'get_featured_job_ids' ) ) :
/**
 * Gets the ids of featured jobs.
 *
 * @access public
 * @return array
 */
function get_featured_job_ids() {
	return get_posts( array(
		'posts_per_page' => -1,
		'post_type'      => 'job_listing',
		'post_status'    => 'publish',
		'meta_key'       => '_featured',
		'meta_value'     => '1',
		'fields'         => 'ids'
	) );
}
endif;

if ( ! function_exists( 'get_job_listing_types' ) ) :
/**
 * Outputs a form to submit a new job to the site from the frontend.
 *
 * @access public
 * @return array
 */
function get_job_listing_types() {
	return get_terms( "job_listing_type", array(
		'orderby'       => 'name',
	    'order'         => 'ASC',
	    'hide_empty'    => false,
	) );
}
endif;

if ( ! function_exists( 'get_job_listing_categories' ) ) :
/**
 * Outputs a form to submit a new job to the site from the frontend.
 *
 * @access public
 * @return array
 */
function get_job_listing_categories() {
	if ( ! get_option( 'job_manager_enable_categories' ) )
		return array();

	return get_terms( "job_listing_category", array(
		'orderby'       => 'name',
	    'order'         => 'ASC',
	    'hide_empty'    => false,
	) );
}
endif;

if ( ! function_exists( 'job_manager_get_filtered_links' ) ) :
/**
 * Shows links after filtering jobs
 */
function job_manager_get_filtered_links( $args = array() ) {

	$links = apply_filters( 'job_manager_job_filters_showing_jobs_links', array(
		'reset' => array(
			'name' => __( 'Reset', 'wp-job-manager' ),
			'url'  => '#'
		),
		'rss_link' => array(
			'name' => __( 'RSS', 'wp-job-manager' ),
			'url'  => get_job_listing_rss_link( apply_filters( 'job_manager_get_listings_custom_filter_rss_args', array(
				'type'           => isset( $args['filter_job_types'] ) ? implode( ',', $args['filter_job_types'] ) : '',
				'location'       => $args['search_location'],
				'job_categories' => implode( ',', $args['search_categories'] ),
				's'              => $args['search_keywords'],
			) ) )
		)
	), $args );

	$return = '';

	foreach ( $links as $key => $link ) {
		$return .= '<a href="' . esc_url( $link['url'] ) . '" class="' . esc_attr( $key ) . '">' . $link['name'] . '</a>';
	}

	return $return;
}
endif;

if ( ! function_exists( 'get_job_listing_rss_link' ) ) :
/**
 * Get the Job Listing RSS link
 *
 * @return string
 */
function get_job_listing_rss_link( $args = array() ) {
	$rss_link = add_query_arg( array_merge( array( 'feed' => 'job_feed' ), $args ), home_url() );

	return $rss_link;
}
endif;

if ( ! function_exists( 'job_manager_create_account' ) ) :
/**
 * Handle account creation.
 *
 * @param  string $account_email
 * @param  string $role 
 * @return WP_error | bool was an account created?
 */
function wp_job_manager_create_account( $account_email, $role = '' ) {
	global  $current_user;

	$user_email = apply_filters( 'user_registration_email', sanitize_email( $account_email ) );

	if ( empty( $user_email ) )
		return false;

	if ( ! is_email( $user_email ) )
		return new WP_Error( 'validation-error', __( 'Your email address isn&#8217;t correct.', 'wp-job-manager' ) );

	if ( email_exists( $user_email ) )
		return new WP_Error( 'validation-error', __( 'This email is already registered, please choose another one.', 'wp-job-manager' ) );

	// Email is good to go - use it to create a user name
	$username = sanitize_user( current( explode( '@', $user_email ) ) );
	$password = wp_generate_password();

	// Ensure username is unique
	$append     = 1;
	$o_username = $username;

	while( username_exists( $username ) ) {
		$username = $o_username . $append;
		$append ++;
	}

	// Final error check
	$reg_errors = new WP_Error();
	do_action( 'register_post', $username, $user_email, $reg_errors );
	$reg_errors = apply_filters( 'registration_errors', $reg_errors, $username, $user_email );

	if ( $reg_errors->get_error_code() )
		return $reg_errors;

	// Create account
	$new_user = array(
		'user_login' => $username,
		'user_pass'  => $password,
		'user_email' => $user_email,
		'role'       => $role ? $role : get_option( 'default_role' )
    );

    $user_id = wp_insert_user( apply_filters( 'job_manager_create_account_data', $new_user ) );

    if ( is_wp_error( $user_id ) )
    	return $user_id;

    // Notify
    wp_new_user_notification( $user_id, $password );

	// Login
    wp_set_auth_cookie( $user_id, true, is_ssl() );
    $current_user = get_user_by( 'id', $user_id );

    return true;
}
endif;

/**
 * True if an the user can post a job. If accounts are required, and reg is enabled, users can post (they signup at the same time).
 *
 * @return bool
 */
function job_manager_user_can_post_job() {
	$can_post = true;

	if ( ! is_user_logged_in() ) {
		if ( job_manager_user_requires_account() && ! job_manager_enable_registration() ) {
			$can_post = false;
		}
	}

	return apply_filters( 'job_manager_user_can_post_job', $can_post );
}

/**
 * True if an the user can edit a job.
 *
 * @return bool
 */
function job_manager_user_can_edit_job( $job_id ) {
	$can_edit = true;
	$job      = get_post( $job_id );

	if ( ! is_user_logged_in() ) {
		$can_edit = false;
	} elseif ( $job->post_author != get_current_user_id() ) {
		$can_edit = false;
	}

	return apply_filters( 'job_manager_user_can_edit_job', $can_edit, $job_id );
}

/**
 * True if registration is enabled.
 *
 * @return bool
 */
function job_manager_enable_registration() {
	return apply_filters( 'job_manager_enable_registration', get_option( 'job_manager_enable_registration' ) == 1 ? true : false );
}

/**
 * True if an account is required to post a job.
 *
 * @return bool
 */
function job_manager_user_requires_account() {
	return apply_filters( 'job_manager_user_requires_account', get_option( 'job_manager_user_requires_account' ) == 1 ? true : false );
}
