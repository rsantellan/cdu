<?php
session_start();
$_SESSION['idEmpresa'] = $job_id;
/**
 * Job Submission Form
 */
if ( ! defined( 'ABSPATH' ) ) exit;

global $job_manager;
?>

<form action="<?php echo $action; ?>" method="post" id="submit-job-form" class="job-manager-form" enctype="multipart/form-data" onsubmit="return checkCheckBoxes();">

	<?php if ( apply_filters( 'submit_job_form_show_signin', true ) ) : ?>

		<?php get_job_manager_template( 'account-signin.php' ); ?>

	<?php endif; ?>

	<?php if ( job_manager_user_can_post_job() ) : ?>
		<!-- Job Information Fields -->
		<?php do_action( 'submit_job_form_job_fields_start' ); ?>
		
        <?php  
					if($_REQUEST['continue'] != "")
					{
						wp_redirect( '../seguir-modificando/', 301 ); 
						exit;
					}
		?>
			
		<?php foreach ( $job_fields as $key => $field ) : ?>
        	<fieldset class="fieldset-<?php esc_attr_e( $key ); ?>">
				<label for="<?php esc_attr_e( $key ); ?>"><?php echo $field['label'] . apply_filters( 'submit_job_form_required_label', $field['required'] ? '' : ' <small>' . __( '(optional)', 'wp-job-manager' ) . '</small>', $field ); ?></label>
				<div class="field <?php echo $field['required'] ? 'required-field' : ''; ?>">
					<?php get_job_manager_template( 'form-fields/' . $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
				</div>
			</fieldset>
		<?php endforeach; ?>
		<?php do_action( 'submit_job_form_job_fields_end' ); ?>
		<!-- Company Information Fields -->
		<?php if ( $company_fields ) : ?>
			<h2><?php _e( 'Company details', 'wp-job-manager' ); ?></h2>

			<?php do_action( 'submit_job_form_company_fields_start' ); ?>

			<?php foreach ( $company_fields as $key => $field ) : ?>
				<fieldset class="fieldset-<?php esc_attr_e( $key ); ?>">
					<label for="<?php esc_attr_e( $key ); ?>"><?php echo $field['label'] . apply_filters( 'submit_job_form_required_label', $field['required'] ? '' : ' <small>' . __( '(optional)', 'wp-job-manager' ) . '</small>', $field ); ?></label>
					<div class="field <?php echo $field['required'] ? 'required-field' : ''; ?>">
						<?php get_job_manager_template( 'form-fields/' . $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
					</div>
				</fieldset>
			<?php endforeach; ?>
			<?php do_action( 'submit_job_form_company_fields_end' ); ?>
		<?php endif; ?>

		<p>
			<?php wp_nonce_field( 'submit_form_posted' ); ?>
			<input type="hidden" name="job_manager_form" value="<?php echo $form; ?>" />
            <input type="hidden" name="job_id" value="<?php echo esc_attr( $job_id ); ?>" />
			<input type="hidden" name="step" value="0" />
<!---------------------esto es el formulario de las imagenes------------------------!>

<?php
    //var_dump($job_fields);
	$output = "";
	$user_ID = get_current_user_id();
	$IdEmpresa = $job_id;
	$_SESSION['idEmpresa'] = $IdEmpresa;
	global $wpdb;
    $job = get_post($job_id);
    $TipoDeEmpresa = get_post_meta($job_id, "_featured", "ARRAY_N");
    $mainCategory = wp_get_post_terms($job_id, 'job_listing_category');
    if(count($mainCategory)> 0){
		$mainCategory = array_pop($mainCategory);
    }
    //var_dump($mainCategory);
    $sqlSubCategories = "SELECT tt.term_id, wtr.term_taxonomy_id, t.slug, tt.taxonomy, t.name FROM wp_term_relationships wtr join wp_term_taxonomy as tt on tt.term_taxonomy_id = wtr.term_taxonomy_id inner join wp_terms as t on tt.term_id = t.term_id WHERE tt.taxonomy = 'job_listing_category' and wtr.object_id = '$job_id'";
	
	$subCategories = $wpdb->get_results($sqlSubCategories, ARRAY_N);
	$categs = array();
	foreach ($subCategories as $results) {
		if ($results[3] == 'job_listing_category') {
			$categs[$results[0]] = $results[2];
		}
	}
    $sqlCategories = "SELECT term_id, name, slug FROM wp_terms where term_id in (43, 44, 45, 46, 47, 48, 117) order by term_id ASC";
    $resultCategories = $wpdb->get_results($sqlCategories, ARRAY_N);
    $usedCategories = array();
    foreach($resultCategories as $category){
      $main = false;
      if($mainCategory->term_id == $category[0]){
		$main = true;
      }
      $usedCategories[$category[0]] = array('name' => $category[1], 'slug' => $category[2], 'main' => $main);

    }
    foreach($job_fields['job_category']['options'] as $key => $value){
    	$found = false;
    	$auxCategory = null;
    	$auxKey = null;
    	foreach($usedCategories as $categoryKey => $category){
			if($key == $category['slug'])
			{
				$found = true;
				$auxCategory = $category;
				$auxKey = $categoryKey;
			}
			if(!isset($usedCategories[$categoryKey]['enabled']))
			{
				$usedCategories[$categoryKey]['enabled'] = false;
			}
			
    	}
		$enabled = false;
		if ($found) {
			if (in_array($key, $categs)) {
				$usedCategories[$auxKey]['enabled'] = true;
			}
		}
    }
    /*
    echo '<hr/>';
    var_dump($usedCategories);
    echo '<hr/>';
    var_dump($IdEmpresa);
	*/
    if($TipoDeEmpresa == 1 && $user_ID > 0)
	{
			
			if($_REQUEST['e'] != 1)
			{
				$output ='
				<div id="divImagenes">';
			}
			else
			{
				$output ='
				<div id="divImagenes">';
			}
			
			?>
			<?php
			global $wpdb;
			$cat = 1;
            $sqlFirst = "SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = '$IdEmpresa' ";
//            var_dump($sqlFirst);
//            echo '<hr/>';
			$result2 = $wpdb->get_results($sqlFirst, ARRAY_N) ; 
            
			foreach( $result2 as $results2 ) 
			{   
                $sqlSecond = "SELECT term_id FROM wp_term_taxonomy WHERE taxonomy = 'job_listing_category' and term_taxonomy_id = '$results2[0]' ";
//                var_dump($sqlSecond);
//                echo '<hr/>';
				$result4 = $wpdb->get_results($sqlSecond, ARRAY_N) ; 
				foreach( $result4 as $results4 ) 
				{
                    $sqlThird = "SELECT name FROM wp_terms WHERE term_id = '$results4[0]' ";
//                    var_dump($sqlThird);
//                    echo '<hr/>';
					$result5 = $wpdb->get_results($sqlThird, ARRAY_N) ; 
					foreach( $result5 as $results5 ) 
					{
						if($cat == 1)
						{
							$Categoria1 = $results5[0];
						}	
						else if ($cat == 2)
						{
							$Categoria2 = $results5[0];
						}
						else if ($cat == 3)
						{
							$Categoria3 = $results5[0];
						}
						else if ($cat == 4)
						{
							$Categoria4 = $results5[0];
						}
						else if ($cat == 5)
						{
							$Categoria5 = $results5[0];
						}
						$cat = $cat + 1;
						
					}
				}
				
			}
			
			$output .= "
			<input type='hidden' name='subir' value='1'/>
			<p>Las imágenes deben ser de 1100x450 px y no pueden superar los 500 kb. Podrá agregar la descripción de la imagen en el campo de texto.</p> 
					";
			
					global $wpdb;
				$upload_dir = wp_upload_dir();
			
			if ($Categoria2 != "")
			{		
				global $wpdb;
				$upload_dir = wp_upload_dir();
				$result3 = $wpdb->get_results("SELECT post_title FROM wp_posts WHERE ID = '$IdEmpresa'", ARRAY_N); 
				foreach( $result3 as $results3 ) 
				{
					$slug = str_replace("/", "_", $results3[0].$Categoria3);
					$slug = sanitize_title($slug);
					//$slug = str_replace(" ", "_", "$slug");
				}
				$result4 = $wpdb->get_results("SELECT ID,post_type FROM wp_posts WHERE post_name = '$slug' ", ARRAY_N); 
                
				foreach( $result4 as $results4 ) 
				{
					if ($results4[1] == "soliloquy")
					{
						$idSolilo = $results4[0];
					}
				}
				if($idSolilo != "")
				{
					$result5 = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_parent = '$idSolilo' ", ARRAY_N); 
					foreach( $result5 as $results5 ) 
					{
						$result6 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE meta_key = '_wp_attached_file' and post_id = '$results5[0]' ", ARRAY_N); 
						$output .= "<div class='imagenesBorrar'>";
						foreach( $result6 as $results6 ) 
						{
							$path = substr($upload_dir['url'], 0, -8);
							$attachment = str_replace("wp-content/uploads", "", $results6[0]);
							$upDir = $upload_dir['baseurl'];
							
							//$output .= '<div class="ImgYBoton"><img class="classImagen" src="'.$path.'/'.$attachment.'" alt="" style="width:220px;height:98px"> <input type="button" class="IdBoton" onclick="borrarImagen(\''.$results5[0].'\')"> </div>';
						} 
						$output .="</div>";
					}
				}
	
			}
			if($Categoria3 != '')
			{		
			
				global $wpdb;
				$upload_dir = wp_upload_dir();
				$result3 = $wpdb->get_results("SELECT post_title FROM wp_posts WHERE ID = '$IdEmpresa'", ARRAY_N); 
				foreach( $result3 as $results3 ) 
				{
					$slug = str_replace("/", "_", $results3[0].$Categoria3);
					$slug = sanitize_title($slug);
					//$slug = str_replace(" ", "_", "$slug");
				}
				
				$result4 = $wpdb->get_results("SELECT ID,post_type FROM wp_posts WHERE post_name = '$slug' ", ARRAY_N); 
				foreach( $result4 as $results4 ) 
				{
					if ($results4[1] == "soliloquy")
					{
						$idSolilo = $results4[0];
					}
				}
				if($idSolilo != "")
				{
					$result5 = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_parent = '$idSolilo' ", ARRAY_N); 
					foreach( $result5 as $results5 ) 
					{
						$result6 = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE meta_key = '_wp_attached_file' and post_id = '$results5[0]' ", ARRAY_N); 
						$output .= "<div class='imagenesBorrar'>";
						foreach( $result6 as $results6 ) 
						{
							$path = substr($upload_dir['url'], 0, -8);
							$attachment = str_replace("wp-content/uploads", "", $results6[0]);
							$upDir = $upload_dir['baseurl'];
							
						}
						$output .="</div>";
					}
				}
	

				
			}
			
				//global $wpdb;
				//if($idSolilo != "")
				//{
				
				$Escribio1 = false;
				$Escribio2 = false;
				$Escribio3 = false;
				$Escribio4 = false;
				$sqlFilesMeta = "SELECT wp.ID,wp.guid, m.meta_value FROM wp_posts wp , wp_postmeta m WHERE wp.post_parent = '$IdEmpresa' and  wp.post_mime_type = 'image/jpeg' and m.post_id = wp.ID and meta_key = 'Categoria'";
				$sqlTextosMeta = "SELECT wp.ID,wp.guid, m.meta_value FROM wp_posts wp , wp_postmeta m WHERE wp.post_parent = '$IdEmpresa' and  wp.post_mime_type = 'image/jpeg' and m.post_id = wp.ID and meta_key = 'Texto'";
                $filesData = $wpdb->get_results($sqlFilesMeta, ARRAY_N); 
				$textosData = $wpdb->get_results($sqlTextosMeta, ARRAY_N); 
				$list = array();
				foreach($filesData as $fileData){
					if(!isset($list[$fileData[2]])){
						$list[$fileData[2]] = array();
					}
					$texto = '';
					foreach($textosData as $textoData){
						if($textoData[0] == $fileData[0]){
							$texto = $textoData[2];
						}
					}
					$list[$fileData[2]][$fileData[0]] = array(
											'id' => $fileData[0],
											'guid' => $fileData[1],
											'cat' => $fileData[2],
											'texto' => $texto);
				}
				for($NumCat = 1; $NumCat < 5; $NumCat++)
				{
						
							if(!$Escribio1 && $NumCat == 1)
							{
								$output .= "<p class='tituloCateg'>$Categoria1</p>";
								$Escribio1 = true;
								$output	.= '
					<input type="file" id="imagen11" name="cat11" onclick="MostrarSiguiente(1,2)"><textarea id="texto11" class="paraescribir" name="texto11"  placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
						';
				
								$output	.= '<input type="file" id="imagen12" name="cat12" onclick="MostrarSiguiente(1,3)" style="display: none" ><textarea class="paraescribir" name="texto12" id="texto12" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
						
								<input type="file" id="imagen13" name="cat13" onclick="MostrarSiguiente(1,4)" style="display: none"><textarea class="paraescribir" id="texto13" name="texto13" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
								
								<input type="file" id="imagen14" name="cat14" onclick="MostrarSiguiente(1,5)" style="display: none"><textarea class="paraescribir" name="texto14" id="texto14" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
								
								<input type="file" id="imagen15" name="cat15" style="display: none"><textarea name="texto15" class="paraescribir" id="texto15" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
								</br>
								';
								if(isset($list[$NumCat]))
								{
									//var_dump(count($list[$NumCat]));
									foreach($list[$NumCat] as $image){
										$output .= "<div class='imagenesBorrar 1'>";
										$output .= '<div id="divId'.$image['id'].'" class="ImgYBoton">';
										$output .= '<img class="classImagen" src="'.$image['guid'].'" alt="" style="width:220px;height:98px" />';
										$output .= '<input type="button" class="IdBoton" onclick="document.getElementById(\'divIdBorrando'.$image['id'].'\').style.display=\'block\';borrarImagen(\''.$image['id'].'\',\''.$image['guid'].'\') " value="Borrar" />';
										$output .= '<span style="display: none" id="divIdBorrando'.$image['id'].'">Borrando imagen</span>';
										$output .='<span id="ModTextAreaContent'.$image['id'].'" class="textoModificar">'.$image['texto']."".'</span>';
										$output .='<input type="button" id="botMostTxt'.$image['id'].'" value="Texto" class="ModText" onclick="MostrarCampo(\'ModTextArea'.$image['id'].'\',\'botMostTxt'.$image['id'].'\',\'btntxtMod'.$image['id'].'\')"/>';
										$output .='<textarea class="textAreaModificar" id="ModTextArea'.$image['id'].'" style="display: none">'.$image['texto'].'</textarea>';
										$output .='<input class="botonModificarClase" type="button" id="btntxtMod'.$image['id'].'" style="display: none" value="Modificar" onclick="ModificarTextoAjax(\''.$image['id'].'\')"/>';
										$output .='<span class="textoExito" style="display: none" id="exitoMod'.$image['id'].'">Texto modificado.</span></div>';
										$output .="</div>";
									}	
								}
								
							}
							if(!$Escribio2 && $NumCat == 2)
							{
								if($Categoria2 != "")
								{
                                    $output .= "<div style='clear:both !important;'></div>";
									$output .= "<p class='tituloCateg'>$Categoria2</p>";
									$Escribio2 = true;
									$output .= '		
							
									<input type="file" id="imagen21" name="cat21" onclick="MostrarSiguiente(2,2)"  ><textarea name="texto21" id="texto21" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" name="texto22" id="imagen22" name="cat22" onclick="MostrarSiguiente(2,3)" style="display: none" ><textarea id="texto22" name="texto22" style="display: none" class="paraescribir" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen23" name="cat23" onclick="MostrarSiguiente(2,4)" style="display: none" ><textarea class="paraescribir" name="texto23" id="texto23" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen24" name="cat24" onclick="MostrarSiguiente(2,5)" style="display: none" ><textarea class="paraescribir" name="texto24" id="texto24" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen25" name="cat25"  style="display: none" ><textarea id="texto25" name="texto25" class="paraescribir" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<br>';
									if(isset($list[$NumCat]))
									{
										//var_dump(count($list[$NumCat]));
										foreach($list[$NumCat] as $image){
											$output .= "<div class='imagenesBorrar 2'>";
											$output .= '<div id="divId'.$image['id'].'" class="ImgYBoton">';
											$output .= '<img class="classImagen" src="'.$image['guid'].'" alt="" style="width:220px;height:98px" />';
											$output .= '<input type="button" class="IdBoton" onclick="document.getElementById(\'divIdBorrando'.$image['id'].'\').style.display=\'block\';borrarImagen(\''.$image['id'].'\',\''.$image['guid'].'\') " value="Borrar" />';
											$output .= '<span style="display: none" id="divIdBorrando'.$image['id'].'">Borrando imagen</span>';
											$output .='<span id="ModTextAreaContent'.$image['id'].'" class="textoModificar">'.$image['texto']."".'</span>';
											$output .='<input type="button" id="botMostTxt'.$image['id'].'" value="Texto" class="ModText" onclick="MostrarCampo(\'ModTextArea'.$image['id'].'\',\'botMostTxt'.$image['id'].'\',\'btntxtMod'.$image['id'].'\')"/>';
											$output .='<textarea class="textAreaModificar" id="ModTextArea'.$image['id'].'" style="display: none">'.$image['texto'].'</textarea>';
											$output .='<input class="botonModificarClase" type="button" id="btntxtMod'.$image['id'].'" style="display: none" value="Modificar" onclick="ModificarTextoAjax(\''.$image['id'].'\')"/>';
											$output .='<span class="textoExito" style="display: none" id="exitoMod'.$image['id'].'">Texto modificado.</span></div>';
											$output .="</div>";
										}	
									}
								
								}
							}
							if(!$Escribio3 && $NumCat == 3)
							{
								if($Categoria3 != "")
								{
                                    $output .= "<div style='clear:both !important;'></div>";
									$output .= "<p class='tituloCateg'>$Categoria3</p>";
									$Escribio3 = true;
									$output .=	'<input type="file" id="imagen31" name="cat31" onclick="MostrarSiguiente(3,2)" ><textarea class="paraescribir" name="texto31" id="texto31" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen32" name="cat32" onclick="MostrarSiguiente(3,3)" style="display: none" ><textarea class="paraescribir" name="texto32" id="texto32" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen33" name="cat33" onclick="MostrarSiguiente(3,4)" style="display: none" ><textarea class="paraescribir" name="texto33" id="texto33" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen34" name="cat34" onclick="MostrarSiguiente(3,5)" style="display: none" ><textarea class="paraescribir" name="texto34" id="texto34" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen35" name="cat35" style="display: none" ><textarea id="texto35" name="texto35" class="paraescribir" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<br>
									';
									if(isset($list[$NumCat]))
									{
										//var_dump(count($list[$NumCat]));
										foreach($list[$NumCat] as $image){
											$output .= "<div class='imagenesBorrar 3'>";
											$output .= '<div id="divId'.$image['id'].'" class="ImgYBoton">';
											$output .= '<img class="classImagen" src="'.$image['guid'].'" alt="" style="width:220px;height:98px" />';
											$output .= '<input type="button" class="IdBoton" onclick="document.getElementById(\'divIdBorrando'.$image['id'].'\').style.display=\'block\';borrarImagen(\''.$image['id'].'\',\''.$image['guid'].'\') " value="Borrar" />';
											$output .= '<span style="display: none" id="divIdBorrando'.$image['id'].'">Borrando imagen</span>';
											$output .='<span id="ModTextAreaContent'.$image['id'].'" class="textoModificar">'.$image['texto']."".'</span>';
											$output .='<input type="button" id="botMostTxt'.$image['id'].'" value="Texto" class="ModText" onclick="MostrarCampo(\'ModTextArea'.$image['id'].'\',\'botMostTxt'.$image['id'].'\',\'btntxtMod'.$image['id'].'\')"/>';
											$output .='<textarea class="textAreaModificar" id="ModTextArea'.$image['id'].'" style="display: none">'.$image['texto'].'</textarea>';
											$output .='<input class="botonModificarClase" type="button" id="btntxtMod'.$image['id'].'" style="display: none" value="Modificar" onclick="ModificarTextoAjax(\''.$image['id'].'\')"/>';
											$output .='<span class="textoExito" style="display: none" id="exitoMod'.$image['id'].'">Texto modificado.</span></div>';
											$output .="</div>";
										}	
									}
							}
						}
						if(!$Escribio4 && $NumCat == 4)
							{
								if($Categoria4 != "")
								{
                                    $output .= "<div style='clear:both !important;'></div>";
									$output .= "<p class='tituloCateg'>$Categoria4</p>";
									$Escribio4 = true;
									$output .=	'<input type="file" id="imagen41" name="cat41" onclick="MostrarSiguiente(4,2)" ><textarea name="texto41" class="paraescribir" id="texto41" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen42" name="cat42" onclick="MostrarSiguiente(4,3)" style="display: none" ><textarea class="paraescribir" name="texto42" id="texto42" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen43" name="cat43" onclick="MostrarSiguiente(4,4)" style="display: none" ><textarea class="paraescribir" name="texto43" id="texto43" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen44"  name="cat44" onclick="MostrarSiguiente(4,5)" style="display: none" ><textarea class="paraescribir" name="texto44" id="texto44" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<input type="file" id="imagen45" name="cat45" style="display: none" ><textarea id="texto45" name="texto45" class="paraescribir" style="display: none" placeholder="Ingrese descripción" rows="4" cols="50"></textarea>
									<br>
									';
									if(isset($list[$NumCat]))
									{
										//var_dump(count($list[$NumCat]));
										foreach($list[$NumCat] as $image){
											$output .= "<div class='imagenesBorrar 4'>";
											$output .= '<div id="divId'.$image['id'].'" class="ImgYBoton">';
											$output .= '<img class="classImagen" src="'.$image['guid'].'" alt="" style="width:220px;height:98px" />';
											$output .= '<input type="button" class="IdBoton" onclick="document.getElementById(\'divIdBorrando'.$image['id'].'\').style.display=\'block\';borrarImagen(\''.$image['id'].'\',\''.$image['guid'].'\') " value="Borrar" />';
											$output .= '<span style="display: none" id="divIdBorrando'.$image['id'].'">Borrando imagen</span>';
											$output .='<span id="ModTextAreaContent'.$image['id'].'" class="textoModificar">'.$image['texto']."".'</span>';
											$output .='<input type="button" id="botMostTxt'.$image['id'].'" value="Texto" class="ModText" onclick="MostrarCampo(\'ModTextArea'.$image['id'].'\',\'botMostTxt'.$image['id'].'\',\'btntxtMod'.$image['id'].'\')"/>';
											$output .='<textarea class="textAreaModificar" id="ModTextArea'.$image['id'].'" style="display: none">'.$image['texto'].'</textarea>';
											$output .='<input class="botonModificarClase" type="button" id="btntxtMod'.$image['id'].'" style="display: none" value="Modificar" onclick="ModificarTextoAjax(\''.$image['id'].'\')"/>';
											$output .='<span class="textoExito" style="display: none" id="exitoMod'.$image['id'].'">Texto modificado.</span></div>';
											$output .="</div>";
										}	
									}
							}
						
							
						}
						
						//echo "---------------->".$Categoria1.$Categoria2.$Categoria3;
						
					
					
				}
				
			
		//	$output .= '<input type="submit" value="Subirimagenes" name="submit">
			$output .= '</div>';
			echo "<div style='display: none;'>  </div>";
							
			echo $output;
			
		}
	//}

?>

<!----------------termina formulario de las imagenes-------------------------------->            
            
            <input type="submit" name="submit_job" class="button" value="<?php esc_attr_e( $submit_button_text ); ?>" />
		</p>

	<?php else : ?>

		<?php do_action( 'submit_job_form_disabled' ); ?>

	<?php endif; ?>

</form>
