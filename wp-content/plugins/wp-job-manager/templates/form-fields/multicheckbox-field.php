<?php
session_start();
global $wpdb;
$wpdb->show_errors();
$TermsDeEmpresa = array();
$categs = array();
$companyTags = array();
$idEmpresa = $_SESSION['idEmpresa'];
$sqlTerm = "SELECT tt.term_id, wtr.term_taxonomy_id, t.slug, tt.taxonomy, t.name FROM wp_term_relationships wtr join wp_term_taxonomy as tt on tt.term_taxonomy_id = wtr.term_taxonomy_id inner join wp_terms as t on tt.term_id = t.term_id WHERE wtr.object_id = '$idEmpresa'";
//var_dump($sqlTerm);
$result = $wpdb->get_results($sqlTerm, ARRAY_N);
foreach ($result as $results) {
  switch ($results[3]) {
    case 'tag':
      $companyTags[] = $results[0];
      break;
    case 'job_listing_category':
      $TermsDeEmpresa[] = $results[0];
      $categs[] = $results[2];
      break;    
    default:
      # code...
      break;
  }
}
$result = $wpdb->get_results("SELECT name,slug FROM wp_terms ", ARRAY_N);

foreach ($result as $results) {
  if ($results[0][0] != "0") {
    $tags["$results[1]"] = $results[0];
  }
}

$catPrin = array();
echo "";
$sqlCategories = "SELECT slug FROM wp_terms where term_id in (43, 44, 45, 46, 47, 48, 117) order by term_id ASC";
$result = $wpdb->get_results($sqlCategories, ARRAY_N);
foreach ($result as $results) {
  $catPrin[] = $results[0];
}

foreach ($field['options'] as $key => $value) :
  ?>
  <?php
  if (in_array(esc_attr($key), $catPrin)) {
    ?>
    <span id="chk<?php echo esc_attr($key); ?>">
      <input type="checkbox" <?php if (in_array($key, $categs)) echo "checked"; ?> class="input-checkbox categoria-secundaria" name="<?php echo esc_attr(isset($field['name']) ? $field['name'] : $key ); ?>[]" id="<?php echo esc_attr($key); ?>" placeholder="<?php echo esc_attr($field['placeholder']); ?>" value="<?php echo $key; ?>" />
      <small style="font-size: 14px;" class="description"><?php echo esc_attr($value); ?></small><br/>
    </span>
    <?php
  }
endforeach;


$TagPadre = array();
$TagPadre[] = "Gráfico / Packaging";
$TagPadre[] = "Interiorismo / Paisajismo";
$TagPadre[] = "Producto";
$TagPadre[] = "Textil / Indumentaria";
$TagPadre[] = "Web / Multimedia";

$SubPadre = array();
$SubPadre[] = "Gráfico / Packaging";
$SubPadre[] = "Interiorismo / Paisajismo";
$SubPadre[] = "Producto";
$SubPadre[] = "Textil / Indumentaria";
$SubPadre[] = "Web / Multimedia";


$SubGrafico = get_graphics_categories();

$SubInteriorismo = get_interiors_categories();

$SubProducto = get_products_categories();

$SubTextil = get_textils_categories();

$SubWeb = get_web_categories();

echo _e("<h2 id='idEtiquetaModificar'>Subcategorias</h2>");
//echo "****************termsDeEmpresa*********************<br>";
//echo var_dump($TermsDeEmpresa);
//echo "<br>******************************************";
foreach ($SubPadre as $Padre) {


  echo "<p class='tituloTag'>$Padre</p>";
  if ($Padre == "Gráfico / Packaging") {
    $tags2 = $SubGrafico;
  } else if ($Padre == "Interiorismo / Paisajismo") {
    $tags2 = $SubInteriorismo;
  } else if ($Padre == "Producto") {
    $tags2 = $SubProducto;
  } else if ($Padre == "Textil / Indumentaria") {
    $tags2 = $SubTextil;
  } else if ($Padre == "Web / Multimedia") {
    $tags2 = $SubWeb;
  }

  echo "<ul class='subcat-class'>";
  foreach ($tags2 as $ArraySubCat) {
    ?>        	
    <li><input type="checkbox" <?php if (in_array($ArraySubCat[1], $TermsDeEmpresa)) echo "checked"; ?> class="input-checkbox" name="job_sub_cat[]"   value="<?php echo $ArraySubCat[1]; ?>" />
      <small style="font-size: 14px;" class="description"><?php echo $ArraySubCat[2] ?></small></li>

    <?php
  }
  echo "</ul>";
}

echo _e("<h2 id='idEtiquetaModificar'>Etiquetas</h2>");
echo "<ul id='IdUl'>";


$TagGrafico = get_graficos_tags();
$TagInteriorismo = get_interiores_tags();
$TagProducto = get_productos_tags();
$TagTextil = get_textil_tags();
$TagWeb = get_web_tags();

foreach ($TagPadre as $Padre) {


  echo "<p class='tituloTag'>$Padre</p>";
  if ($Padre == "Gráfico / Packaging") {
    $tags2 = $TagGrafico;
  } else if ($Padre == "Interiorismo / Paisajismo") {
    $tags2 = $TagInteriorismo;
  } else if ($Padre == "Producto") {
    $tags2 = $TagProducto;
  } else if ($Padre == "Textil / Indumentaria") {
    $tags2 = $TagTextil;
  } else if ($Padre == "Web / Multimedia") {
    $tags2 = $TagWeb;
  }
  //echo '<hr/>';
  //var_dump($tags2);
  //echo '<hr/>';
  echo "<ul>";
  foreach ($tags2 as $key => $value):
    ?>
    <li id="chk<?php echo esc_attr($key); ?>"><input type="checkbox" <?php if (in_array($key, $companyTags)) echo "checked"; ?> class="input-checkbox" name="job_tag[]" id="<?php echo esc_attr($key); ?>" placeholder="<?php echo esc_attr($field['placeholder']); ?>" value="<?php echo $key; ?>" />
      <small style="font-size: 14px;" class="description"><?php echo esc_attr($value); ?></small></li>
    <?php
  endforeach;
  echo "</ul>";
}
?>



<?php echo "</ul>"; ?>

<?php
$result = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '$idEmpresa' and meta_key = 'etiqueta_personal'", ARRAY_N);
foreach ($result as $results) {
  $EtiquetaPerson = $results[0];
}
?>

<p class='tituloTag'>Etiquetas personalizadas</p>
<p id="ingrese-etiquetar-per">Ingrese sus etiquetas personalizadas:</p>
<input type="text" name="add-tag" value="<?php echo $EtiquetaPerson ?>" class="add-tag-class" placeholder="Ej. Etiqueta1, Etiqueta2, Etiqueta3..." />
