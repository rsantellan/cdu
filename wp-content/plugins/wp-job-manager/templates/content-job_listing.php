<li <?php job_listing_class(); ?>>
	<?php if (is_position_featured( $post )) :?>
		<a href="<?php the_job_permalink(); ?>">
			<?php the_company_logo(); ?>
		</a>
		<div class="position">
			<h3><a href="<?php the_job_permalink(); ?>"><?php the_title(); ?></a></h3>
			<?php the_job_type(); ?>

			<!-- <div class="company">
				<?php the_company_name( '<strong>', '</strong> ' ); ?>
				<?php the_job_type( '<span class="tagline">', '</span>' ); ?>
			</div> -->
		</div>

		<div class="location">
			<span><?php the_job_location( false ); ?></span>
			<span>

			<?php
			$phone 		= get_post_meta(get_the_ID(), '_job_phone', true);
			$mobile 	= get_post_meta(get_the_ID(), '_job_mobile', true);
			?>


				<?php if(!empty($phone)) 		echo 'Tel: ' . get_post_meta(get_the_ID(), '_job_phone', true) . ''; ?>
				<?php if(empty($phone)) 		echo 'Cel: ' . get_post_meta(get_the_ID(), '_job_mobile', true) . ''; ?>

			</span>
		</div>

		<div class="category">
			<?php
				$categories = wp_get_post_terms( $post->ID, 'job_listing_category');
				$i = 0;
				$primary_category = '';
				$secondary_categories = '';
				foreach ($categories as $category) {
					if ($category->parent == 0){
						if($category->term_id == $post->_job_primarycategory) $primary_category = $category->name;
						else $secondary_categories .= $category->name;

						if(end($categories) !== $category) $secondary_categories .= ' ';
					}
				}
			?>
			<? $pm = get_term( $post->_job_primarycategory, 'job_listing_category' ); ?>
			<span class="catetext"><?php echo __( 'Main Category', 'wp-job-manager' ) . ': ' ?><div class="inner"><span class="job-category <?php echo $pm->slug; ?>"><?php echo $primary_category; ?></span></div></span>

			<?php
				$secondary_cats = array();
				foreach ( $categories as $category ) {
					if($category->term_id != $pm->term_id && $category->parent == 0) {
						array_push($secondary_cats, $category);
					}
				}
			?>

			<?php if($secondary_cats) : ?>
				<span class="catetext"><?php echo __( 'Other Categories', 'wp-job-manager' ) . ': ' ?><div class="inner">

		 			<?php foreach ( $secondary_cats as $category ) : ?>
						<?php if ( class_exists( 'WP_Job_Manager_Cat_Colors' ) ) : ?>

							<span class="job-category <?php echo $category->slug; ?>"><?php echo $category->name; ?></span>

						<?php else : ?>

							<span style="display:none" href="<?php echo get_term_link( $category, 'job_listing_category' ); ?>"><i class="icon-book-open"></i> <?php echo $category->name; ?></span>

						<?php endif; ?>
					<?php endforeach; ?>
				</div></span>

			<?php endif; ?>
		</div>

		<div class="contact">
			<a href="<?php the_job_permalink(); ?>"><?php echo __( 'Info/Portfolio', 'wp-job-manager' ); ?></a><a  class="dir_application_button" onclick="showDirAppModal(<?php the_ID(); ?>, '<?php the_title(); ?>');" href="javascript:void(0);"><?php echo __( 'Contact', 'wp-job-manager' ); ?></a>
		</div>
	<?php else: ?>
			<?php if (is_position_semifeatured()) : ?>
				<a href="<?php the_job_permalink(); ?>">
					<?php the_company_logo(); ?>
				</a>
			<?php else: ?>
				<div style="margin-left: 38px; margin-right: 2%; float: left;">&nbsp;</div>
			<?php endif; ?>
			<div class="position">
				<h3><?php the_title(); ?></h3>
				<?php the_job_type(); ?>
			</div>
			<div class="location">
				<span><?php the_job_location( false ); ?></span>
			</div>
			<div class="category">&nbsp;</div>

			<?php if (is_position_semifeatured()) : ?>
			<div class="contact">
				<a href="<?php the_job_permalink(); ?>"><?php echo __( 'Info', 'wp-job-manager' ); ?></a><a  class="dir_application_button" onclick="showDirAppModal(<?php the_ID(); ?>, '<?php the_title(); ?>');" href="javascript:void(0);"><?php echo __( 'Contact', 'wp-job-manager' ); ?></a>
			</div>
			<?php else: ?>
				<div class="portfolio">&nbsp;</div>
				<div class="contact">
					<a onclick="showDirAppModal(<?php the_ID(); ?>, '<?php the_title(); ?>');" class="dir_application_button" href="javascript:void(0);"><?php echo __( 'Contact', 'wp-job-manager' ); ?></a>
				</div>
			<?php endif; ?>
		</a>
	<?php endif; ?>
</li>