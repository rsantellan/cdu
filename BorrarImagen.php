<?php 

$path = $_SERVER['DOCUMENT_ROOT'];
include_once($path . '/wp-config.php');
include_once($path . '/wp-load.php');
include_once($path . '/wp-includes/wp-db.php');

global $wpdb;

$user_ID = get_current_user_id();

if($user_ID == 0){
  echo 'not allowed';
}
$img = $_GET['img'];
$result = $wpdb->get_results("SELECT post_author, post_parent FROM wp_posts WHERE ID = '$img'", ARRAY_N); 
$doDelete = false;
$postParent = null;
foreach( $result as $results ) 
{
    $postParent = $results[1];
    $Author = $results[0];
    $doDelete = true;
}
$response = false;
if($doDelete && job_manager_user_can_edit_job($postParent)){
  $aux = wp_delete_attachment($img, true);
  $response = true;
}  
echo json_encode(array('result' => $response, 'id' => $img));
die();
